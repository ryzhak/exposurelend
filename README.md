# ExposureLend

[ ![Codeship Status for ChainCloud/ExposureLend](https://app.codeship.com/projects/d0f00b10-875f-0136-36d8-2a57a5bfeaf7/status?branch=master)](https://app.codeship.com/projects/302647)

Blockchain implementation of [CFD](https://en.wikipedia.org/wiki/Contract_for_difference). Consists of smart contract and cron script(drone) for managing CFDs.

## How it works

### How it works for user
1. User creates a new long or short CFD at some USD/ETH or USD/BTC or USD/INDEX(ETH,BTC,BTH,LTC,XRP) rate and funds it with some ether(Ex: user creates long CFD at price 200$/ETH and funds it with 1ETH).
2. User can close CFD manually(to take profit) or drone can close CFD by margin call(Ex: price went up to 300$/ETH, used closed CFD and got 1.5ETH).

### How drone works
1. Drone finds all CFDs that should be processed somehow.
2. Drone autofunds CFDs that should be autofunded.
3. Drone hedges CFDs that were autofunded.
4. Drone sets CFDs' state to BadEthRate if CFD open price is not valid.
5. Drone autocloses CFDs that should be autoclosed.
6. Drone hedges CFDs that were autoclosed.
7. Drone sets CFDs' state to MarginCallTimeElapsed if they are expired(on CFD creation user sets CFD duration in days).
8. Drone hedges CFDs' that were margin called with time elapsed.
9. Drone sets CFDs' state to MarginCallPriceMovement if price went up/down too much( Ex: user creates CFD with long position, price 100 USD and 1:1 leverage. If price goes down to 50 USD then drone closes the CFD. Ex: user creates CFD with short position, price 100 USD and 2:1 leverage. If price goes up to 175 USD then drone closes the CFD).
10. Drone hedges CFDs' that were margin called with price movement.
11. Drone sets CFDs' state to MarginCallMaxGainReached if user wins more than he has(Ex: user creates CFD with long position and price 100 USD. If price goes up to 199 USD then drone closes the CFD. Ex: user creates CFD with short position and price 100 USD. If price goes down to 1 USD then drone closes the CFD).
12. Drone hedges CFDs' that were margin called with max gain reached.

### How drone hedges
Drone hedges amounts in CFD's asset. There are 3 assets available: ETH, BTC, INDEX(ETH,BTC,BCH,LTC,XRP). Also user specifies leverage on CFD creation: 1:1, 2:1, 4:1. All leverages differ in terms of margin call and hedge amount:
- 1:1. Margin call on 50% loss and hedge sum = initial funding.
- 2:1. Margin call on 75% loss and hedge sum = 2 * initial funding
- 4:1. Margin call on 87.5% loss and hedge sum = 4 * initial funding

On INDEX hedging drone buys equal amounts(in terms of USD) of assets. If CFD is funded by 1ETH with price 200$/ETH then drone would buy ETH worth of 40$, BTC worth of 40$, BCH worth 40$, LTC worth of 40$ and XRP worth of 40$.

## How to deploy

Assuming your frontend and backend repositories are already setup with auto deploy from github.

### How to deploy frontend to heroku
1. Setup angular environment variables for heroku in `src/environments/environment.heroku.ts`:
```
export const environment = {
	production: true,
	exposureLedgerAddress: { // contract addresses for different networks
		"main": "0xebb8677875c773f4b37000f9db0efac759bf1bc6", // main net address
		"ropsten": "", // ropsten address
		"kovan": "0x384261cc2c6e28ab8ceda7540179b68652a18efa", // kovan address
		"rinkeby": "", // rinkeby address
		"development": "" // local blockchain address
	},
	apiUrl: "https://exposure-backend.herokuapp.com", // api url
	spreadFee: 9500 // prices spread fee, if spread fee is 2% and current price is 200$ then sell price will be 198% and buy price will be 202$, 1000000 = 100%
};
```
2. Commit changes via `git commit -m "updated config"`

### How to deploy ExposureLedger smart contract
1. In the root folder setup `env.js` file:
```
module.exports = {
	LEDGER_ADDRESS_DEVELOPMENT: "",
	LEDGER_ADDRESS_MAIN: "",
	LEDGER_ADDRESS_ROPSTEN: "",
	LEDGER_ADDRESS_RINKEBY: "",
	LEDGER_ADDRESS_KOVAN: "",
	DRONE_ADDRESS: "",
	ADMIN_ADDRESS: "",
	INFURA_API_KEY: "",
	MNEMONIC: "",
	KRAKEN_API_KEY: "",
	KRAKEN_PRIVATE_KEY: "",
	GAS_AMOUNT: 1000000, 
	GAS_PRICE: 20000000000, // 20 gwei
	DB_HOST: "localhost",
	DB_NAME: "",
	DB_USERNAME: "",
	DB_PASSWORD: "",
	PLATFORM_FEE_ON_CLOSE: 550,
	CURRENCY_DELTA: 1000000,
	CURRENCY_DENOMINATION: 1000000,
	MAX_EXPOSURE_DURATION_IN_DAYS: 30,
	MAX_FUNDING: 1000000000000000000,
	TIMEZONE: "Europe/Zurich",
	BACKEND_URL: "http://localhost:8087",
	ENABLE_CRON: 1,
	DRONE_TIMEOUT: 600,
    MAX_OPEN_EXPOSURES_PER_ADDRESS: 4,
	MAX_USER_LOSS: 5000,
	MAILER_SMTP_HOST: "smtp.gmail.com",
	MAILER_ADMIN_EMAIL: "",
	MAILER_ADMIN_PASSWORD: "",
	MAILER_TARGET_EMAIL: "",
	NOTIFICATION_PRICE_NOT_RECEIVED_TIMEOUT: 3600,
	NOTIFICATION_MIN_LEDGER_UNLOCKED_BALANCE_IN_WEI: 1000000000000000000,
	NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_USD: 200,
	NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_ETH: 1,
	NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_BTC: 0.5,
	NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_BCH: 1,
	NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_LTC: 10,
	NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_XRP: 1000
};
```
* **LEDGER_ADDRESS_DEVELOPMENT**
Development ledger address. Used only with private blockhain on drone execution.
* **LEDGER_ADDRESS_MAIN**
Main net ledger address. Used on drone execution. Can be empty.
* **LEDGER_ADDRESS_ROPSTEN**
Ropsten ledger address. Used on drone execution. Can be empty.
* **LEDGER_ADDRESS_RINKEBY**
Rinkeby ledger address. Used on drone execution. Can be empty.
* **LEDGER_ADDRESS_KOVAN**
Kovan ledger address. Used on drone execution. Can be empty.
* **DRONE_ADDRESS**
Drone address. Used on drone execution and in tests.
* **ADMIN_ADDRESS**
Admin address. Used on drone execution.
* **INFURA_API_KEY**
[Infura](https://infura.io/) api key. Used for contract deployment(migration) and email notifications.
* **MNEMONIC**
12 words mnemonic phrase that can be restored to accounts private keys. Used on deployment and in tests. 1st account from mnemonic phrase will be used for deployment so there should be some ether there.
* **KRAKEN_API_KEY**
[Kraken](https://www.kraken.com) exchange api key. Used for hedging.
* **KRAKEN_PRIVATE_KEY**
[Kraken](https://www.kraken.com) exchange private key. Used for hedging.
* **GAS_AMOUNT**
Default gas amount that passed on contract function execution. Used in: `autoClose()`,`autoCloseMany()`,`badEthRateMany()`,`marginCall()`,`marginCallMany()`.
* **GAS_PRICE**
Default gas price in wei for all drone methods.
* **DB_HOST**
Database host. 
* **DB_NAME**
Database name.
* **DB_USERNAME**
Database user login.
* **DB_PASSWORD**
Database user password.
* **PLATFORM_FEE_ON_CLOSE**
Platform fee per day on CFD close. 1000000 == 100%. Used on migration.
* **CURRENCY_DELTA**
Value in % that defines valid price difference. 1000000 == 100%. Used on migration. Ex: if currency delta is 100000(10%) and user opens CFD at 200$ per ETH, then if price goes down to 179$ and drone tries to fund CFD then it will mark it as BadEthRate because 200$-10%=180$ which is min valid price.
* **CURRENCY_DENOMINATION**
Currency denomination. Most of the time should be default 1000000. Used on deployment.
* **MAX_EXPOSURE_DURATION_IN_DAYS**
Max amount of days when exposure(CFD) is "alive". After this time limit CFD will be closed by margin call time elapsed. Used on deployment.
* **MAX_FUNDING**
Max funding sum in wei per CFD. Used on deployment.
* **TIMEZONE**
Used on saving data to DB and in drone log.
* **BACKEND_URL**
Server backend url. Used in cron script that runs drone.
* **ENABLE_CRON**
Whether to enable cron. Available values: 1 and 0. 1 - enabled, 0 - disabled.
* **DRONE_TIMEOUT**
Max drone execution time in seconds before a new execution starts.
* **MAX_OPEN_EXPOSURES_PER_ADDRESS**
Max open CFD count user can have. Used on deployment.
* **MAX_USER_LOSS**
Max user loss rate. Restricts user loss. 10000 = 100%. If max user loss is 5000(50%) then if user puts 1ETH to CFD he can not lose more than 0.5ETH. Used on deployment.
* **MAILER_SMTP_HOST**
SMTP server name for sending email notifications.
* **MAILER_ADMIN_EMAIL**
Email login for SMTP server.
* **MAILER_ADMIN_PASSWORD**
Email account password.
* **MAILER_TARGET_EMAIL**
Email notifications will be sent to this address.
* **NOTIFICATION_PRICE_NOT_RECEIVED_TIMEOUT**
Timeout in seconds. If latest prices are not received during this timeout then email notification is sent.
* **NOTIFICATION_MIN_LEDGER_UNLOCKED_BALANCE_IN_WEI**
If ledger(smart contract) balance is less than this amount then email notification is sent.
* **NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_USD**
If exchange balance in USD is less than this amount then email notification is sent.
* **NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_ETH**
If exchange balance in ETH is less than this amount then email notification is sent.
* **NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_BTC**
If exchange balance in BTC is less than this amount then email notification is sent.
* **NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_BCH**
If exchange balance in BCH is less than this amount then email notification is sent.
* **NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_LTC**
If exchange balance in LTC is less than this amount then email notification is sent.
* **NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_XRP**
If exchange balance in XRP is less than this amount then email notification is sent.

2. Setup admin, drone and platform fee addresses in `migrations/2_deploy_exposure_ledger.js`:
```
return deployer.deploy(
	ExposureLedger, 
	accounts[0], // admin address, who can control drone
	accounts[0], // drone address
	accounts[0], // kraken deposit/platform fee/platform funds
	+env.PLATFORM_FEE_ON_CLOSE,  // platform fee on close, 1000000 = 100%
	currencyDelta,  // currency delta, on which rate is confirmed to be valid on funding, 10000 = 100%
	currencyDenomination, // currency denomination
	+env.MAX_EXPOSURE_DURATION_IN_DAYS, // max exposure duration in days
	+env.MAX_FUNDING, // max exposure funding sum in wei
    +env.MAX_OPEN_EXPOSURES_PER_ADDRESS, // max open exposures count user can have
	+env.MAX_USER_LOSS // max user loss rate, 10000 = 100%
);
```
3. Run migration:
```
truffle migrate --network main
```
### How to deploy drone
1. Add `ClearDB MySQL` addon on heroku.
2. Setup environment varibles on heroku according to your `env.js` file.
3. Run DB migrations:
```
cd drone/db && ../../node_modules/.bin/sequelize db:migrate && ../../node_modules/.bin/sequelize db:seed:all
```
4. Drone is a simple node script. Drone uses database to store orders that were opened on hedging. Add `Heroku Scheduler` addon to your heroku backend account and add script:
```
node drone/run-from-cron.js 1
```
Notice that `1` stands for network id. So to run drone in main net you should add: `node drone/run-from-cron.js 1`. To run drone in kovan your should add: `node drone/run-from-cron.js 42`.
5. DB is also used for storing ETH/USD and BTC/ETH prices. We need these prices to display price history graph and to show prices when kraken's `prices()` method returns error(which is quite frequently). Add to `Heroku Scheduler` script for loading prices from exchange:
```
node drone/load-exchange-prices.js
```
6. Drone sends email notifications when:
- price from exchange has not been received for too long
- error occured during any exchange operation
- ledger(smart contract) unlocked balance is too low
- exchange balances in USD, ETH, BTC, BCH, LTC, XRP are too low
To enable email notifications add the following script to cron:
```
node drone/send-email-notifications.js
```

## How to run tests?

```
npm run test
```
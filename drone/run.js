const Promise = require('bluebird');

const drone = require("./drone.js");
const kraken = require("./exchange.js");
const util = require("./util.js");

const ExposureLedgerArtifact = require("../build/contracts/ExposureLedger.json");
const ExposureLedger = web3.eth.contract(ExposureLedgerArtifact.abi);
const networkName = process.argv[5];
const fixedPrice = process.argv[6];
const exposureLedgerAddress = util.getLedgerAddressByNetworkName(networkName);
let exposureLedger = ExposureLedger.at(exposureLedgerAddress);
exposureLedger = promisifyLedger(exposureLedger);

module.exports = async (callback) => {
	try {
		const prices = await getPrices();
		const block = await getBlock("latest");
		// get all exposures to be processed somehow
		const exposuresToProcess = await drone.getExposuresToProcess(exposureLedger, prices, block.timestamp);
		// autofund
		await drone.autoFundMany(exposureLedger, exposuresToProcess.to_auto_fund);
		await drone.hedgeMany(networkName, exposureLedger, exposuresToProcess.to_auto_fund, true, prices);
		// badethrate
		await drone.badEthRateMany(exposureLedger, exposuresToProcess.to_bad_eth_rate, prices);
		// autoclose
		await drone.autoCloseMany(exposureLedger, exposuresToProcess.to_auto_close, prices);
		await drone.hedgeMany(networkName, exposureLedger, exposuresToProcess.to_auto_close, false, prices);
		// margin call time elapsed
		await drone.marginCallMany(exposureLedger, exposuresToProcess.to_margin_call_time_elapsed, drone.states.STATE_MARGIN_CALL_TIME_ELAPSED, prices);
		await drone.hedgeMany(networkName, exposureLedger, exposuresToProcess.to_margin_call_time_elapsed, false, prices);
		// margin call price movement
		await drone.marginCallMany(exposureLedger, exposuresToProcess.to_margin_call_price_movement, drone.states.STATE_MARGIN_CALL_PRICE_MOVEMENT, prices);
		await drone.hedgeMany(networkName, exposureLedger, exposuresToProcess.to_margin_call_price_movement, false, prices);
		// margin call max gain reached
		await drone.marginCallMany(exposureLedger, exposuresToProcess.to_margin_call_max_gain_reached, drone.states.STATE_MARGIN_CALL_MAX_GAIN_REACHED, prices);
		await drone.hedgeMany(networkName, exposureLedger, exposuresToProcess.to_margin_call_max_gain_reached, false, prices);
		// mark exchange as available
		await drone.setSetting(drone.settings.IS_EXCHANGE_AVAILABLE, 1);
	} catch (e) {
		drone.neatLog("drone error", e);
		await drone.setSetting(drone.settings.IS_EXCHANGE_AVAILABLE, 0);
	} finally {
		drone.neatLog("drone finished");
		callback();
	}
};

/**
 * Helpers
 */

/**
 * Returns prices based on network name and existing fixed price
 */
async function getPrices() {
	let prices = await kraken.getPrices();
	// if fixed price exists and network is not main then update prices
	if(fixedPrice != undefined && networkName != "main") {
		Object.keys(prices).map(k => { prices[k] = fixedPrice; });
	}
	return prices;
}

/**
 * Wraps sync requests in promises
 */
function promisifyLedger(exposureLedger) {
	exposureLedger.autoClose = Promise.promisify(exposureLedger.autoClose);
	exposureLedger.autoCloseMany = Promise.promisify(exposureLedger.autoCloseMany);
	exposureLedger.autoFund = Promise.promisify(exposureLedger.autoFund);
	exposureLedger.autoFundMany = Promise.promisify(exposureLedger.autoFundMany);
	exposureLedger.badEthRate = Promise.promisify(exposureLedger.badEthRate);
	exposureLedger.badEthRateMany = Promise.promisify(exposureLedger.badEthRateMany);
	exposureLedger.defaultRate = Promise.promisify(exposureLedger.defaultRate);
	exposureLedger.exchangeDeltaPercent = Promise.promisify(exposureLedger.exchangeDeltaPercent);
	exposureLedger.exposureCount = Promise.promisify(exposureLedger.exposureCount);
	exposureLedger.forceClose = Promise.promisify(exposureLedger.forceClose);
	exposureLedger.forceCloseMany = Promise.promisify(exposureLedger.forceCloseMany);
	exposureLedger.getExposureResultData = Promise.promisify(exposureLedger.getExposureResultData);
	exposureLedger.getExposureSystemData = Promise.promisify(exposureLedger.getExposureSystemData);
	exposureLedger.getExposureUserData = Promise.promisify(exposureLedger.getExposureUserData);
	exposureLedger.marginCall = Promise.promisify(exposureLedger.marginCall);
	exposureLedger.marginCallMany = Promise.promisify(exposureLedger.marginCallMany);
	return exposureLedger;
}

/**
 * Web3 methods
 */

/**
 * Returns block info
 * @param number 
 */
async function getBlock(number) {
	return new Promise((resolve, reject) => {
		web3.eth.getBlock(number, (err, result) => {
			if (err) {
				return reject(err);
			}
			resolve(result);
		});
	});
}
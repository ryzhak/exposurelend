'use strict';
module.exports = (sequelize, DataTypes) => {
  const Ticker = sequelize.define('Ticker', {
    pairId: DataTypes.INTEGER,
    lastTradePrice: DataTypes.STRING
  }, {});
  Ticker.associate = function(models) {
    // associations can be defined here
  };
  return Ticker;
};
'use strict';
module.exports = (sequelize, DataTypes) => {
  const OrderCfd = sequelize.define('OrderCfd', {
    orderId: DataTypes.INTEGER,
    cfdId: DataTypes.INTEGER
  }, {});
  OrderCfd.associate = function(models) {
    // associations can be defined here
  };
  return OrderCfd;
};
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Pair = sequelize.define('Pair', {
    name: DataTypes.STRING
  }, {});
  Pair.associate = function(models) {
    // associations can be defined here
  };
  return Pair;
};
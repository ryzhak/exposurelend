'use strict';
module.exports = (sequelize, DataTypes) => {
  const Ohlc = sequelize.define('Ohlc', {
    timestamp: DataTypes.INTEGER,
    pairId: DataTypes.INTEGER,
    closePrice: DataTypes.STRING
  }, {});
  Ohlc.associate = function(models) {
    // associations can be defined here
  };
  return Ohlc;
};
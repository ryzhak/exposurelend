'use strict';
module.exports = (sequelize, DataTypes) => {
  const DroneExecution = sequelize.define('DroneExecution', {
    pid: DataTypes.INTEGER,
    isRunning: DataTypes.INTEGER,
    nodeError: DataTypes.TEXT,
    stdError: DataTypes.TEXT,
    output: DataTypes.TEXT
  }, {});
  DroneExecution.associate = function(models) {
    // associations can be defined here
  };
  return DroneExecution;
};
'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.bulkInsert('Pairs', [
			{id: 1, name: "XETHZUSD", createdAt: "1000-01-01 00:00:00", updatedAt: "1000-01-01 00:00:00"},
			{id: 2, name: "XXBTZUSD", createdAt: "1000-01-01 00:00:00", updatedAt: "1000-01-01 00:00:00"},
			{id: 3, name: "BCHUSD", createdAt: "1000-01-01 00:00:00", updatedAt: "1000-01-01 00:00:00"},
			{id: 4, name: "XLTCZUSD", createdAt: "1000-01-01 00:00:00", updatedAt: "1000-01-01 00:00:00"},
			{id: 5, name: "XXRPZUSD", createdAt: "1000-01-01 00:00:00", updatedAt: "1000-01-01 00:00:00"}
		], {});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.bulkDelete('Pairs', null, {});
	}
};

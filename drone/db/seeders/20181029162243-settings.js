'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.bulkInsert('Settings', [
			{id: 1, name: "isExchangeAvailable", value: "1", createdAt: "1000-01-01 00:00:00", updatedAt: "1000-01-01 00:00:00"}
		], {});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.bulkDelete('Settings', null, {});
	}
};

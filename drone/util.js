const env = require("../env.js");

/**
 * Returns ledger address by network 
 */
function getLedgerAddressByNetworkName(networkName) {
	let ledgerAddress = env.LEDGER_ADDRESS_DEVELOPMENT;
	if(networkName == "main") ledgerAddress = env.LEDGER_ADDRESS_MAIN;
	if(networkName == "ropsten") ledgerAddress = env.LEDGER_ADDRESS_ROPSTEN;
	if(networkName == "rinkeby") ledgerAddress = env.LEDGER_ADDRESS_RINKEBY;
	if(networkName == "kovan") ledgerAddress = env.LEDGER_ADDRESS_KOVAN;
	return ledgerAddress;
}

module.exports = {
	getLedgerAddressByNetworkName: getLedgerAddressByNetworkName
}
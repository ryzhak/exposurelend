const http = require('http');
const https = require('https');
const env = require("../env.js");

const networkId = process.argv[2];
const url = `${env.BACKEND_URL}/start-drone?networkId=${networkId}`;

if(env.ENABLE_CRON == "1") {
	if(env.BACKEND_URL.indexOf("https") > -1) {
		https.get(url, (resp) => {});
	} else {
		http.get(url, (resp) => {});
	}
}
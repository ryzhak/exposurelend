const express = require('express');
const app = express();
const { exec } = require('child_process');

const env = require('../env.js');
const kraken = require('./exchange.js');
const drone = require('./drone.js');
const Sequelize = require('sequelize');
const DataTypes = require('sequelize/lib/data-types');
const sequelize = new Sequelize(env.DB_NAME, env.DB_USERNAME, env.DB_PASSWORD, {
	host: env.DB_HOST,
	dialect: 'mysql',
	logging: false,
	timezone: env.TIMEZONE
});
const DroneExecution = require('./db/models/droneexecution.js')(sequelize, DataTypes);
const Order = require('./db/models/order.js')(sequelize, DataTypes);
const OrderCfd = require('./db/models/ordercfd.js')(sequelize, DataTypes);
const Ohlc = require('./db/models/ohlc.js')(sequelize, DataTypes);
const Pair = require('./db/models/pair.js')(sequelize, DataTypes);
const Ticker = require('./db/models/ticker.js')(sequelize, DataTypes);

app.use((req, res, next) => {
    res.append('Access-Control-Allow-Origin', ['*']);
    res.append('Content-Type', 'application/json');
    next();
});

app.get('/start-drone', (req, res) => {
	let networkName = getNetworkName(req.query.networkId);
	let fixedPrice = req.query.fixedPrice;
	startDrone(networkName, fixedPrice);
    res.send(JSON.stringify({ result: 'OK' }));
});

app.get('/drone-log', async (req, res) => {
	let latestDroneExecution = await DroneExecution.findOne({order: [['id', 'DESC']]});
    res.send(JSON.stringify({ result: latestDroneExecution }));
});

app.get('/exchange-available', async (req, res) => {
	let setting = await drone.getSetting(drone.settings.IS_EXCHANGE_AVAILABLE);
    res.send(JSON.stringify(setting));
});

app.get('/exchange-balance', async (req, res) => {
	let result = (await kraken.getBalance()).result;
    res.send(JSON.stringify({ result: result }));
});

app.get('/explicit-close', (req, res) => {
	let networkName = getNetworkName(req.query.networkId);
	let exposureIndex = req.query.exposureIndex;
	explicitClose(networkName, exposureIndex);
    res.send(JSON.stringify({ result: 'OK' }));
});

app.get('/explicit-fund', (req, res) => {
	let networkName = getNetworkName(req.query.networkId);
	let exposureIndex = req.query.exposureIndex;
	explicitFund(networkName, exposureIndex);
    res.send(JSON.stringify({ result: 'OK' }));
});

app.get('/order', async (req, res) => {
	let orders = await Order.findAll({
		order: [['id', 'DESC']],
		limit: 5
	});
	res.send(JSON.stringify({ result: orders }));
});

app.get('/prices', async (req, res) => {
	let prices = await kraken.getPrices();
	res.send(JSON.stringify(prices));
});

app.get('/prices-cached', async(req, res) => {
	let prices = {};
	try {
		// try to get prices from kraken
		prices = await kraken.getPrices();
	} catch(e) {
		// on error get prices from db
		let ethUsdTicker = await Ticker.findOne({where: {pairId: 1},order: [['id', 'DESC']],limit: 1});
		let btcUsdTicker = await Ticker.findOne({where: {pairId: 2},order: [['id', 'DESC']],limit: 1});
		let bchUsdTicker = await Ticker.findOne({where: {pairId: 3},order: [['id', 'DESC']],limit: 1});
		let ltcUsdTicker = await Ticker.findOne({where: {pairId: 4},order: [['id', 'DESC']],limit: 1});
		let xrpUsdTicker = await Ticker.findOne({where: {pairId: 5},order: [['id', 'DESC']],limit: 1});
		prices = {
			ETH: ethUsdTicker.lastTradePrice,
			BTC: btcUsdTicker.lastTradePrice,
			BCH: bchUsdTicker.lastTradePrice,
			LTC: ltcUsdTicker.lastTradePrice,
			XRP: xrpUsdTicker.lastTradePrice
		};
		prices = drone.applyIndexesToPrices(prices);
	}
	res.send(JSON.stringify(prices));
});

app.get('/price-history', async (req, res) => {
	let pairModel = await Pair.findById(req.query.pairId);
	if(!pairModel) res.status(400).send(JSON.stringify({error: "Pair not found"}))
	let ohlc = await Ohlc.findAll({
		where: { pairId: pairModel.id },
		order: [['id', 'DESC']],
		limit: 48
	});
	ohlc = ohlc.reverse();
	res.send(JSON.stringify(ohlc));
});

app.listen(process.env.PORT || 8087, () => console.log('Example app listening on port 8087'));

function explicitClose(networkName, exposureIndex) {
	const cmd = `${__dirname}/../node_modules/.bin/truffle exec ${__dirname}/../drone/autoclose.js --network ${networkName} ${exposureIndex}`;
	exec(cmd);
}

function explicitFund(networkName, exposureIndex) {
	const cmd = `${__dirname}/../node_modules/.bin/truffle exec ${__dirname}/../drone/autofund.js --network ${networkName} ${exposureIndex}`;
	exec(cmd);
}

function getNetworkName(networkId) {
	let networkName = "development";
	if(networkId == 1) networkName = "main";
	if(networkId == 3) networkName = "ropsten";
	if(networkId == 4) networkName = "rinkeby";
	if(networkId == 42) networkName = "kovan";
	return networkName;
}

async function startDrone(networkName, fixedPrice) {
	// validation
	if(fixedPrice == undefined) fixedPrice = '';

	const latestDroneExecution = await DroneExecution.findOne({order: [['id', 'DESC']]});

	// if latest dron execution is running too long then stop it manually
	if(latestDroneExecution) {
		const createdAt = Math.round(new Date(latestDroneExecution.createdAt).getTime() / 1000);
		const now = Math.round(new Date().getTime() / 1000);
		if(now - createdAt > env.DRONE_TIMEOUT) {
			await latestDroneExecution.update({isRunning: 0});
		}
	}

	// if latest drone execution exists and is running then return and dont start new execution
	if(latestDroneExecution && latestDroneExecution.isRunning) return;

	// start drone execution
	const cmd = `${__dirname}/../node_modules/.bin/truffle exec ${__dirname}/../drone/run.js --network ${networkName} ${fixedPrice}`;
	const execRes = exec(cmd, async (err, stdout, stderr) => {
		// update latest drone execution
		const latestDroneExecution = await DroneExecution.findOne({order: [['id', 'DESC']]});
		await latestDroneExecution.update({
			isRunning: 0,
			nodeError: err,
			stdError: stderr,
			output: stdout
		});
	});

	// save drone execution to db
	await DroneExecution.create({pid: execRes.pid, isRunning: 1});
}
const moment = require("moment");
const db = require("./db/models");
const kraken = require("./exchange.js");

// for all pairs
db.Pair.findAll().then((pairs) => {

	let pairParam = [];
	pairs.map(p => { pairParam.push(p.name) });
	pairParam = pairParam.join(',');
	
	// get ticker info
	kraken.getTicker(pairParam).then(tickerInfo => {

		// collect save ticker to db requests
		let saveTickerRequests = [];
		Object.keys(tickerInfo.result).map(tickerName => {
			// for each ticker name in response find pair model
			let pairModel = null;
			for(let i = 0; i < pairs.length; i++) {
				if(pairs[i].name == tickerName) pairModel = pairs[i];
			}
			if(!pairModel) throw new Error("Pair model can not be null");
			// get last trade price
			let lastTradePrice = tickerInfo.result[tickerName].c[0];
			saveTickerRequests.push(db.Ticker.create({
				pairId: pairModel.id,
				lastTradePrice: lastTradePrice
			}));
		});

		// save tickers
		Promise.all(saveTickerRequests).then(() => {
			
			// get OHLC data
			let getOHLCRequests = [];
			let since = moment().subtract(1,"day").unix();
			pairs.map(pairModel => { getOHLCRequests.push(kraken.getOHLC(pairModel.name, 30, since)); });
			Promise.all(getOHLCRequests).then((ohlcData) => {
				// collect save ohlc requests
				let saveOhlcRequests = [];
				// for each pair in response
				for(let i = 0; i < ohlcData.length; i++) {
					// for each history data in pair
					for(let j = 0; j < ohlcData[i].result[pairs[i].name].length; j++) {
						console.log(pairs[i].name);
						saveOhlcRequests.push(db.Ohlc.findOrCreate({
							where: {
								timestamp: ohlcData[i].result[pairs[i].name][j][0],
								pairId: pairs[i].id
							},
							defaults: {
								closePrice: ohlcData[i].result[pairs[i].name][j][4]
							}
						}));
					}
				}

				// save ohlc history
				Promise.all(saveOhlcRequests).then(() => {
					process.exit();
				});

			});

		});

	}).catch((e) => {
		console.log(e);
		process.exit(1);
	});

});

const env = require('../env.js');
const kraken = require("./exchange.js");

// db connection
const Sequelize = require('sequelize');
const DataTypes = require('sequelize/lib/data-types');
const sequelize = new Sequelize(env.DB_NAME, env.DB_USERNAME, env.DB_PASSWORD, {
	host: env.DB_HOST,
	dialect: 'mysql',
	logging: false,
	timezone: env.TIMEZONE
});
const Order = require('./db/models/order.js')(sequelize, DataTypes);
const OrderCfd = require('./db/models/ordercfd.js')(sequelize, DataTypes);
const Setting = require('./db/models/setting.js')(sequelize, DataTypes);

const CURRENCIES = { USD: 0 };

const ASSETS = {
	ETH: 0,
	BTC: 1,
	INDEX1: 2
}

const INDEX_BASKET = {
	INDEX1: ["XETHZUSD","XXBTZUSD","BCHUSD","XLTCZUSD","XXRPZUSD"]
}

const LEVERAGES = {
	OneToOne: 0,
	TwoToOne: 1,
	FourToOne: 2
}

const STATES = {
	STATE_FUNDED_BY_USER: 0,
	STATE_BAD_ETH_RATE: 1,
	STATE_FUNDED_BY_PLATFORM: 2,
	STATE_CLOSED_BY_USER: 3,
	STATE_MARGIN_CALL_TIME_ELAPSED: 4,
	STATE_MARGIN_CALL_PRICE_MOVEMENT: 5,
	STATE_MARGIN_CALL_MAX_GAIN_REACHED: 6,
	STATE_CLOSED_BY_ADMIN: 7
};

const SETTINGS = {
	IS_EXCHANGE_AVAILABLE: 1
};

const adminAddress = env.ADMIN_ADDRESS;
const droneAddress = env.DRONE_ADDRESS;

/**
 * Drone methods
 */

/**
 * Applies indexes to prices from exchange 
 */
function applyIndexesToPrices(prices) {
	prices['INDEX1'] = String((+prices.BTC + +prices.ETH + +prices.BCH + +prices.LTC + +prices.XRP) / INDEX_BASKET.INDEX1.length);
	return prices;
}

/**
 * Autocloses exposure by index 
 */
async function autoClose(exposureLedger, index, prices) {
	const realPrice = await getRealPrice(exposureLedger, index, prices) * 100;
	neatLog("autoclose CFD", `#${index}`);
	await exposureLedger.autoClose(index, realPrice, {from: droneAddress, gasPrice: env.GAS_PRICE, gas: env.GAS_AMOUNT});
}

/**
 * Autocloses many exposures by indexes array 
 */
async function autoCloseMany(exposureLedger, indexes, prices) {
	if(indexes.length == 0) return false;
	neatLog("autoclosing many CFDs", indexes.join(","));
	let assets = Object.values(ASSETS);
	for(let i = 0; i < assets.length; i++) {
		let indexesToProcess = await getExposureIndexesByAsset(exposureLedger, indexes, assets[i]);
		if(indexesToProcess.length > 0) {
			let realPrice = await getRealPriceByAsset(prices, assets[i]) * 100;
			await exposureLedger.autoCloseMany(indexesToProcess, realPrice, {from: droneAddress, gasPrice: env.GAS_PRICE, gas: env.GAS_AMOUNT});
		}
	}
	return true;
}

/**
 * Autofunds exposure by index 
 */
async function autoFund(exposureLedger, index) {
	neatLog("autofund CFD", `#${index}`);
	await exposureLedger.autoFund(index, {from: droneAddress, gasPrice: env.GAS_PRICE});
}

/**
 * Auto funds many exposures by indexes array
 */
async function autoFundMany(exposureLedger, indexes) {
	if(indexes.length == 0) return false;
	neatLog("autofunding many CFDs", indexes.join(","));
	await exposureLedger.autoFundMany(indexes, {from: droneAddress, gasPrice: env.GAS_PRICE});
	return true;
}

/**
 * Marks exposure as invalid 
 */
async function badEthRate(exposureLedger, index, prices) {
	const realPrice = await getRealPrice(exposureLedger, index, prices) * 100;
	neatLog("bad eth rate CFD", `#${index}`);
	await exposureLedger.badEthRate(index, realPrice, {from: droneAddress, gasPrice: env.GAS_PRICE});
}

/**
 * Bad eth rate many exposures by indexes array 
 */
async function badEthRateMany(exposureLedger, indexes, prices) {
	if(indexes.length == 0) return false;
	neatLog("bad eth rate many CFDs", indexes.join(","));
	let assets = Object.values(ASSETS);
	for(let i = 0; i < assets.length; i++) {
		let indexesToProcess = await getExposureIndexesByAsset(exposureLedger, indexes, assets[i]);
		if(indexesToProcess.length > 0) {
			let realPrice = await getRealPriceByAsset(prices, assets[i]) * 100;
			await exposureLedger.badEthRateMany(indexesToProcess, realPrice, {from: droneAddress, gasPrice: env.GAS_PRICE, gas: env.GAS_AMOUNT});
		}
	}
	return true;
}

/**
 * Checks whether exposure is in provided state 
 */
async function exposureInState(exposureLedger, index, state) {
	const exposureSystemData = await exposureLedger.getExposureSystemData(index);
	return exposureSystemData[1].toNumber() == state;
}

/**
 * Force close exposure by index 
 */
async function forceClose(exposureLedger, index, prices) {
	const realPrice = await getRealPrice(exposureLedger, index, prices) * 100;
	neatLog("force close CFD", `#${index}`);
	await exposureLedger.forceClose(index, realPrice, {from: adminAddress, gasPrice: env.GAS_PRICE, gas: env.GAS_AMOUNT});
}

/**
 * Force close many exposures by indexes array 
 */
async function forceCloseMany(exposureLedger, indexes, prices) {
	if(indexes.length == 0) return false;
	neatLog("force close many CFDs", indexes.join(","));
	let assets = Object.values(ASSETS);
	for(let i = 0; i < assets.length; i++) {
		let indexesToProcess = await getExposureIndexesByAsset(exposureLedger, indexes, assets[i]);
		if(indexesToProcess.length > 0) {
			let realPrice = await getRealPriceByAsset(prices, assets[i]) * 100;
			await exposureLedger.forceCloseMany(indexesToProcess, realPrice, {from: adminAddress, gasPrice: env.GAS_PRICE, gas: env.GAS_AMOUNT});
		}
	}
	return true;
}

/**
 * Returns exposure indexes of specified asset from given indexes 
 */
async function getExposureIndexesByAsset(exposureLedger, indexes, asset) {
	let indexesByAsset = [];
	for(let i = 0; i < indexes.length; i++) {
		let exposureSystemData = await exposureLedger.getExposureSystemData(indexes[i]);
		if(exposureSystemData[4].toNumber() == asset) indexesByAsset.push(indexes[i]);
	}
	return indexesByAsset;
}

/**
 * Returns all exposures that need to be processed in current drone cycle 
 */
async function getExposuresToProcess(exposureLedger, prices, blockTimestamp) {
	let res = {
		to_auto_fund: [],
		to_bad_eth_rate: [],
		to_auto_close: [],
		to_margin_call_time_elapsed: [],
		to_margin_call_price_movement: [],
		to_margin_call_max_gain_reached: []
	};

	// loop through all exposures
	const exposureCount = (await exposureLedger.exposureCount()).toNumber();
	for(let i = 0; i < exposureCount; i++) {

		// if state is funded by platform
		if(await exposureInState(exposureLedger, i, STATES.STATE_FUNDED_BY_PLATFORM)) {

			// if exposure is going to be closed then autoclose
			const exposureUserData = await exposureLedger.getExposureUserData(i);
			const isGoingToBeClosed = exposureUserData[5];
			if(isGoingToBeClosed) {
				res.to_auto_close.push(i);
			}

			// if time elapsed then margin call time elapsed
			if(await timeElapsed(exposureLedger, i, blockTimestamp)) {
				res.to_margin_call_time_elapsed.push(i);
			}

			// if max user loss then margin call price movement
			if(await maxUserLoss(exposureLedger, i, prices)) {
				res.to_margin_call_price_movement.push(i);
			}

			// if max user gain then margin call max gain reached
			if(await maxUserGain(exposureLedger, i, prices)) {
				res.to_margin_call_max_gain_reached.push(i);
			}

		}

		// if state is funded by user
		if(await exposureInState(exposureLedger, i, STATES.STATE_FUNDED_BY_USER)) {
			const valid = await validateExposure(exposureLedger, i, prices);
			if(valid) {
				// autofund
				res.to_auto_fund.push(i);
			} else {
				// badethrate
				res.to_bad_eth_rate.push(i);
			}
		}
	}

	return res;
}

/**
 * Returns leverage coefficient. Ex: for 1:1 coef = 1, for 2:1 coef = 2, etc... 
 */
async function getLeverageCoef(exposureLedger, index) {
	let coef = null;
	const exposureSystemData = await exposureLedger.getExposureSystemData(index);
	const leverage = exposureSystemData[3].toNumber();
	if(leverage == LEVERAGES.OneToOne) coef = 1;
	if(leverage == LEVERAGES.TwoToOne) coef = 2;
	if(leverage == LEVERAGES.FourToOne) coef = 4;
	if(!coef) throw new Error("Leverate coef can not be null");
	return coef;
}

/**
 * Returns volume of asset or array of volumes in case of index to buy/sell on exchange(without leverage)
 */
async function getOrderVolume(exposureLedger, index, prices) {
	let volume = null;
	const exposureResultData = await exposureLedger.getExposureResultData(index);
	const exposureSystemData = await exposureLedger.getExposureSystemData(index);
	const asset = exposureSystemData[4].toNumber();
	const initialUserFundInEth = exposureResultData[1].toNumber() / Math.pow(10, 18);
	const initialUserFundInUsd = initialUserFundInEth * +prices.ETH;

	if(asset == ASSETS.ETH) {
		volume = initialUserFundInEth;
	}

	if(asset == ASSETS.BTC) {
		volume = initialUserFundInUsd / +prices.BTC;
	}

	if(asset == ASSETS.INDEX1) {
		const assetAmountInUsd = initialUserFundInUsd / INDEX_BASKET.INDEX1.length;
		volume = [
			assetAmountInUsd / +prices.ETH,
			assetAmountInUsd / +prices.BTC,
			assetAmountInUsd / +prices.BCH,
			assetAmountInUsd / +prices.LTC,
			assetAmountInUsd / +prices.XRP
		];
	}
	
	if(!volume) throw new Error('volume can not be null');
	
	return volume;
}

/**
 * Returns price rate diff between open and real price 
 */
async function getPriceRateDiff(exposureLedger, index, prices) {
	const exposureUserData = await exposureLedger.getExposureUserData(index);
	const realPrice = await getRealPrice(exposureLedger, index, prices) * 100;
	const openPrice = exposureUserData[2].toNumber();
	return (realPrice / openPrice) * 100;
}

/**
 * Returns real price depending on exposure asset 
 */
async function getRealPrice(exposureLedger, index, prices) {
	const exposureSystemData = await exposureLedger.getExposureSystemData(index);
	const asset = exposureSystemData[4].toNumber();
	switch(asset) {
		case ASSETS.ETH: return +prices.ETH;
		case ASSETS.BTC: return +prices.BTC;
		case ASSETS.INDEX1: return +prices.INDEX1;
		default: console.log(`Price not found for asset ${asset}`);
	}
}

/**
 * Returns real price by asset 
 */
async function getRealPriceByAsset(prices, asset) {
	switch(asset) {
		case ASSETS.ETH: return +prices.ETH;
		case ASSETS.BTC: return +prices.BTC;
		case ASSETS.INDEX1: return +prices.INDEX1;
		default: console.log(`Asset ${asset} not found`);
	}
}

/**
 * Gets setting parameter 
 */
async function getSetting(settingId) {
	return await Setting.findById(settingId);
}

/**
 * Returns exchange ticker name or array of tickers in case of index by asset id 
 */
function getTickerByAsset(asset) {
	switch(asset) {
		case ASSETS.ETH: return "XETHZUSD";
		case ASSETS.BTC: return "XXBTZUSD";
		case ASSETS.INDEX1: return INDEX_BASKET.INDEX1;
		default: console.log(`Asset ${asset} not found`);
	}
}

/**
 * Hedges position by exposure index
 */
async function hedge(networkName, exposureLedger, index, isHedgeOpening, prices) {
	const exposureUserData = await exposureLedger.getExposureUserData(index);
	const exposureSystemData = await exposureLedger.getExposureSystemData(index);
	const asset = exposureSystemData[4].toNumber();
	const isLong = exposureUserData[4];
	const leverageCoef = await getLeverageCoef(exposureLedger, index);
	let tickers = await getTickerByAsset(asset);
	if(!Array.isArray(tickers)) tickers = [tickers];
	let volumes = await getOrderVolume(exposureLedger, index, prices);
	if(!Array.isArray(volumes)) volumes = [volumes];
	// apply leverage
	for(let i = 0; i < volumes.length; i++) {
		volumes[i] = volumes[i] * leverageCoef;
	}

	let type;
	if(isLong && isHedgeOpening || !isLong && !isHedgeOpening) {
		type = 'buy';
	} else {
		type = 'sell';
	}

	if(networkName == "main") {
		try {
			for(let i = 0; i < tickers.length; i++) {
				// create order on exchange
				const respOrder = await kraken.addOrder(tickers[i], type, "market", volumes[i]);
				// save order to db
				const order = (await Order.findOrCreate({where: {
					txid: respOrder.result.txid[0],
					descr: respOrder.result.descr.order,
					status: 'open'
				}}))[0];
				// link cfd with order
				await OrderCfd.findOrCreate({where: {
					orderId: order.id,
					cfdId: index
				}});
			}
		} catch(e) {
			neatLog(`error on creating hedge for cfd #${index}`);
			await forceClose(exposureLedger, index, prices);
			await setSetting(SETTINGS.IS_EXCHANGE_AVAILABLE, 0);
		}
	} else {
		neatLog("simulating hedging");
	}
	if(type == "buy") {
		for(let i = 0; i < tickers.length; i++) { 
			neatLog(`buying ${tickers[i]} with USD`, volumes[i]); 
		}
	} else {
		for(let i = 0; i < tickers.length; i++) { 
			neatLog(`selling ${tickers[i]} for USD`, volumes[i]); 
		}
	}

	return {
		type: type,
		tickers: tickers,
		volumes: volumes
	};
}

/**
 * Hedges many exposures by indexes array
 */
async function hedgeMany(networkName, exposureLedger, indexes, isHedgeOpening, prices) {
	if(indexes.length == 0) return false;
	let hedgeRes = [];
	for(let i = 0; i < indexes.length; i++) {
		hedgeRes.push(await hedge(networkName, exposureLedger, indexes[i], isHedgeOpening, prices));
	}
	return hedgeRes;
}

/**
 * Closes exposure by margin call 
 */
async function marginCall(exposureLedger, index, newState, prices) {
	const realPrice = await getRealPrice(exposureLedger, index, prices) * 100;
	let marginCallReason = "";
	if(newState == STATES.STATE_MARGIN_CALL_TIME_ELAPSED) marginCallReason = "time elapsed";
	if(newState == STATES.STATE_MARGIN_CALL_PRICE_MOVEMENT) marginCallReason = "price movement";
	if(newState == STATES.STATE_MARGIN_CALL_MAX_GAIN_REACHED) marginCallReason = "max gain reached";
	neatLog("margin call CFD", `#${index}`, marginCallReason);
	await exposureLedger.marginCall(index, realPrice, newState, {from: droneAddress, gasPrice: env.GAS_PRICE, gas: env.GAS_AMOUNT});
}

/**
 * Closes many exposures by margin call
 */
async function marginCallMany(exposureLedger, indexes, newState, prices) {
	if(indexes.length == 0) return false;
	let marginCallReason = "";
	if(newState == STATES.STATE_MARGIN_CALL_TIME_ELAPSED) marginCallReason = "time elapsed";
	if(newState == STATES.STATE_MARGIN_CALL_PRICE_MOVEMENT) marginCallReason = "price movement";
	if(newState == STATES.STATE_MARGIN_CALL_MAX_GAIN_REACHED) marginCallReason = "max gain reached";
	neatLog("margin call many CFDs", indexes.join(","), marginCallReason);
	let assets = Object.values(ASSETS);
	for(let i = 0; i < assets.length; i++) {
		let indexesToProcess = await getExposureIndexesByAsset(exposureLedger, indexes, assets[i]);
		if(indexesToProcess.length > 0) {
			let realPrice = await getRealPriceByAsset(prices, assets[i]) * 100;
			await exposureLedger.marginCallMany(indexesToProcess, realPrice, newState, {from: droneAddress, gasPrice: env.GAS_PRICE, gas: env.GAS_AMOUNT});
		}
	}
	return true;
}

/**
 * Checks whether user has won everything in exposure
 */
async function maxUserGain(exposureLedger, index, prices) {
	const borderRateForMarginCall = 1;
	const exposureUserData = await exposureLedger.getExposureUserData(index);
	const isLong = exposureUserData[4];
	const rateDiff = await getPriceRateDiff(exposureLedger, index, prices);
	if(rateDiff <= borderRateForMarginCall && !isLong || rateDiff >= 200 - borderRateForMarginCall && isLong) {
		return true;
	}
	return false;
}

/**
 * Checks whether user has lost everything in exposure
 */
async function maxUserLoss(exposureLedger, index, prices) {
	const leverageCoef = await getLeverageCoef(exposureLedger, index);
	const borderRateForMarginCall = 50 / leverageCoef;
	const exposureUserData = await exposureLedger.getExposureUserData(index);
	const isLong = exposureUserData[4];
	const rateDiff = await getPriceRateDiff(exposureLedger, index, prices);
	if(rateDiff <= borderRateForMarginCall && isLong || rateDiff >= 200 - borderRateForMarginCall && !isLong) {
		return true;
	}
	return false;
}

/**
 * Checks whether exposure at index is already hedged 
 */
async function needHedge(index, type) {
	let needHedge = true;
	const orderCfds = await OrderCfd.findAll({where: {cfdId: index}});
	for(let i = 0; i < orderCfds.length; i++) {
		const order = await Order.findById(orderCfds[i].orderId);
		if(order.descr.indexOf(type) != -1) {
			needHedge = false;
			break;
		}
	}
	return needHedge;
}

/**
 * Returns neat message
 */
function neatLog(theme, ...additional) {
	const msg = `${new Date().toLocaleString("en-US", {timeZone: env.TIMEZONE})} ${theme} ${additional}`;
	console.log(msg);
	return msg;
}

/**
 * Sets setting parameter 
 */
async function setSetting(settingId, value) {
	const setting = await Setting.findById(settingId);
	setting.value = value;
	await setting.save();
}

/**
 * Checks whether exposure duration elapsed 
 */
async function timeElapsed(exposureLedger, index, now) {
	const exposureSystemData = await exposureLedger.getExposureSystemData(index);
	const exposureUserData = await exposureLedger.getExposureUserData(index);
	const durationInDays = exposureUserData[3].toNumber();
	const createdAt = exposureSystemData[0].toNumber();
	return (now - createdAt) / (60 * 60 * 24) > durationInDays;
}

/**
 * Checks whether exposure is valid
 */
async function validateExposure(exposureLedger, index, prices) {
	const exposureUserData = await exposureLedger.getExposureUserData(index);
	const currency = exposureUserData[6].toNumber();
	const openPrice = exposureUserData[2].toNumber();
	const realPrice = await getRealPrice(exposureLedger, index, prices) * 100;
	const defaultRate = (await exposureLedger.defaultRate()).toNumber();
	const exchangeDeltaPercent = (await exposureLedger.exchangeDeltaPercent()).toNumber();
	const validPriceDiff = openPrice / defaultRate * exchangeDeltaPercent;
	const minValidOpenPrice = openPrice - validPriceDiff;
	const maxValidOpenPrice = openPrice + validPriceDiff;
	if(realPrice < minValidOpenPrice || realPrice > maxValidOpenPrice) {
		return false;
	}
	return true;
}

module.exports = {
	applyIndexesToPrices: applyIndexesToPrices,
	assets: ASSETS,
	autoClose: autoClose,
	autoCloseMany: autoCloseMany,
	autoFund: autoFund,
	autoFundMany: autoFundMany,
	badEthRate: badEthRate,
	badEthRateMany: badEthRateMany,
	currencies: CURRENCIES,
	exposureInState: exposureInState,
	forceClose: forceClose,
	forceCloseMany: forceCloseMany,
	getExposureIndexesByAsset: getExposureIndexesByAsset,
	getExposuresToProcess: getExposuresToProcess,
	getLeverageCoef: getLeverageCoef,
	getOrderVolume: getOrderVolume,
	getPriceRateDiff: getPriceRateDiff,
	getRealPrice: getRealPrice,
	getRealPriceByAsset: getRealPriceByAsset,
	getSetting: getSetting,
	getTickerByAsset: getTickerByAsset,
	hedge: hedge,
	hedgeMany: hedgeMany,
	indexBasket: INDEX_BASKET,
	leverages: LEVERAGES,
	marginCall: marginCall,
	marginCallMany: marginCallMany,
	maxUserGain: maxUserGain,
	maxUserLoss: maxUserLoss,
	neatLog: neatLog,
	needHedge: needHedge,
	settings: SETTINGS,
	setSetting: setSetting,
	states: STATES,
	timeElapsed: timeElapsed,
	validateExposure: validateExposure
}
const KrakenClient = require('kraken-api');
const env = require('../env.js');

const kraken = new KrakenClient(env.KRAKEN_API_KEY, env.KRAKEN_PRIVATE_KEY, {timeout: 15000});

module.exports = {
	addOrder: addOrder,
	getBalance: getBalance,
	getOHLC: getOHLC,
	getPrices: getPrices,
	getTicker: getTicker
}

/**
 * Public API
 */

/**
 * Returns available asset pairs
 */
async function getAssetPairs() {
	return await kraken.api('AssetPairs');
}

/**
 * Returns OHLC price history
 * Ex: await getOHLC("ETHUSD", "30", 1537106400);
 * Resp:
 * {
 *   "error": [],
 *   "result": {
 *       "XETHZUSD": [
 *           [
 *               1537108200,
 *               "215.93",
 *               "217.42",
 *               "215.28",
 *               "217.00",
 *               "216.45",
 *               "677.77813453",
 *               213
 *           ],
 *           [
 *               1537110000,
 *               "217.00",
 *               "217.01",
 *               "216.00",
 *               "216.00",
 *               "216.58",
 *               "160.53225482",
 *               64
 *			]
 *		]
 *	  }
 *  }
 */
async function getOHLC(pair, interval, since) {
	return await kraken.api('OHLC', {pair: pair, interval: interval, since: since});
}

/**
 * Returns latest close prices 
 * Resp:
 * {
 *   "ETH": "198.18000",
 *   "BTC": "6343.20000",
 *   "BCH": "423.300000",
 *   "LTC": "50.42000",
 *   "XRP": "0.45680000"
 * }
 */
async function getPrices() {
	const pair = "XETHZUSD,XXBTZUSD,BCHUSD,XLTCZUSD,XXRPZUSD";
	const resp = await kraken.api('Ticker', {pair: pair});
	let prices = {
		ETH: resp.result.XETHZUSD.c[0],
		BTC: resp.result.XXBTZUSD.c[0],
		BCH: resp.result.BCHUSD.c[0],
		LTC: resp.result.XLTCZUSD.c[0],
		XRP: resp.result.XXRPZUSD.c[0]
	};
	const drone = require('./drone.js');
	prices = drone.applyIndexesToPrices(prices);
	return prices;
}

/**
 * Returns ticker info
 * Ex: await kraken.getTicker("XETHZUSD");
 * Resp:
 * { error: [],
 * result:
 *  { XETHZUSD:
 *     { a: [Array],
 *       b: [Array],
 *       c: [Array],
 *       v: [Array],
 *       p: [Array],
 *       t: [Array],
 *       l: [Array],
 *       h: [Array],
 *       o: '224.58000' 
 *     } 
 *  } 
 * }
 */
async function getTicker(pair) {
	return await kraken.api('Ticker', {pair: pair})
}

/**
 * Returns exchange server time
 */
async function getTime() {
	return await kraken.api('Time');
}

/**
 * Private API
 */

/**
 * Places an order for buy or sell
 * Ex: await addOrder("ETHUSD", "buy", "market", "0.02");
 * Ex: await addOrder("ETHUSD", "sell", "market", "0.02");
 * Resp: 
 * { error: [],
 * 	 result:
 *  	{ 
 *          descr: { order: 'buy 0.02000000 ETHUSD @ market' },
 *	 	    txid: [ 'OZXIBP-IWJTW-D4K6VZ' ] 
 *	     } 
 *	}
 */
async function addOrder(pair, type, ordertype, volume) {
	return await kraken.api('AddOrder', {
		pair: pair,
		type: type,
		ordertype: ordertype,
		volume: volume
	});
}

/**
 * Returns closed orders by offset 
 * Ex: await closedOrders();
 * Resp:
 *     { 
 *         error: [],
 *         result:
 *         { closed:
 *             { 'OTZG67-QF3ZN-ND4MGT': [Object],
 *               'OPE32N-NAXQH-IHIQFB': [Object] 
 *             },
 *            count: 25 
 *         } 
 *     }
 */
async function closedOrders(offset = 0) {
	return await kraken.api('ClosedOrders', {
		ofs: offset
	});
}

/**
 * Returns balances
 */
async function getBalance() {
	return await kraken.api('Balance');
}


/**
 * Returns open orders
 * Ex: await openOrders();
 * Resp:
 *     { 
 *         error: [],
 *         result:
 *         { open:
 *             { 'OTZG67-QF3ZN-ND4MGT': [Object],
 *               'OPE32N-NAXQH-IHIQFB': [Object] 
 *             },
 *            count: 25 
 *         } 
 *     }
 */
async function openOrders() {
	return await kraken.api('OpenOrders');
}

/**
 * Query orders
 * Ex: await queryOrders(["OZXIBP-IWJTW-D4K6VZ"]);
 */
async function queryOrders(txIdArray) {
	return await kraken.api('QueryOrders', {
		txid: txIdArray
	});
}

/**
 * Query trades
 * Ex: await queryTrades(["OZXIBP-IWJTW-D4K6VZ"]);
 */
async function queryTrades(txIdArray) {
	return await kraken.api('QueryTrades', {
		txid: txIdArray
	});
}

/**
 * Withdraws assets 
 * Ex: await withdraw("ETH", "account1", "0.01");
 * Resp:
 * { error: [], result: { refid: 'A2BDB4T-JYJUBH-IFLOVD' } }
 */
async function withdraw(asset, key, amount) {
	return await kraken.api('Withdraw', {
		asset: asset,
		key: key,
		amount: amount
	});
}

/**
 * Checks withdraw status
 * Ex: await withdrawStatus("ETH");
 * Resp:
 * {
 *		error: [
 *			
 *		],
 *		result: [
 *			{
 *				method: 'Ether',
 *				aclass: 'currency',
 *				asset: 'XETH',
 *				refid: 'A2BYRVB-IJWFEL-7YUGSN',
 *				txid: null,
 *				info: '0x10136b9e25aad1389c79fe146f2d1e7338342121',
 *				amount: '0.0050000000',
 *				fee: '0.0050000000',
 *				time: 1536649388,
 *				status: 'Initial'
 *			},
 *			{
 *				method: 'Ether',
 *				aclass: 'currency',
 *				asset: 'XETH',
 *				refid: 'A2BDVF3-U7ID3N-ZMABC4',
 *				txid: '0x3dc43aaf33160fe19d75673242dae490aab2f0012cd986719046b746dd99bc20',
 *				info: '0x10136b9e25aad1389c79fe146f2d1e7338342121',
 *				amount: '0.0352189800',
 *				fee: '0.0050000000',
 *				time: 1535744225,
 *				status: 'Success'
 *			}
 *		]
 *	} 
 */
async function withdrawStatus(asset) {
	return await kraken.api('WithdrawStatus', {
		asset: asset
	});
}
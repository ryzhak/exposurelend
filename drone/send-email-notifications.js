const nodemailer = require('nodemailer');
const Web3 = require('web3');
const db = require("./db/models");
const drone = require("./drone.js");
const env = require("../env.js");
const kraken = require("./exchange.js");
const web3 = new Web3(new Web3.providers.HttpProvider(`https://mainnet.infura.io/${env.INFURA_API_KEY}`));

const ExposureLedgerArtifact = require("../build/contracts/ExposureLedger.json");
const exposureLedger = new web3.eth.Contract(ExposureLedgerArtifact.abi, env.LEDGER_ADDRESS_MAIN);

// start
run();

/**
 * Helper functions
 */

/**
 * Notifies admin if price has not been received from exchange for 1 hour
 */
async function notifyOnPriceNotReceived() {
    // get latest price created at
    const latestTicker = await db.Ticker.findOne({order: [['id', 'DESC']]});
    if(latestTicker) {
        const createdAt = Math.round(new Date(latestTicker.createdAt).getTime() / 1000);
        const now = Math.round(new Date().getTime() / 1000);
		if(now - createdAt > +env.NOTIFICATION_PRICE_NOT_RECEIVED_TIMEOUT) {
			await sendEmailNotification("CFD notification: exchange price not recevied", `Exchange price has not been received for more than ${env.PRICE_NOT_RECEIVED_NOTIFICATION_TIMEOUT} sec`);
		}
    }
}

/**
 * Notify admin if exchange is not available(if drone wasn't able to get prices or hedge one of the cfds)
 */
async function notifyOnExchangeNotAvailable() {
    // get setting
    const settingExchangeAvailable = await drone.getSetting(drone.settings.IS_EXCHANGE_AVAILABLE);
    // send notification if exchange is not available
    if(settingExchangeAvailable.value == 0) {
        await sendEmailNotification("CFD notification: exchange not available", `Exchange is not available`);
    }
}

/**
 * Notify admin if smart contract/exchange balance in ETH/exchange balance in USD is too low
 */
async function notifyOnLowBalance() {
    // get ledger unlocked balance
    const ledgerUnlockedBalanceInWei = await exposureLedger.methods.getUnlockedBalance().call();
    if(+ledgerUnlockedBalanceInWei < +env.NOTIFICATION_MIN_LEDGER_UNLOCKED_BALANCE_IN_WEI) {
        const ledgerUnlockedBalanceInEth = web3.utils.fromWei(String(ledgerUnlockedBalanceInWei));
        const minLedgerUnlockedBalanceInEth = web3.utils.fromWei(String(env.NOTIFICATION_MIN_LEDGER_UNLOCKED_BALANCE_IN_WEI));
        await sendEmailNotification("CFD notification: low ledger unlocked balance", `Current ledger unlocked balance: ${ledgerUnlockedBalanceInEth} ETH. Min ledger unlocked balance: ${minLedgerUnlockedBalanceInEth} ETH.`);
    }
    // exchange balance in usd
    const exchangeBalances = await kraken.getBalance();
    const balanceInUsd = exchangeBalances.result['ZUSD'] || 0;
    if(+balanceInUsd < +env.NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_USD) {
        await sendEmailNotification("CFD notification: low exchange balance in USD", `Current exchange balance: ${balanceInUsd} USD. Min exchange balance: ${env.NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_USD} USD.`);
    }
    // exchange balance in eth
    const balanceInEth = exchangeBalances.result['XETH'] || 0;
    if(+balanceInEth < +env.NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_ETH) {
        await sendEmailNotification("CFD notification: low exchange balance in ETH", `Current exchange balance: ${balanceInEth} ETH. Min exchange balance: ${env.NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_ETH} ETH.`);
    }
    // exchange balance in btc
    const balanceInBtc = exchangeBalances.result['XXBT'] || 0;
    if(+balanceInBtc < +env.NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_BTC) {
        await sendEmailNotification("CFD notification: low exchange balance in BTC", `Current exchange balance: ${balanceInBtc} BTC. Min exchange balance: ${env.NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_BTC} BTC.`);
    }
    // exchange balance in bch
    const balanceInBch = exchangeBalances.result['BCH'] || 0;
    if(+balanceInBch < +env.NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_BCH) {
        await sendEmailNotification("CFD notification: low exchange balance in BCH", `Current exchange balance: ${balanceInBch} BCH. Min exchange balance: ${env.NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_BCH} BCH.`);
    }
    // exchange balance in ltc
    const balanceInLtc = exchangeBalances.result['XLTC'] || 0;
    if(+balanceInLtc < +env.NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_LTC) {
        await sendEmailNotification("CFD notification: low exchange balance in LTC", `Current exchange balance: ${balanceInLtc} LTC. Min exchange balance: ${env.NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_LTC} LTC.`);
    }
    // exchange balance in xrp
    const balanceInXrp = exchangeBalances.result['XXRP'] || 0;
    if(+balanceInXrp < +env.NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_XRP) {
        await sendEmailNotification("CFD notification: low exchange balance in XRP", `Current exchange balance: ${balanceInXrp} XRP. Min exchange balance: ${env.NOTIFICATION_MIN_EXCHANGE_BALANCE_IN_XRP} XRP.`);
    }
}

/**
 * Starts sending all email notifications
 */
async function run() {
    await notifyOnPriceNotReceived();
    await notifyOnExchangeNotAvailable();
    await notifyOnLowBalance();
    process.exit(1);
}

/**
 * Sends email notification
 */
async function sendEmailNotification(subject, html) {
    const transporter = nodemailer.createTransport({
        host: env.MAILER_SMTP_HOST,
        port: 465,
        secure: true,
        auth: {
            user: env.MAILER_ADMIN_EMAIL,
            pass: env.MAILER_ADMIN_PASSWORD
        }
    });
    
    const mailOptions = {
        from: env.MAILER_ADMIN_EMAIL,
        to: env.MAILER_TARGET_EMAIL,
        subject: subject,
        html: html
    };
    
    await transporter.sendMail(mailOptions);
}

const drone = require("./drone.js");
const env = require("../env.js");
const kraken = require("./exchange.js");
const util = require("./util.js");

const ExposureLedgerArtifact = require("../build/contracts/ExposureLedger.json");
const ExposureLedger = web3.eth.contract(ExposureLedgerArtifact.abi);
const networkName = process.argv[5];
const exposureLedgerAddress = util.getLedgerAddressByNetworkName(networkName);
const exposureLedger = ExposureLedger.at(exposureLedgerAddress);

module.exports = async (callback) => {
	try {
		const prices = await kraken.getPrices();
		const exposureIndex = process.argv[6];
		await drone.autoClose(exposureLedger, exposureIndex, prices);
	} catch(e) {
		console.log(e);
	}
	callback();
};
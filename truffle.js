require('babel-register');
require('babel-polyfill');
const HDWalletProvider = require("truffle-hdwallet-provider");
const env = require('./env.js');

const INFURA_API_KEY = env.INFURA_API_KEY;
const MNEMONIC = env.MNEMONIC;
const GAS_PRICE = 12000000000; // 12 gwei

module.exports = {
	networks: {
		development: {
			host: "localhost",
			port: 8555,
			network_id: "*", // Match any network id
			gas: 10000000
		},
		coverage: {
			host:       'localhost',
			network_id: '*',
			port:       8570,
			gas:        0xfffffffffff,
			gasPrice:   0x01,
		},
		main: {
			provider: () => new HDWalletProvider(MNEMONIC, `https://mainnet.infura.io/v3/${INFURA_API_KEY}`),
			network_id: 1,
			gasPrice: GAS_PRICE
		},
		ropsten: {
			provider: () => new HDWalletProvider(MNEMONIC, `https://ropsten.infura.io/v3/${INFURA_API_KEY}`),
			network_id: 3,
			gasPrice: GAS_PRICE
		},
		kovan: {
			provider: () => new HDWalletProvider(MNEMONIC, `https://kovan.infura.io/v3/${INFURA_API_KEY}`),
			network_id: 42,
			gasPrice: GAS_PRICE
		},
		rinkeby: {
			provider: () => new HDWalletProvider(MNEMONIC, `https://rinkeby.infura.io/v3/${INFURA_API_KEY}`),
			network_id: 4,
			gasPrice: GAS_PRICE
		}
	},
	mocha: {
		reporter: 'spec'
	}
};

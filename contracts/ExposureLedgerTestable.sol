pragma solidity ^0.4.24;

import "./ExposureLedger.sol";

contract ExposureLedgerTestable is ExposureLedger {

	constructor(
		address _adminAddress,
		address _droneAddress, 
		address _platformFeeAddress, 
		address _discountTokenAddress,
		uint _closingFee, 
		uint _exchangeDeltaPercent, 
		uint _defaultRate,
		uint _maxExposureDurationInDays,
		uint _maxFunding,
		uint _maxOpenExposuresPerAddress,
		uint _maxUserLoss
	) public ExposureLedger(_adminAddress, _droneAddress, _platformFeeAddress, _discountTokenAddress, _closingFee, _exchangeDeltaPercent, _defaultRate, _maxExposureDurationInDays, _maxFunding, _maxOpenExposuresPerAddress, _maxUserLoss) {}

	function finish(uint _index, uint _realExchangeRate) external {
		_finish(_index, _realExchangeRate);
	}

	function getPlatformFee(uint _index) external view returns(uint) {
		return _getPlatformFee(_index);
	}

	function getResultUserSum(uint _index, uint _realExchangeRate) external view returns(uint) {
		return _getResultUserSum(_index, _realExchangeRate);
	}

}
pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";

/**
 * @title DiscountSystem
 * @dev Contract manages users' discounts
 */
contract DiscountSystem is Ownable {

    using SafeMath for uint;

    address public adminAddress;
    ERC20 public discountToken;
    uint public pointsPerTokensCount;
    uint public tokensPerPointsCount;
    uint public daysOfIdleToResetPoints;
    DiscountStage[] public discountStages;

    mapping (address => Discount) public userDiscount;

    enum TokenOperationType { Stake, Withdraw, UseForFeeDiscount, ApplyForFeeDiscount }

    struct Discount {
        mapping (uint => TokenOperation) tokenOperations; // mapping of all token operations
        uint tokenOperationsCount; // number of token operations
        uint balanceInTokens; // balance in LEND tokens
        uint updatedAt; // last updated at timestamp
    }

    struct TokenOperation {
        TokenOperationType tokenOperationType; // operation type: stake or withdraw
        uint createdAt; // operation create timestamp
        uint amount; // amount of LEND tokens
    }

    struct DiscountStage {
        uint discountPoints; // min amount of points to reach this stage
        uint discountRatePpm; // discount rate in ppm, 1000000 == 100%
    }

    /**
	 * Modifiers
	 */

    /**
	 * Modifier checks that method can be called only by admin
	 */
	modifier onlyAdmin() {
		require(msg.sender == adminAddress);
		_;
	}

    /**
     * @dev Constructor
     * @param _discountTokenAddress LEND token address
     * @param _adminAddress admin address
     * @param _pointsPerTokensCount how many points are added per tokens count, ex: user gets 10 discount points per 10000 tokens so _pointsPerTokensCount == 10
     * @param _tokensPerPointsCount tokens amount for single points addition, ex: user gets 10 discount points per 10000 tokens so _tokensPerPointsCount == 10000
     * @param _daysOfIdleToResetPoints if CFD is not created in this number of days and min points stage is not reached then poinst are burnt
     */
    constructor(
        address _discountTokenAddress, 
        address _adminAddress, 
        uint _pointsPerTokensCount, 
        uint _tokensPerPointsCount,
        uint _daysOfIdleToResetPoints
    ) public {
        // validation
        require(_discountTokenAddress != address(0));
        require(_adminAddress != address(0));
        require(_pointsPerTokensCount > 0);
        require(_tokensPerPointsCount > 0);
        // set contract properties
        discountToken = ERC20(_discountTokenAddress);
        adminAddress = _adminAddress;
        pointsPerTokensCount = _pointsPerTokensCount;
        tokensPerPointsCount = _tokensPerPointsCount;
        daysOfIdleToResetPoints = _daysOfIdleToResetPoints;
    }

    /**
     * Public methods
     */

    /**
     * @dev Returns discount points and rate where user address sits level with
     * @param _userAddress user address
     * @return discount points and discount rate in ppm
     */
    function getDiscountStageByUserAddress(address _userAddress) external view returns (uint, uint) {
        // validation
        require(discountStages.length > 0);
        // calculate discount rate
        uint pointsCount = getPointsCountByUserAddress(_userAddress);
        uint prevMaxStageIndex = 0;
        uint discountPoints = 0;
        uint discountRatePpm = 0;
        // for all discount stages
        for(uint i = 0; i < discountStages.length; i++) {
            if(pointsCount >= discountStages[i].discountPoints && discountStages[i].discountPoints >= discountStages[prevMaxStageIndex].discountPoints) {
                prevMaxStageIndex = i;
                discountPoints = discountStages[prevMaxStageIndex].discountPoints;
                discountRatePpm = discountStages[prevMaxStageIndex].discountRatePpm;
            }
        }
        return (discountPoints, discountRatePpm);
    }

    /**
     * @dev Returns discount stage with min discount points
     * @return min discount stage
     */
    function getMinDiscountStage() public view returns(uint, uint) {
        // validation
        require(discountStages.length > 0);
        // find discount stage with min discount points
        uint minStageIndex = 0;
        for(uint i = 0; i < discountStages.length; i++) {
            if(discountStages[i].discountPoints < discountStages[minStageIndex].discountPoints) {
                minStageIndex = i;
            }
        }
        return (
            discountStages[minStageIndex].discountPoints,
            discountStages[minStageIndex].discountRatePpm
        );
    }

    /**
     * @dev Returns amount of discount points by user address
     * @param _userAddress user address
     * @return amount of discount points
     */
    function getPointsCountByUserAddress(address _userAddress) public view returns (uint) {
        // validation
        require(_userAddress != address(0));
        // calculate points count by user address
        uint pointsSum = 0;
        uint balanceInTokensSum = 0;
        uint lastApplyForFeeTimestamp = 0;
        (uint minDiscountPoints,) = getMinDiscountStage();
        for(uint i = 0; i < userDiscount[_userAddress].tokenOperationsCount; i++) {
            TokenOperation memory tokenOperation = userDiscount[_userAddress].tokenOperations[i];
            if(i == 0 || tokenOperation.tokenOperationType == TokenOperationType.ApplyForFeeDiscount) {
                lastApplyForFeeTimestamp = tokenOperation.createdAt;
            }
            // update balances in tokens
            if(tokenOperation.tokenOperationType == TokenOperationType.Stake) {
                balanceInTokensSum = balanceInTokensSum.add(tokenOperation.amount);
            } else {
                balanceInTokensSum = balanceInTokensSum.sub(tokenOperation.amount);
            }
            // get days passed
            uint daysPassed = _getDaysPassed(_userAddress, i);
            // subtract points in case user closed exposure
            if(tokenOperation.tokenOperationType == TokenOperationType.UseForFeeDiscount) {
                uint pointsToSubtract = tokenOperation.amount.div(tokensPerPointsCount).mul(pointsPerTokensCount);
                if(pointsToSubtract > pointsSum) {
                    pointsSum = 0;
                } else {
                    pointsSum = pointsSum.sub(pointsToSubtract);
                }
            }
            // if exposure is not created for too long and min points discount is not reached then reset points
            uint tokenOperationCreatedAt = tokenOperation.createdAt;
            if(i == userDiscount[_userAddress].tokenOperationsCount.sub(1)) {
                tokenOperationCreatedAt = now;
            } 
            if(pointsSum < minDiscountPoints && tokenOperationCreatedAt.sub(lastApplyForFeeTimestamp).div(1 days) > daysOfIdleToResetPoints) {
                pointsSum = 0;
            } else {
                // add points for time being
                pointsSum = pointsSum.add(balanceInTokensSum.div(tokensPerPointsCount).mul(pointsPerTokensCount).mul(daysPassed));
            }
        }
        return pointsSum;
    }

    /**
     * @dev Returns token operation info by user address and token operation index
     * @param _userAddress user address
     * @param _index token operation index
     * @return token operation details
     */
    function getTokenOperation(address _userAddress, uint _index) external view returns(TokenOperationType, uint, uint) {
        // validation
        require(_userAddress != address(0));
        require(_index < userDiscount[_userAddress].tokenOperationsCount);
        // return token operation details
        return (
            userDiscount[_userAddress].tokenOperations[_index].tokenOperationType,
            userDiscount[_userAddress].tokenOperations[_index].createdAt,
            userDiscount[_userAddress].tokenOperations[_index].amount
        );
    }

    /**
     * @dev Puts some LEND tokens at stake
     * @param _amount amount of LEND tokens to put at stake
     */
    function stake(uint _amount) external {
        // validation
        require(_amount > 0);
        require(discountToken.balanceOf(msg.sender) >= _amount);
        // put tokens at stake
        _createTokenOperation(msg.sender, _amount, TokenOperationType.Stake);
    }

    /**
     * @dev Withdraws LEND tokens from stake
     * @param _amount amount of LEND tokens to withdraw
     */
    function withdraw(uint _amount) external {
        // validation
        require(_amount > 0);
        require(userDiscount[msg.sender].balanceInTokens >= _amount);
        // withdraw tokens
        _createTokenOperation(msg.sender, _amount, TokenOperationType.Withdraw);
    }

    /**
     * Admin methods
     */

    /**
     * @dev Changes number of days of idle when points should be reset
     * @param _daysOfIdleToResetPoints number of days after which points are reset if min discount stage is not reached
     */
    function changeDaysOfIdleToResetPoints(uint _daysOfIdleToResetPoints) external onlyAdmin {
        daysOfIdleToResetPoints = _daysOfIdleToResetPoints;
    }
    
    /**
     * @dev Changes amount of points per amount of tokens
     * @param _pointsPerTokensCount amount of points per amount of tokens
     * @param _tokensPerPointsCount amount of tokens per which points are added
     */
    function changePointsPerTokensCount(uint _pointsPerTokensCount, uint _tokensPerPointsCount) external onlyAdmin {
        // validation
        require(_pointsPerTokensCount > 0);
        require(_tokensPerPointsCount > 0);
        // update contract properties
        pointsPerTokensCount = _pointsPerTokensCount;
        tokensPerPointsCount = _tokensPerPointsCount;
    }

    /**
     * @dev Creates a new discount stage. Ex: when user reaches 10 points he gets 5% discount.
     * @param _discountPoints min amount of discount points to reach this stage
     * @param _discountRatePpm discount rate in ppm, 1000000 == 100%
     */
    function createDiscountStage(uint _discountPoints, uint _discountRatePpm) external onlyAdmin {
        DiscountStage memory discountStage;
        discountStage.discountPoints = _discountPoints;
        discountStage.discountRatePpm = _discountRatePpm;
        discountStages.push(discountStage);
    }

    /**
     * @dev Deletes a discount stage
     * @param _stageIndex discount stage index
     */
    function deleteDiscountStage(uint _stageIndex) external onlyAdmin {
        // validation
        require(_stageIndex < discountStages.length);
        // delete discount stage
        discountStages[_stageIndex].discountPoints = discountStages[discountStages.length-1].discountPoints;
        discountStages[_stageIndex].discountRatePpm = discountStages[discountStages.length-1].discountRatePpm;
        discountStages.length--;
    }

    /**
     * @dev Updates a discount stage
     * @param _stageIndex discount stage index
     * @param _discountPoints min amount of discount points to reach this stage
     * @param _discountRatePpm discount rate in ppm, 1000000 == 100%
     */
    function updateDiscountStage(uint _stageIndex, uint _discountPoints, uint _discountRatePpm) external onlyAdmin {
        // validation
        require(_stageIndex < discountStages.length);
        // update discount stage
        discountStages[_stageIndex].discountPoints = _discountPoints;
        discountStages[_stageIndex].discountRatePpm = _discountRatePpm;
    }

    /**
     * Owner methods
     */

    /**
     * @dev Applies for fee discount. Used when drone autofunds exposure
     * @param _userAddress user address who applies for token discount
     */
    function applyForFeeDiscount(address _userAddress) external onlyOwner {
        // validation
        require(_userAddress != address(0));
        // use tokens for fee discount
        _createTokenOperation(_userAddress, 0, TokenOperationType.ApplyForFeeDiscount);
    }

    /**
	 * @dev Changes admin address
	 * @param _adminAddress new admin address
	 */
	function changeAdminAddress(address _adminAddress) external onlyOwner {
		require(_adminAddress != address(0));
		adminAddress = _adminAddress;
	}

    /**
     * @dev Users tokens to get a fee discount
     * @param _userAddress user address whose tokens are going to be used for discount
     * @param _amount amount of LEND tokens
     */
    function useForFeeDiscount(address _userAddress, uint _amount) external onlyOwner {
        // validation
        require(_userAddress != address(0));
        require(userDiscount[_userAddress].balanceInTokens >= _amount);
        // use tokens for fee discount
        _createTokenOperation(_userAddress, _amount, TokenOperationType.UseForFeeDiscount);
    }

    /**
     * Internal helper methods
     */

    /**
     * @dev Creates new token operation. Notice: before stake operation user should approve spending LEND tokens for current contract.
     * @param _userAddress user address
     * @param _amount amount of LEND tokens
     * @param _tokenOperationType token operation type, stake or withdraw
     */
    function _createTokenOperation(address _userAddress, uint _amount, TokenOperationType _tokenOperationType) internal {
        // validation
        require(_userAddress != address(0));
        // create token operation
        TokenOperation memory tokenOperation;
        tokenOperation.tokenOperationType = _tokenOperationType;
        tokenOperation.createdAt = now;
        tokenOperation.amount = _amount;
        userDiscount[_userAddress].tokenOperations[userDiscount[_userAddress].tokenOperationsCount] = tokenOperation;
        userDiscount[_userAddress].updatedAt = now;
        userDiscount[_userAddress].tokenOperationsCount = userDiscount[_userAddress].tokenOperationsCount.add(1);
        if(_tokenOperationType == TokenOperationType.Stake) {
            // stake operation
            userDiscount[_userAddress].balanceInTokens = userDiscount[_userAddress].balanceInTokens.add(_amount);
            discountToken.transferFrom(_userAddress, address(this), _amount);
        } else if(_tokenOperationType == TokenOperationType.Withdraw) {
            // withdraw operation
            userDiscount[_userAddress].balanceInTokens = userDiscount[_userAddress].balanceInTokens.sub(_amount);
            discountToken.transfer(_userAddress, _amount);
        }
    }

    /**
     * @dev Returns number of days that passed between current token operation and the next one
     * @param _userAddress user address who created token operation
     * @param _tokenOperationIndex token operation index
     * @return number of days between current token operation and the next one
     */
    function _getDaysPassed(address _userAddress, uint _tokenOperationIndex) internal view returns(uint) {
        // validation
        require(_userAddress != address(0));
        require(_tokenOperationIndex < userDiscount[_userAddress].tokenOperationsCount);
        // get days passed
        TokenOperation memory tokenOperation = userDiscount[_userAddress].tokenOperations[_tokenOperationIndex];
        uint daysPassed;
        // if next operation exists
        if(_tokenOperationIndex + 1 < userDiscount[_userAddress].tokenOperationsCount) {
            TokenOperation memory nextTokenOperation = userDiscount[_userAddress].tokenOperations[_tokenOperationIndex+1];
            daysPassed = nextTokenOperation.createdAt.sub(tokenOperation.createdAt).div(1 days);
        } else {
            // calculate days between current token operation and current timestamp
            daysPassed = now.sub(tokenOperation.createdAt).div(1 days);
        }
        return daysPassed;
    }

}
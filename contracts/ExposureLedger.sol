pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/lifecycle/Pausable.sol";
import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "./DiscountSystem.sol";

/**
 * @title ExposureLedger
 * @dev Contract holds all exposures and manages them
 */
contract ExposureLedger is Ownable, Pausable {

	using SafeMath for uint;

	address public adminAddress;
	address public droneAddress;
	address public platformFeeAddress;
	uint public closingFee;
	uint public exchangeDeltaPercent;
	uint public exposureCount;
	uint public defaultRate;
	uint public lockedBalance;
	uint public maxExposureDurationInDays;
	uint public maxOpenExposuresPerAddress;
	uint public maxFunding;
	uint public maxUserLoss;
	DiscountSystem public discountSystem;

	mapping(uint => Exposure) exposures;
	mapping(address => uint) userExposureCount;
	mapping(address => mapping(uint => uint)) userExposure;
	mapping(address => uint) userOpenExposureCount;

	enum Asset { ETH, BTC, INDEX1 }

	enum Currency { USD }

	enum Leverage { OneToOne, TwoToOne, FourToOne }

	enum State {
		FundedByUser,
		BadEthRate,
		FundedByPlatform,
		ClosedByUser,
		MarginCallTimeElapsed,
		MarginCallPriceMovement,
		MarginCallMaxGainReached,
		ClosedByAdmin
	}

	struct Exposure {
		address userAddress; // user address who created the exposure
		address returnUserAddress; // user address where to transfer funds after exposure closed
		uint createdAt; // timestamp when contract was created
		uint closedAt; // timestamp when contract was closed
		uint exchangeRateCurrencyPerEther; // exchange rate in cents at the time of creation, ex: 500 usd == 50000
		uint closeExchangeRateCurrencyPerEther; // exchange rate in cents passed by drone on exposure close, ex: 500 usd == 50000
		uint durationInDays; // days after which the exposure will be closed
		uint initialUserFund; // initial user funding sum
		uint resultPlatformFee; // result fee when exposure is closed
		uint resultPlatformSum; // result platform/exposure sum when exposure is closed
		uint resultUserSum; // result user sum when exposure is closed
		bool isLong; // position type, long|short
		bool isGoingToBeClosed; // whether user has marked exposure to be closed by drone soon
		Currency currency; // currency of the exposure
		State currentState; // current exposure state
		Leverage leverage; // exposure leverage, the more leverage the more user can lose
		Asset asset; // exposure asset, ex: ETH, BTC, etc...
	}

	/**
	 * Modifiers
	 */

	/**
	 * @dev Modifier checks that method can be called only by drone
	 */
	modifier onlyDrone() {
		require(msg.sender == droneAddress);
		_;
	}

	/**
	 * @dev Modifier checks that method can be called only in specific exposure state
	 * @param _index exposure index
	 * @param _state state
	 */
	modifier onlyInState(uint _index, State _state) {
		require(_index < exposureCount);
		require(exposures[_index].currentState == _state);
		_;
	}

	/**
	 * @dev Modifier checks that method can be called only by user who created an exposure
	 * @param _index exposure index
	 */
	modifier onlyUser(uint _index) {
		require(_index < exposureCount);
		require(msg.sender == exposures[_index].userAddress);
		_;
	}

	/**
	 * Modifier checks that method can be called only by admin
	 */
	modifier onlyAdmin() {
		require(msg.sender == adminAddress);
		_;
	}

	/**
	 * @dev Modifier checks that method can be called only with existing exposure index
	 * @param _index exposure index
	 */
	modifier validExposureIndex(uint _index) {
		require(_index < exposureCount);
		_;
	}

	constructor(
		address _adminAddress,
		address _droneAddress, 
		address _platformFeeAddress, 
		address _discountTokenAddress,
		uint _closingFee, 
		uint _exchangeDeltaPercent, 
		uint _defaultRate,
		uint _maxExposureDurationInDays,
		uint _maxFunding,
		uint _maxOpenExposuresPerAddress,
		uint _maxUserLoss
	) public {
		// validate params
		require(_adminAddress != address(0));
		require(_droneAddress != address(0));
		require(_platformFeeAddress != address(0));
		require(_discountTokenAddress != address(0));
		require(_closingFee > 0);
		require(_maxExposureDurationInDays != 0);
		require(_maxFunding != 0);
		// set params
		adminAddress = _adminAddress;
		droneAddress = _droneAddress;
		platformFeeAddress = _platformFeeAddress;
		closingFee = _closingFee;
		exchangeDeltaPercent = _exchangeDeltaPercent;
		defaultRate = _defaultRate;
		maxExposureDurationInDays = _maxExposureDurationInDays;
		maxFunding = _maxFunding;
		maxOpenExposuresPerAddress = _maxOpenExposuresPerAddress;
		maxUserLoss = _maxUserLoss;
		discountSystem = new DiscountSystem(_discountTokenAddress, _adminAddress, 10, 10000, 30);
	}

	/**
	 * Owner methods
	 */

	/**
	 * @dev Updates admin address
	 * @param _newAdminAddress new admin address
	 */
	function changeAdminAddress(address _newAdminAddress) external onlyOwner {
		require(_newAdminAddress != address(0));
		adminAddress = _newAdminAddress;
		discountSystem.changeAdminAddress(_newAdminAddress);
	}

	/**
	 * @dev Updates closing fee. By default 1000000 == 100%
	 * @param _newClosingFee new closing fee
	 */
	function changeClosingFee(uint _newClosingFee) external onlyOwner {
		require(_newClosingFee > 0);
		closingFee = _newClosingFee;
	}

	/**
	 * @dev Updates default rate. Ex.: 10000 = 100%. Used in Exposure _finish() method
	 * @param _newDefaultRate new default rate
	 */
	function changeDefaultRate(uint _newDefaultRate) external onlyOwner {
		require(_newDefaultRate > 0);
		defaultRate = _newDefaultRate;
	}

	/**
	 * @dev Updates dron/daemon address
	 * @param _newDroneAddress new daemon address
	 */
	function changeDroneAddress(address _newDroneAddress) external onlyOwner {
		require(_newDroneAddress != address(0));
		droneAddress = _newDroneAddress;
	}

	/**
	 * @dev Updates exchange rate delta. By default 10000 == 100%. Exposure's exchange rate should be in [rate-delta,rate+delta]
	 * @param _newExchangeDeltaPercent new exchange rate delta
	 */
	function changeExchangeDelta(uint _newExchangeDeltaPercent) external onlyOwner {
		exchangeDeltaPercent = _newExchangeDeltaPercent;
	}

	/**
	 * @dev Updates max exposure duration in days
	 * @param _newMaxExposureDurationInDays new max exposure duration in days
	 */
	function changeMaxExposureDurationInDays(uint _newMaxExposureDurationInDays) external onlyOwner {
		require(_newMaxExposureDurationInDays != 0);
		maxExposureDurationInDays = _newMaxExposureDurationInDays;
	}

	/**
	 * @dev Updates max funding sum per exposure
	 * @param _newMaxFunding new max funding sum in wei
	 */
	function changeMaxFunding(uint _newMaxFunding) external onlyOwner {
		require(_newMaxFunding != 0);
		maxFunding = _newMaxFunding;
	}

	/**
	 * @dev Updates max open exposures per user
	 * @param _newMaxOpenExposuresPerAddress new max open exposures count per user
	 */
	function changeMaxOpenExposuresPerAddress(uint _newMaxOpenExposuresPerAddress) external onlyOwner {
		maxOpenExposuresPerAddress = _newMaxOpenExposuresPerAddress;
	}

	/**
	 * @dev Updates max user loss
	 * @param _newMaxUserLoss new max user loss rate, 10000 = 100%
	 */
	function changeMaxUserLoss(uint _newMaxUserLoss) external onlyOwner {
		maxUserLoss = _newMaxUserLoss;
	}

	/**
	 * @dev Updates platform fee address
	 * @param _newPlatformFeeAddress new platform fee address
	 */
	function changePlatformFeeAddress(address _newPlatformFeeAddress) external onlyOwner {
		require(_newPlatformFeeAddress != address(0));
		platformFeeAddress = _newPlatformFeeAddress;
	}

	/**
	 * Public ledger methods
	 */

	/**
	 * @dev Creates a new exposure
	 * @param _isLong whether long or short position
	 * @param _currency currency enum
	 * @param _exchangeRateCurrencyPerEther exchange rate in cents at the time of creation
	 * @param _durationInDays days count after which exposure will be closed
	 * @param _returnUserAddress address where to transfer user funds after closing the exposure
	 * @param _leverage exposure leverage
	 * @param _asset exposure asset
	 */
	function createAndFundNewExposure(
		bool _isLong, 
		Currency _currency, 
		uint _exchangeRateCurrencyPerEther, 
		uint _durationInDays, 
		address _returnUserAddress,
		Leverage _leverage,
		Asset _asset
	) external payable whenNotPaused {
		// validation
		require(_exchangeRateCurrencyPerEther > 0);
		require(_durationInDays != 0);
		require(_durationInDays <= maxExposureDurationInDays);
		require(_returnUserAddress != address(0));
		require(msg.value > 0);
		require(msg.value <= maxFunding);
		require(getUnlockedBalance().sub(msg.value) >= msg.value);
		require(getOpenExposuresCountByAddress(msg.sender) < maxOpenExposuresPerAddress);
		// create a new exposure
		Exposure memory exposure;
		exposure.userAddress = msg.sender;
		exposure.returnUserAddress = _returnUserAddress;
		exposure.createdAt = now;
		exposure.exchangeRateCurrencyPerEther = _exchangeRateCurrencyPerEther;
		exposure.durationInDays = _durationInDays;
		exposure.initialUserFund = msg.value;
		exposure.isLong = _isLong;
		exposure.currency = _currency;
		exposure.currentState = State.FundedByUser;
		exposure.leverage = _leverage;
		exposure.asset = _asset;
		// assign exposure to mappings
		exposures[exposureCount] = exposure;
		userExposure[msg.sender][userExposureCount[msg.sender]] = exposureCount;
		exposureCount = exposureCount.add(1);
		userExposureCount[msg.sender] = userExposureCount[msg.sender].add(1);
		// update locked balance
		lockedBalance = lockedBalance.add(msg.value.mul(2));
		// update user's open exposures count
		userOpenExposureCount[msg.sender] = userOpenExposureCount[msg.sender].add(1);
	}

	/**
	 * @dev Returns exposure result data by index
	 * @param _index exposure index/id
	 * @return exposure result data
	 */
	function getExposureResultData(uint _index) public view validExposureIndex(_index) returns(uint, uint, uint, uint, uint) {
		return (
			exposures[_index].closeExchangeRateCurrencyPerEther,
			exposures[_index].initialUserFund,
			exposures[_index].resultPlatformFee,
			exposures[_index].resultPlatformSum,
			exposures[_index].resultUserSum
		);
	}

	/**
	 * @dev Returns exposure system data by index
	 * @param _index exposure index/id
	 * @return exposure system data
	 */
	function getExposureSystemData(uint _index) public view validExposureIndex(_index) returns(uint, State, uint, Leverage, Asset) {
		return (
			exposures[_index].createdAt,
			exposures[_index].currentState,
			exposures[_index].closedAt,
			exposures[_index].leverage,
			exposures[_index].asset
		);
	}

	/**
	 * @dev Returns exposure user data by index
	 * @param _index exposure index/id
	 * @return exposure user data
	 */
	function getExposureUserData(uint _index) public view returns(address, address, uint, uint, bool, bool, Currency) {
		require(_index < exposureCount);
		return (
			exposures[_index].userAddress,
			exposures[_index].returnUserAddress,
			exposures[_index].exchangeRateCurrencyPerEther,
			exposures[_index].durationInDays,
			exposures[_index].isLong,
			exposures[_index].isGoingToBeClosed,
			exposures[_index].currency
		);
	}

	/**
	 * @dev Returns exposures count by user address
	 * @param _userAddress client address
	 * @return exposures count
	 */
	function getExposuresCountForUser(address _userAddress) external view returns(uint) {
		return userExposureCount[_userAddress];
	}

	/**
	 * @dev Returns exposure for user by index
	 * @param _userAddress client address
	 * @param _index exposure index/id
	 * @return exposure
	 */
	function getExposureIdForUserByIndex(address _userAddress, uint _index) external view returns(uint) {
		require(_index < userExposureCount[_userAddress]);
		return userExposure[_userAddress][_index];
	}

	/**
	 * @dev Returns ledger's unlocked balance(how much ether left for funding)
	 * @return unlocked balance
	 */
	function getUnlockedBalance() public view returns(uint) {
		return address(this).balance.sub(lockedBalance);
	}

	/**
	 * @dev Returns open exposures count by user address
	 * @param _userAddress user address for whom we search exposures
	 * @return open exposures count
	 */
	function getOpenExposuresCountByAddress(address _userAddress) public view returns(uint) {
		require(_userAddress != address(0));
		return userOpenExposureCount[_userAddress];
	}

	/**
	 * User methods
	 */

	/**
	 * @dev Marks exposure to be closed by drone
	 * @param _index exposure index
	 */
	function close(uint _index) external onlyInState(_index, State.FundedByPlatform) onlyUser(_index) {
		// validation
		require(!exposures[_index].isGoingToBeClosed);
		// mark exposure for closing
		exposures[_index].isGoingToBeClosed = true;
	}

	/**
	 * Admin methods
	 */

	/**
	 * @dev Closes exposure forcibly
	 * @param _index exposure index
	 * @param _realExchangeRateCurrencyPerEther real exchange rate in cents
	 */
	function forceClose(uint _index, uint _realExchangeRateCurrencyPerEther) public onlyAdmin {
		// validation
		require(_index < exposureCount);
		require(_realExchangeRateCurrencyPerEther > 0);
		require(exposures[_index].currentState == State.FundedByUser || exposures[_index].currentState == State.FundedByPlatform);
		// update state
		exposures[_index].currentState = State.ClosedByAdmin;
		// transfer funds
		_finish(_index, _realExchangeRateCurrencyPerEther);
	}

	/**
	 * @dev Closes exposures forcibly in batch mode
	 * @param _indexes array of exposure indexes to close forcibly
	 * @param _realExchangeRateCurrencyPerEther real exchange rate in cents
	 */
	function forceCloseMany(uint[] _indexes, uint _realExchangeRateCurrencyPerEther) external onlyAdmin {
		// validation
		require(_indexes.length > 0);
		require(_realExchangeRateCurrencyPerEther > 0);
		// force close all exposures
		for(uint i = 0; i < _indexes.length; i++) {
			forceClose(_indexes[i], _realExchangeRateCurrencyPerEther);
		}
	}

	/**
	 * @dev Withdraws unlocked amount to output address
	 * @param _amount amount to withdraw in wei
	 * @param _output withdraw address
	 */
	function withdrawUnlockedBalance(uint _amount, address _output) external onlyAdmin {
		// validation
		require(_amount > 0);
		require(_amount <= getUnlockedBalance());
		require(_output != address(0));
		// withraw
		_output.transfer(_amount);
	}

	/**
	 * Drone methods
	 */

	/**
	 * @dev Closes exposure, transfers fees and funds
	 * @param _index exposure index
	 * @param _realExchangeRateCurrencyPerEther real exchange rate in cents
	 */
	function autoClose(uint _index, uint _realExchangeRateCurrencyPerEther) public onlyInState(_index, State.FundedByPlatform) onlyDrone {
		// validation
		require(exposures[_index].isGoingToBeClosed);
		require(_realExchangeRateCurrencyPerEther > 0);
		// update state
		exposures[_index].currentState = State.ClosedByUser;
		// transfer funds
		_finish(_index, _realExchangeRateCurrencyPerEther);
	}

	/**
	 * @dev Closes exposures in batch mode
	 * @param _indexes array of exposure indexes to close
	 * @param _realExchangeRateCurrencyPerEther real exchange rate in cents
	 */
	function autoCloseMany(uint[] _indexes, uint _realExchangeRateCurrencyPerEther) external onlyDrone {
		// validation
		require(_indexes.length > 0);
		require(_realExchangeRateCurrencyPerEther > 0);
		// autoclose all exposures
		for(uint i = 0; i < _indexes.length; i++) {
			autoClose(_indexes[i], _realExchangeRateCurrencyPerEther);
		}
	}

	/**
	 * @dev Autofunds contract in case of successful validation by drone
	 * @param _index exposure index
	 */
	function autoFund(uint _index) public onlyInState(_index, State.FundedByUser) onlyDrone {
		// update state
		exposures[_index].currentState = State.FundedByPlatform;
		// create apply for fee token operation
		discountSystem.applyForFeeDiscount(exposures[_index].userAddress);
	}

	/**
	 * @dev Autofunds exposures in batch mode
	 * @param _indexes array of exposure indexes to fund
	 */
	function autoFundMany(uint[] _indexes) external onlyDrone {
		// validation
		require(_indexes.length > 0);
		// autofund all exposures
		for(uint i = 0; i < _indexes.length; i++) {
			autoFund(_indexes[i]);
		}
	}

	/**
	 * @dev Sets exposure state to BadEthRate in case of failed validation by frone
	 * @param _index exposure index
	 * @param _realExchangeRateCurrencyPerEther real exchange rate in cents
	 */
	function badEthRate(uint _index, uint _realExchangeRateCurrencyPerEther) public onlyInState(_index, State.FundedByUser) onlyDrone {
		require(_realExchangeRateCurrencyPerEther > 0);
		// decrease locked balance
		lockedBalance = lockedBalance.sub(exposures[_index].initialUserFund.mul(2));
		// decrease user's open exposures count
		userOpenExposureCount[exposures[_index].userAddress] = userOpenExposureCount[exposures[_index].userAddress].sub(1);
		// set exposure properties
		exposures[_index].closeExchangeRateCurrencyPerEther = _realExchangeRateCurrencyPerEther;
		exposures[_index].currentState = State.BadEthRate;
		exposures[_index].closedAt = now;
		// refund user
		exposures[_index].userAddress.transfer(exposures[_index].initialUserFund);
	}

	/**
	 * @dev Bad eth rate exposures in batch mode
	 * @param _indexes array of exposure indexes to bad eth rate
	 * @param _realExchangeRateCurrencyPerEther real exchange rate in cents
	 */
	function badEthRateMany(uint[] _indexes, uint _realExchangeRateCurrencyPerEther) external onlyDrone {
		// validation
		require(_indexes.length > 0);
		require(_realExchangeRateCurrencyPerEther > 0);
		// bad eth rate all exposures
		for(uint i = 0; i < _indexes.length; i++) {
			badEthRate(_indexes[i], _realExchangeRateCurrencyPerEther);
		}
	}

	/**
	 * @dev Sets state to margin call and transfers funds
	 * @param _index exposure index
	 * @param _realExchangeRateCurrencyPerEther real exchange rate in cents
	 * @param _newMarginCallState margin call state to set
	 */
	function marginCall(uint _index, uint _realExchangeRateCurrencyPerEther, State _newMarginCallState) public onlyInState(_index, State.FundedByPlatform) onlyDrone {
		require(_realExchangeRateCurrencyPerEther > 0);
		require(_newMarginCallState == State.MarginCallTimeElapsed || 
				_newMarginCallState == State.MarginCallPriceMovement || 
				_newMarginCallState == State.MarginCallMaxGainReached);
		// update state
		exposures[_index].currentState = _newMarginCallState;
		// transfer funds
		_finish(_index, _realExchangeRateCurrencyPerEther);
	}

	/**
	 * @dev Margin calls exposures in batch mode
	 * @param _indexes array of exposure indexes to margin call
	 * @param _realExchangeRateCurrencyPerEther real exchange rate in cents
	 * @param _newMarginCallState margin call state to set
	 */
	function marginCallMany(uint[] _indexes, uint _realExchangeRateCurrencyPerEther, State _newMarginCallState) external onlyDrone {
		// validation
		require(_indexes.length > 0);
		require(_realExchangeRateCurrencyPerEther > 0);
		require(_newMarginCallState == State.MarginCallTimeElapsed || 
		 		_newMarginCallState == State.MarginCallPriceMovement || 
		 		_newMarginCallState == State.MarginCallMaxGainReached);
		// margin call all exposures
		for(uint i = 0; i < _indexes.length; i++) {
			marginCall(_indexes[i], _realExchangeRateCurrencyPerEther, _newMarginCallState);
		}
	}

	/**
	 * Other methods
	 */

	/**
	 * @dev Enable payments to contract address
	 */
	function() external payable {}

	/**
	 * Internal helper methods
	 */

	 /** 
	  * @dev Closes order and transfers fee, user and platform funds
	  * @param _index exposure index
	  * @param _realExchangeRateCurrencyPerEther real exchange rate in cents
	  */
	 function _finish(uint _index, uint _realExchangeRateCurrencyPerEther) internal validExposureIndex(_index) {
		// validation
		require(_realExchangeRateCurrencyPerEther > 0);
		// update exposure properties
		exposures[_index].closeExchangeRateCurrencyPerEther = _realExchangeRateCurrencyPerEther;
		exposures[_index].closedAt = now;
		// decrease locked balance
		lockedBalance = lockedBalance.sub(exposures[_index].initialUserFund.mul(2));
		// decrease user's open exposures count
		userOpenExposureCount[exposures[_index].userAddress] = userOpenExposureCount[exposures[_index].userAddress].sub(1);
		// get sums
		exposures[_index].resultPlatformFee = _getPlatformFee(_index);
		exposures[_index].resultUserSum = _getResultUserSum(_index, _realExchangeRateCurrencyPerEther);
		exposures[_index].resultPlatformSum = exposures[_index].initialUserFund.mul(2).sub(exposures[_index].resultUserSum).sub(exposures[_index].resultPlatformFee);
		// create use for fee discount token operation
		(uint discountPoints,) = discountSystem.getDiscountStageByUserAddress(exposures[_index].userAddress);
		uint discountTokensAmount = discountPoints.div(discountSystem.pointsPerTokensCount()).mul(discountSystem.tokensPerPointsCount());
		discountSystem.useForFeeDiscount(exposures[_index].userAddress, discountTokensAmount);
		// transfer funds
		platformFeeAddress.transfer(exposures[_index].resultPlatformFee);
		exposures[_index].returnUserAddress.transfer(exposures[_index].resultUserSum);
	}

	/**
	 * @dev Returns platform fee
	 * @param _index exposure index
	 * @return platform fee
	 */
	function _getPlatformFee(uint _index) internal view validExposureIndex(_index) returns(uint) {
		// return fee
		uint daysPassed = now.sub(exposures[_index].createdAt).div(1 days);
		if(daysPassed == 0) {
			daysPassed = 1;
		}
		// get platform fee
		uint platformFee = exposures[_index].initialUserFund.div(10**6).mul(closingFee).mul(daysPassed);
		// substitute discount
		(, uint discountRatePpm) = discountSystem.getDiscountStageByUserAddress(exposures[_index].userAddress);
		uint discountSum = platformFee.div(10**6).mul(discountRatePpm);
		if(discountSum > platformFee) {
			platformFee = 0;
		} else {
			platformFee = platformFee.sub(discountSum);
		}
		// platform fee can not be greater than initial user fund
		if(platformFee > exposures[_index].initialUserFund) {
			platformFee = exposures[_index].initialUserFund;
		} 
		return platformFee;
	}

	/**
	 * @dev Returns final user win/lose sum
	 * @param _index exposure index
	 * @param _realExchangeRateCurrencyPerEther real exchange rate in cents
	 * @return final user sum which should be transfered
	 */
	function _getResultUserSum(uint _index, uint _realExchangeRateCurrencyPerEther) internal view validExposureIndex(_index) returns(uint) {
		// validation
		require(_realExchangeRateCurrencyPerEther > 0);

		// get exposure properties
		uint initialFund = exposures[_index].initialUserFund;
		uint exchangeRateCurrencyPerEther = exposures[_index].exchangeRateCurrencyPerEther;
		bool isLong = exposures[_index].isLong;

		// calculate user sum
		uint resultRateDiff = _realExchangeRateCurrencyPerEther.mul(defaultRate).div(exchangeRateCurrencyPerEther);
		if(resultRateDiff >= defaultRate) {
			resultRateDiff = resultRateDiff.sub(defaultRate);
			bool isUp = true;
		} else {
			resultRateDiff = defaultRate.sub(resultRateDiff);
			isUp = false;
		}
		uint resultDiffSum = initialFund.div(defaultRate).mul(resultRateDiff);
		uint platformFee = _getPlatformFee(_index);

		// substitute platform fee from initial user sum
		uint resultSum = initialFund.sub(platformFee);
		
		// if price went up and user in long or price went down and user in short then add sum
		if((isUp && isLong) || (!isUp && !isLong)) {
			// user can not win more than contract has
			if(resultDiffSum > initialFund) {
				resultDiffSum = initialFund;
			}
			resultSum = resultSum.add(resultDiffSum);
		}
		// if price went up and user in short or price went down and user in long then substitute sum
		if((isUp && !isLong) || (!isUp && isLong)) {
			// restrict user loss
			if(resultRateDiff > maxUserLoss) {
				resultDiffSum = initialFund.div(defaultRate).mul(maxUserLoss);
			}
			// if platform fee ate most of the initial fund then return 0 else substitute
			if(resultSum < resultDiffSum) {
				resultSum = 0;
			} else {
				resultSum = resultSum.sub(resultDiffSum);
			}
		}
		return resultSum;
	}
}
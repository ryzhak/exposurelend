pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/token/ERC20/MintableToken.sol";

import "./DiscountSystem.sol";

contract DiscountSystemTestable is DiscountSystem {

    constructor(
        MintableToken _discountToken, 
        address _adminAddress, 
        uint _pointsPerTokensCount, 
        uint _tokensPerPointsCount,
        uint _daysOfIdleToResetPoints
    ) public DiscountSystem(_discountToken, _adminAddress, _pointsPerTokensCount, _tokensPerPointsCount, _daysOfIdleToResetPoints) {}

    function createTokenOperation(address _userAddress, uint _amount, TokenOperationType _tokenOperationType) external {
        _createTokenOperation(_userAddress, _amount, _tokenOperationType);
    }

    function getDaysPassed(address _userAddress, uint _tokenOperationIndex) external view returns(uint) {
        return _getDaysPassed(_userAddress, _tokenOperationIndex);
    }

}
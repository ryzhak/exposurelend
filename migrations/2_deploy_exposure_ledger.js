const env = require("../env.js");
const DiscountSystem = artifacts.require("./DiscountSystem");
const ExposureLedger = artifacts.require("./ExposureLedger.sol");
const MintableToken = artifacts.require("./MintableToken.sol");
const SafeMath = artifacts.require("./SafeMath.sol");

// NOTICE: we need this timeout between contract deployments because of https://github.com/trufflesuite/truffle/issues/763
const TIMEOUT = 60000;

module.exports = (deployer, network, accounts) => {

	// convert ppm units(10^6) to 10^4
	const currencyDelta = +env.CURRENCY_DELTA / 100;
	const currencyDenomination = +env.CURRENCY_DENOMINATION / 100;

	let discountSystem;
	let exposureLedger;
	let mintableToken;

	deployer.deploy(SafeMath)
		.then(() => new Promise(resolve => setTimeout(() => resolve(), TIMEOUT)))
		.then(() => {
			return deployer.link(SafeMath, [ExposureLedger, DiscountSystem])
		})
		.then(() => new Promise(resolve => setTimeout(() => resolve(), TIMEOUT)))
		.then(() => {
			return deployer.deploy(MintableToken);
		})
		.then((mintableTokenInstance) => {
			mintableToken = mintableTokenInstance; 
			return new Promise(resolve => setTimeout(() => resolve(), TIMEOUT)); 
		})
		.then(() => {
			return deployer.deploy(
				ExposureLedger, 
				accounts[0], // admin address, who can control drone
				accounts[0], // drone address
				accounts[0], // kraken deposit/platform fee/platform funds
				mintableToken.address, // LEND token address
				+env.PLATFORM_FEE_ON_CLOSE,  // platform fee on close, 1000000 = 100%
				currencyDelta,  // currency delta, on which rate is confirmed to be valid on funding, 10000 = 100%
				currencyDenomination, // currency denomination
				+env.MAX_EXPOSURE_DURATION_IN_DAYS, // max exposure duration in days
				+env.MAX_FUNDING, // max exposure funding sum in wei
				+env.MAX_OPEN_EXPOSURES_PER_ADDRESS, // max open exposures count user can have
				+env.MAX_USER_LOSS // max user loss rate, 10000 = 100%
			);
		})
		.then((exposureLedgerInstance) => {
			exposureLedger = exposureLedgerInstance;
			return exposureLedger.discountSystem();
		})
		.then((discountSystemAddress) => {
			discountSystem = DiscountSystem.at(discountSystemAddress);
			return discountSystem.createDiscountStage(10, 50000);
		})
		.then(() => {
			return discountSystem.createDiscountStage(50, 100000);
		})
		.then(() => {
			return discountSystem.createDiscountStage(250, 150000);
		})
		.then(() => {
			return discountSystem.createDiscountStage(500, 300000);
		});
};
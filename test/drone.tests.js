require('chai').use(require('chai-as-promised')).should();

const drone = require("../drone/drone.js");
const env = require("../env.js");
const { increaseTime } = require('./utils/helpers.js');

const DiscountSystem = artifacts.require("DiscountSystem");
const ExposureLedger = artifacts.require("ExposureLedgerTestable");
const MintableToken = artifacts.require("MintableToken");

contract("Drone", (accounts) => {

	const creatorAddr = accounts[0];
	const userAddr = accounts[1];
	const platformFeeAddr = accounts[2];
	const returnUserAddr = accounts[3];

	const defaultPrices = { ETH: '1.00', BTC: '2.00', BCH: '3.00', LTC: '4.00', XRP: '5.00', INDEX1: '3.00' };
	const initialFund = web3.toWei("0.1", "ether");

	let exposureLedger;
	let discountToken;
	let discountSystem;

	beforeEach(async() => {
		discountToken = await MintableToken.new();
		exposureLedger = await ExposureLedger.new(creatorAddr, env.DRONE_ADDRESS, platformFeeAddr, discountToken.address, 50000, 500, 10000, 30, web3.toWei("1","ether"), 20, 5000);
		discountSystem = DiscountSystem.at(await exposureLedger.discountSystem());
		await discountSystem.createDiscountStage(10, 100000, {from: creatorAddr}).should.be.fulfilled;
		await exposureLedger.send(initialFund, {from: creatorAddr}).should.be.fulfilled;
	});

	describe("applyIndexesToPrices()", () => {
		it("should apply indexes to prices from exchange", async() => {
			let pricesWithIndexes = drone.applyIndexesToPrices(defaultPrices);
			assert.equal(pricesWithIndexes['INDEX1'], 3);
		});
	});

	describe("autoClose()", () => {
		it("should autoclose the exposure", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: env.DRONE_ADDRESS}).should.be.fulfilled;
			await exposureLedger.close(0, {from: userAddr}).should.be.fulfilled;

			await drone.autoClose(exposureLedger, 0, defaultPrices).should.be.fulfilled;

			const exposureSystemData = await exposureLedger.getExposureSystemData(0);
			assert.equal(exposureSystemData[1], drone.states.STATE_CLOSED_BY_USER);
		});
	});

	describe("autoCloseMany()", () => {
		it("should return false if indexes array is empty", async() => {
			const autoCloseRes = await drone.autoCloseMany(exposureLedger, [], {});
			assert.isFalse(autoCloseRes);
		});

		it("should autoclose many exposures", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: web3.toWei("0.01","ether")}).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.BTC, {from: userAddr, value: web3.toWei("0.01","ether")}).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.INDEX1, {from: userAddr, value: web3.toWei("0.01","ether")}).should.be.fulfilled;		
			await drone.autoFundMany(exposureLedger, [0, 1, 2]);
			await exposureLedger.close(0, {from: userAddr}).should.be.fulfilled;
			await exposureLedger.close(1, {from: userAddr}).should.be.fulfilled;
			await exposureLedger.close(2, {from: userAddr}).should.be.fulfilled;
			 
			await drone.autoCloseMany(exposureLedger, [0, 1, 2], defaultPrices);

			const exposureSystemData1 = await exposureLedger.getExposureSystemData(0);
			const exposureResultData1 = await exposureLedger.getExposureResultData(0);
			const exposureSystemData2 = await exposureLedger.getExposureSystemData(1);
			const exposureResultData2 = await exposureLedger.getExposureResultData(1);
			const exposureSystemData3 = await exposureLedger.getExposureSystemData(2);
			const exposureResultData3 = await exposureLedger.getExposureResultData(2);
			assert.equal(exposureSystemData1[1], drone.states.STATE_CLOSED_BY_USER);
			assert.equal(exposureResultData1[0], defaultPrices.ETH * 100);
			assert.equal(exposureSystemData2[1], drone.states.STATE_CLOSED_BY_USER);
			assert.equal(exposureResultData2[0], defaultPrices.BTC * 100);
			assert.equal(exposureSystemData3[1], drone.states.STATE_CLOSED_BY_USER);
			assert.equal(exposureResultData3[0], defaultPrices.INDEX1 * 100);
		});
	});

	describe("autoFund()", () => {
		it("should autofund the exposure", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await drone.autoFund(exposureLedger, 0).should.be.fulfilled;

			const exposureSystemData = await exposureLedger.getExposureSystemData(0);
			assert.equal(exposureSystemData[1], drone.states.STATE_FUNDED_BY_PLATFORM);
		});
	});

	describe("autoFundMany()", () => {
		it("should return false on empty indexes array", async() => {
			const autoFunded = await drone.autoFundMany(exposureLedger, []);
			assert.isFalse(autoFunded);
		});

		it("should autofund many exposures", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: web3.toWei("0.01","ether")}).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: web3.toWei("0.01","ether")}).should.be.fulfilled;
			
			await drone.autoFundMany(exposureLedger, [0, 1]);

			const exposureSystemData1 = await exposureLedger.getExposureSystemData(0);
			const exposureSystemData2 = await exposureLedger.getExposureSystemData(1);
			assert.equal(exposureSystemData1[1], drone.states.STATE_FUNDED_BY_PLATFORM);
			assert.equal(exposureSystemData2[1], drone.states.STATE_FUNDED_BY_PLATFORM);
		});
	});

	describe("badEthRate()", () => {
		it("should mark exposure as bad eth rate", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await drone.badEthRate(exposureLedger, 0, defaultPrices).should.be.fulfilled;

			const exposureSystemData = await exposureLedger.getExposureSystemData(0);
			assert.equal(exposureSystemData[1], drone.states.STATE_BAD_ETH_RATE);
		});
	});

	describe("badEthRateMany()", () => {
		it("should return false if indexes array is empty", async() => {
			const badEthRateRes = await drone.badEthRateMany(exposureLedger, [], {});
			assert.isFalse(badEthRateRes);
		});

		it("should bad eth rate many exposures", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10600, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: web3.toWei("0.01","ether")}).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10600, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.BTC, {from: userAddr, value: web3.toWei("0.01","ether")}).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10600, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.INDEX1, {from: userAddr, value: web3.toWei("0.01","ether")}).should.be.fulfilled;

			await drone.badEthRateMany(exposureLedger, [0, 1, 2], defaultPrices).should.be.fulfilled;

			const exposureSystemData1 = await exposureLedger.getExposureSystemData(0);
			const exposureResultData1 = await exposureLedger.getExposureResultData(0);
			const exposureSystemData2 = await exposureLedger.getExposureSystemData(1);
			const exposureResultData2 = await exposureLedger.getExposureResultData(1);
			const exposureSystemData3 = await exposureLedger.getExposureSystemData(2);
			const exposureResultData3 = await exposureLedger.getExposureResultData(2);
			assert.equal(exposureSystemData1[1], drone.states.STATE_BAD_ETH_RATE);
			assert.equal(exposureResultData1[0], defaultPrices.ETH * 100);
			assert.equal(exposureSystemData2[1], drone.states.STATE_BAD_ETH_RATE);
			assert.equal(exposureResultData2[0], defaultPrices.BTC * 100);
			assert.equal(exposureSystemData3[1], drone.states.STATE_BAD_ETH_RATE);
			assert.equal(exposureResultData3[0], defaultPrices.INDEX1 * 100);
		});
	});

	describe("exposureInState()", () => {
		it("should return false if exposure is not in the provided state", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			assert.isFalse(await drone.exposureInState(exposureLedger, 0, drone.states.STATE_FUNDED_BY_PLATFORM)); 
		});

		it("should return true if exposure is in the provided state", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			assert.isTrue(await drone.exposureInState(exposureLedger, 0, drone.states.STATE_FUNDED_BY_USER)); 
		});
	});

	describe("forceClose()", () => {
		it("should force close the exposure", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;

			await drone.forceClose(exposureLedger, 0, defaultPrices).should.be.fulfilled;

			const exposureSystemData = await exposureLedger.getExposureSystemData(0);
			assert.equal(exposureSystemData[1], drone.states.STATE_CLOSED_BY_ADMIN);
		});
	});

	describe("forceCloseMany()", () => {
		it("should return false if indexes array is empty", async() => {
			const forceCloseRes = await drone.forceCloseMany(exposureLedger, [], {});
			assert.isFalse(forceCloseRes);
		});

		it("should force close many exposures", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: web3.toWei("0.01","ether")}).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.BTC, {from: userAddr, value: web3.toWei("0.01","ether")}).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.INDEX1, {from: userAddr, value: web3.toWei("0.01","ether")}).should.be.fulfilled;			
			await drone.autoFundMany(exposureLedger, [0, 1, 2]);
			 
			await drone.forceCloseMany(exposureLedger, [0, 1, 2], defaultPrices);

			const exposureSystemData1 = await exposureLedger.getExposureSystemData(0);
			const exposureResultData1 = await exposureLedger.getExposureResultData(0);
			const exposureSystemData2 = await exposureLedger.getExposureSystemData(1);
			const exposureResultData2 = await exposureLedger.getExposureResultData(1);
			const exposureSystemData3 = await exposureLedger.getExposureSystemData(2);
			const exposureResultData3 = await exposureLedger.getExposureResultData(2);
			assert.equal(exposureSystemData1[1], drone.states.STATE_CLOSED_BY_ADMIN);
			assert.equal(exposureResultData1[0], defaultPrices.ETH * 100);
			assert.equal(exposureSystemData2[1], drone.states.STATE_CLOSED_BY_ADMIN);
			assert.equal(exposureResultData2[0], defaultPrices.BTC * 100);
			assert.equal(exposureSystemData3[1], drone.states.STATE_CLOSED_BY_ADMIN);
			assert.equal(exposureResultData3[0], defaultPrices.INDEX1 * 100);
		});
	});

	describe("getExposureIndexesByAsset()", () => {
		it("should return empty array if there are no exposures", async() => {
			const indexesOfAsset = await drone.getExposureIndexesByAsset(exposureLedger, [], drone.assets.ETH);
			assert.deepEqual(indexesOfAsset, []);
		});

		it("should return exposure indexes by asset", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: web3.toWei("0.01","ether")}).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.BTC, {from: userAddr, value: web3.toWei("0.01","ether")}).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.INDEX1, {from: userAddr, value: web3.toWei("0.01","ether")}).should.be.fulfilled;

			const indexesOfEthAsset = await drone.getExposureIndexesByAsset(exposureLedger, [0,1,2], drone.assets.ETH);
			const indexesOfBtcAsset = await drone.getExposureIndexesByAsset(exposureLedger, [0,1,2], drone.assets.BTC);
			const indexesOfIndex1Asset = await drone.getExposureIndexesByAsset(exposureLedger, [0,1,2], drone.assets.INDEX1);
			assert.deepEqual(indexesOfEthAsset, [0]);
			assert.deepEqual(indexesOfBtcAsset, [1]);
			assert.deepEqual(indexesOfIndex1Asset, [2]);
		});
	});

	describe("getExposuresToProcess()", () => {
		it("should return exposures that should be processed in current drone cycle", async() => {
			const fundInWei = web3.toWei("0.001", "ether");
			const prices = { ETH: '100.00', BTC: '200.00', BCH: '320.00', LTC: '400.00', XRP: '500.00', INDEX1: '1500.00' };

			// exposures for auto fund
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: fundInWei}).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: fundInWei}).should.be.fulfilled;
			
			// exposures for bad eth rate
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10600, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: fundInWei}).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10600, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: fundInWei}).should.be.fulfilled;

			// exposures for auto close
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: fundInWei}).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: fundInWei}).should.be.fulfilled;
			await exposureLedger.autoFund(4, {from: env.DRONE_ADDRESS}).should.be.fulfilled;
			await exposureLedger.autoFund(5, {from: env.DRONE_ADDRESS}).should.be.fulfilled;
			await exposureLedger.close(4, {from: userAddr}).should.be.fulfilled;
			await exposureLedger.close(5, {from: userAddr}).should.be.fulfilled;

			// exposures for margin call price movement
			await exposureLedger.createAndFundNewExposure(false, drone.currencies.USD, 5000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: fundInWei}).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(false, drone.currencies.USD, 5000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: fundInWei}).should.be.fulfilled;
			await exposureLedger.autoFund(6, {from: env.DRONE_ADDRESS}).should.be.fulfilled;
			await exposureLedger.autoFund(7, {from: env.DRONE_ADDRESS}).should.be.fulfilled;

			// exposures for margin call max gain reached
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 5000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: fundInWei}).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 5000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: fundInWei}).should.be.fulfilled;
			await exposureLedger.autoFund(8, {from: env.DRONE_ADDRESS}).should.be.fulfilled;
			await exposureLedger.autoFund(9, {from: env.DRONE_ADDRESS}).should.be.fulfilled;

			// exposures for margin call time elapsed
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 1, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: fundInWei}).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 1, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: fundInWei}).should.be.fulfilled;
			await exposureLedger.autoFund(10, {from: env.DRONE_ADDRESS}).should.be.fulfilled;
			await exposureLedger.autoFund(11, {from: env.DRONE_ADDRESS}).should.be.fulfilled;
			await increaseTime(60 * 60 * 24 * 2);
			const block = await web3.eth.getBlock("latest");

			// closed exposures that shouldn't be processed
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: fundInWei}).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: fundInWei}).should.be.fulfilled;
			await exposureLedger.autoFund(12, {from: env.DRONE_ADDRESS}).should.be.fulfilled;
			await exposureLedger.autoFund(13, {from: env.DRONE_ADDRESS}).should.be.fulfilled;
			await exposureLedger.close(12, {from: userAddr}).should.be.fulfilled;
			await exposureLedger.close(13, {from: userAddr}).should.be.fulfilled;
			await exposureLedger.autoClose(12, 10000, {from: env.DRONE_ADDRESS}).should.be.fulfilled;
			await exposureLedger.autoClose(13, 10000, {from: env.DRONE_ADDRESS}).should.be.fulfilled;
			
			const exposuresToProcess = await drone.getExposuresToProcess(exposureLedger, prices, block.timestamp);

			assert.equal(exposuresToProcess.to_auto_fund[0], 0);
			assert.equal(exposuresToProcess.to_auto_fund[1], 1);
			assert.equal(exposuresToProcess.to_bad_eth_rate[0], 2);
			assert.equal(exposuresToProcess.to_bad_eth_rate[1], 3);
			assert.equal(exposuresToProcess.to_auto_close[0], 4);
			assert.equal(exposuresToProcess.to_auto_close[1], 5);
			assert.equal(exposuresToProcess.to_margin_call_price_movement[0], 6);
			assert.equal(exposuresToProcess.to_margin_call_price_movement[1], 7);
			assert.equal(exposuresToProcess.to_margin_call_max_gain_reached[0], 8);
			assert.equal(exposuresToProcess.to_margin_call_max_gain_reached[1], 9);
			assert.equal(exposuresToProcess.to_margin_call_time_elapsed[0], 10);
			assert.equal(exposuresToProcess.to_margin_call_time_elapsed[1], 11);
		});
	});

	describe("getLeverageCoef()", () => {
		it("should return 1 on 1:1 leverage", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const coef = await drone.getLeverageCoef(exposureLedger, 0);
			assert.equal(coef, 1);
		});

		it("should return 2 on 2:1 leverage", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.TwoToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const coef = await drone.getLeverageCoef(exposureLedger, 0);
			assert.equal(coef, 2);
		});

		it("should return 4 on 4:1 leverage", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.FourToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const coef = await drone.getLeverageCoef(exposureLedger, 0);
			assert.equal(coef, 4);
		});
	});

	describe("getOrderVolume()", () => {
		it("should return order volume for ETH asset", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const prices = { ETH: '100.00', BTC: '200.00', BCH: '300.00', LTC: '400.00', XRP: '500.00', INDEX1: '1500.00' };
			const volume = await drone.getOrderVolume(exposureLedger, 0, prices);
			assert.equal(volume, 0.1);
		});

		it("should return order volume for BTC asset", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.BTC, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const prices = { ETH: '100.00', BTC: '200.00', BCH: '300.00', LTC: '400.00', XRP: '500.00', INDEX1: '1500.00' };
			const volume = await drone.getOrderVolume(exposureLedger, 0, prices);
			assert.equal(volume, 0.05);
		});

		it("should return array if order volumes for INDEX1 asset", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.INDEX1, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const prices = { ETH: '100.00', BTC: '200.00', BCH: '320.00', LTC: '400.00', XRP: '500.00', INDEX1: '1500.00' };
			const volume = await drone.getOrderVolume(exposureLedger, 0, prices);
			assert.deepEqual(volume, [0.02, 0.01, 0.00625, 0.005, 0.004]);
		});
	});

	describe("getPriceRateDiff()", () => {
		it("should return price rate diff between open and real price", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const prices = { ETH: '50.00' };
			const priceRateDiff = await drone.getPriceRateDiff(exposureLedger, 0, prices);
			assert.equal(priceRateDiff, 50);
		});
	});

	describe("getRealPrice()", () => {
		it("should return real price for ETH asset", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const realPrice = await drone.getRealPrice(exposureLedger, 0, defaultPrices);
			assert.equal(realPrice, 1.00); 
		});

		it("should return real price for BTC asset", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.BTC, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const realPrice = await drone.getRealPrice(exposureLedger, 0, defaultPrices);
			assert.equal(realPrice, 2.00); 
		});

		it("should return real price for INDEX1 asset", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.INDEX1, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const realPrice = await drone.getRealPrice(exposureLedger, 0, defaultPrices);
			assert.equal(realPrice, 3.00); 
		});
	});

	describe("getRealPriceByAsset()", () => {
		it("should return real price by ETH asset", async() => {
			const realPrice = await drone.getRealPriceByAsset(defaultPrices, drone.assets.ETH);
			assert.equal(realPrice, 1.00); 
		});

		it("should return real price by BTC asset", async() => {
			const realPrice = await drone.getRealPriceByAsset(defaultPrices, drone.assets.BTC);
			assert.equal(realPrice, 2.00); 
		});

		it("should return real price by INDEX1 asset", async() => {
			const realPrice = await drone.getRealPriceByAsset(defaultPrices, drone.assets.INDEX1);
			assert.equal(realPrice, 3.00); 
		});
	});

	describe("getTickerByAsset()", () => {
		it("should return ticker name by ETH asset", async() => {
			const tickerName = await drone.getTickerByAsset(drone.assets.ETH);
			assert.equal(tickerName, "XETHZUSD"); 
		});

		it("should return ticker name by BTC asset", async() => {
			const tickerName = await drone.getTickerByAsset(drone.assets.BTC);
			assert.equal(tickerName, "XXBTZUSD"); 
		});

		it("should return array of ticker names by INDEX1 asset", async() => {
			const tickerNames = await drone.getTickerByAsset(drone.assets.INDEX1);
			assert.deepEqual(tickerNames, drone.indexBasket.INDEX1); 
		});
	});

	describe("hedge()", () => {
		it("should buy ETH on long position and hedge opening with 1:1 leverage", async() => {
			const prices = { ETH: '100.00', BTC: '200.00', BCH: '300.00', LTC: '400.00', XRP: '500.00', INDEX1: '1500.00' };
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const hedgeRes = await drone.hedge("development", exposureLedger, 0, true, prices);
			assert.equal(hedgeRes.type, "buy");
			assert.deepEqual(hedgeRes.tickers, ["XETHZUSD"]);
			assert.deepEqual(hedgeRes.volumes, [+web3.fromWei(initialFund, "ether")]);
		});

		it("should buy BTC on long position and hedge opening with 1:1 leverage", async() => {
			const prices = { ETH: '100.00', BTC: '200.00', BCH: '300.00', LTC: '400.00', XRP: '500.00', INDEX1: '1500.00' };
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.BTC, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const hedgeRes = await drone.hedge("development", exposureLedger, 0, true, prices);
			assert.equal(hedgeRes.type, "buy");
			assert.deepEqual(hedgeRes.tickers, ["XXBTZUSD"]);
			assert.deepEqual(hedgeRes.volumes, [+web3.fromWei(initialFund, "ether") * +prices.ETH / +prices.BTC]);
		});

		it("should buy assets of INDEX1 on long position and hedge opening with 1:1 leverage", async() => {
			const prices = { ETH: '100.00', BTC: '200.00', BCH: '320.00', LTC: '400.00', XRP: '500.00', INDEX1: '1500.00' };
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.INDEX1, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const hedgeRes = await drone.hedge("development", exposureLedger, 0, true, prices);
			assert.equal(hedgeRes.type, "buy");
			assert.deepEqual(hedgeRes.tickers, drone.indexBasket.INDEX1);
			assert.deepEqual(hedgeRes.volumes, [0.02, 0.01, 0.00625, 0.005, 0.004]);
		});

		it("should buy ETH on long position and hedge opening with 2:1 leverage", async() => {
			const prices = { ETH: '100.00', BTC: '200.00', BCH: '300.00', LTC: '400.00', XRP: '500.00', INDEX1: '1500.00' };
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 1, 30, returnUserAddr, drone.leverages.TwoToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const hedgeRes = await drone.hedge("development", exposureLedger, 0, true, prices);
			assert.equal(hedgeRes.type, "buy");
			assert.deepEqual(hedgeRes.tickers, ["XETHZUSD"]);
			assert.deepEqual(hedgeRes.volumes, [+web3.fromWei(initialFund * 2, "ether")]);
		});

		it("should buy BTC on long position and hedge opening with 2:1 leverage", async() => {
			const prices = { ETH: '100.00', BTC: '200.00', BCH: '300.00', LTC: '400.00', XRP: '500.00', INDEX1: '1500.00' };
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 1, 30, returnUserAddr, drone.leverages.TwoToOne, drone.assets.BTC, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const hedgeRes = await drone.hedge("development", exposureLedger, 0, true, prices);
			assert.equal(hedgeRes.type, "buy");
			assert.deepEqual(hedgeRes.tickers, ["XXBTZUSD"]);
			assert.deepEqual(hedgeRes.volumes, [+web3.fromWei(initialFund * 2, "ether") * +prices.ETH / +prices.BTC]);
		});

		it("should buy assets of INDEX1 on long position and hedge opening with 2:1 leverage", async() => {
			const prices = { ETH: '100.00', BTC: '200.00', BCH: '320.00', LTC: '400.00', XRP: '500.00', INDEX1: '1500.00' };
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 1, 30, returnUserAddr, drone.leverages.TwoToOne, drone.assets.INDEX1, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const hedgeRes = await drone.hedge("development", exposureLedger, 0, true, prices);
			assert.equal(hedgeRes.type, "buy");
			assert.deepEqual(hedgeRes.tickers, drone.indexBasket.INDEX1);
			assert.deepEqual(hedgeRes.volumes, [0.04, 0.02, 0.0125, 0.01, 0.008]);
		});

		it("should buy ETH on long position and hedge opening with 4:1 leverage", async() => {
			const prices = { ETH: '100.00', BTC: '200.00', BCH: '320.00', LTC: '400.00', XRP: '500.00', INDEX1: '1500.00' };
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 1, 30, returnUserAddr, drone.leverages.FourToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const hedgeRes = await drone.hedge("development", exposureLedger, 0, true, prices);
			assert.equal(hedgeRes.type, "buy");
			assert.deepEqual(hedgeRes.tickers, ["XETHZUSD"]);
			assert.deepEqual(hedgeRes.volumes, [+web3.fromWei(initialFund * 4, "ether")]);
		});

		it("should sell ETH on short position and hedge opening with 1:1 leverage", async() => {
			const prices = { ETH: '100.00', BTC: '200.00', BCH: '320.00', LTC: '400.00', XRP: '500.00', INDEX1: '1500.00' };
			await exposureLedger.createAndFundNewExposure(false, drone.currencies.USD, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const hedgeRes = await drone.hedge("development", exposureLedger, 0, true, prices);
			assert.equal(hedgeRes.type, "sell");
			assert.deepEqual(hedgeRes.tickers, ["XETHZUSD"]);
			assert.deepEqual(hedgeRes.volumes, [+web3.fromWei(initialFund, "ether")]);
		});

		it("should sell ETH on short position and hedge opening with 2:1 leverage", async() => {
			const prices = { ETH: '100.00', BTC: '200.00', BCH: '320.00', LTC: '400.00', XRP: '500.00', INDEX1: '1500.00' };
			await exposureLedger.createAndFundNewExposure(false, drone.currencies.USD, 1, 30, returnUserAddr, drone.leverages.TwoToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const hedgeRes = await drone.hedge("development", exposureLedger, 0, true, prices);
			assert.equal(hedgeRes.type, "sell");
			assert.deepEqual(hedgeRes.tickers, ["XETHZUSD"]);
			assert.deepEqual(hedgeRes.volumes, [+web3.fromWei(initialFund * 2, "ether")]);
		});

		it("should sell ETH on short position and hedge opening with 4:1 leverage", async() => {
			const prices = { ETH: '100.00', BTC: '200.00', BCH: '320.00', LTC: '400.00', XRP: '500.00', INDEX1: '1500.00' };
			await exposureLedger.createAndFundNewExposure(false, drone.currencies.USD, 1, 30, returnUserAddr, drone.leverages.FourToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const hedgeRes = await drone.hedge("development", exposureLedger, 0, true, prices);
			assert.equal(hedgeRes.type, "sell");
			assert.deepEqual(hedgeRes.tickers, ["XETHZUSD"]);
			assert.deepEqual(hedgeRes.volumes, [+web3.fromWei(initialFund * 4, "ether")]);
		});

		it("should sell ETH on long position and hedge closing with 1:1 leverage", async() => {
			const prices = { ETH: '100.00', BTC: '200.00', BCH: '320.00', LTC: '400.00', XRP: '500.00', INDEX1: '1500.00' };
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const hedgeRes = await drone.hedge("development", exposureLedger, 0, false, prices);
			assert.equal(hedgeRes.type, "sell");
			assert.deepEqual(hedgeRes.tickers, ["XETHZUSD"]);
			assert.deepEqual(hedgeRes.volumes, [+web3.fromWei(initialFund, "ether")]);
		});

		it("should sell ETH on long position and hedge closing with 2:1 leverage", async() => {
			const prices = { ETH: '100.00', BTC: '200.00', BCH: '320.00', LTC: '400.00', XRP: '500.00', INDEX1: '1500.00' };
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 1, 30, returnUserAddr, drone.leverages.TwoToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const hedgeRes = await drone.hedge("development", exposureLedger, 0, false, prices);
			assert.equal(hedgeRes.type, "sell");
			assert.deepEqual(hedgeRes.tickers, ["XETHZUSD"]);
			assert.deepEqual(hedgeRes.volumes, [+web3.fromWei(initialFund * 2, "ether")]);
		});

		it("should sell ETH on long position and hedge closing with 4:1 leverage", async() => {
			const prices = { ETH: '100.00', BTC: '200.00', BCH: '320.00', LTC: '400.00', XRP: '500.00', INDEX1: '1500.00' };
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 1, 30, returnUserAddr, drone.leverages.FourToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const hedgeRes = await drone.hedge("development", exposureLedger, 0, false, prices);
			assert.equal(hedgeRes.type, "sell");
			assert.deepEqual(hedgeRes.tickers, ["XETHZUSD"]);
			assert.deepEqual(hedgeRes.volumes, [+web3.fromWei(initialFund * 4, "ether")]);
		});

		it("should buy ETH on short position and hedge closing with 1:1 leverage", async() => {
			const prices = { ETH: '100.00', BTC: '200.00', BCH: '320.00', LTC: '400.00', XRP: '500.00', INDEX1: '1500.00' };
			await exposureLedger.createAndFundNewExposure(false, drone.currencies.USD, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const hedgeRes = await drone.hedge("development", exposureLedger, 0, false, prices);
			assert.equal(hedgeRes.type, "buy");
			assert.deepEqual(hedgeRes.tickers, ["XETHZUSD"]);
			assert.deepEqual(hedgeRes.volumes, [+web3.fromWei(initialFund, "ether")]);
		});

		it("should buy ETH on short position and hedge closing with 2:1 leverage", async() => {
			const prices = { ETH: '100.00', BTC: '200.00', BCH: '320.00', LTC: '400.00', XRP: '500.00', INDEX1: '1500.00' };
			await exposureLedger.createAndFundNewExposure(false, drone.currencies.USD, 1, 30, returnUserAddr, drone.leverages.TwoToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const hedgeRes = await drone.hedge("development", exposureLedger, 0, false, prices);
			assert.equal(hedgeRes.type, "buy");
			assert.deepEqual(hedgeRes.tickers, ["XETHZUSD"]);
			assert.deepEqual(hedgeRes.volumes, [+web3.fromWei(initialFund * 2, "ether")]);
		});

		it("should buy ETH on short position and hedge closing with 4:1 leverage", async() => {
			const prices = { ETH: '100.00', BTC: '200.00', BCH: '320.00', LTC: '400.00', XRP: '500.00', INDEX1: '1500.00' };
			await exposureLedger.createAndFundNewExposure(false, drone.currencies.USD, 1, 30, returnUserAddr, drone.leverages.FourToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const hedgeRes = await drone.hedge("development", exposureLedger, 0, false, prices);
			assert.equal(hedgeRes.type, "buy");
			assert.deepEqual(hedgeRes.tickers, ["XETHZUSD"]);
			assert.deepEqual(hedgeRes.volumes, [+web3.fromWei(initialFund * 4, "ether")]);
		});
	});

	describe("hedgeMany()", () => {
		it("should return false if indexes array is empty", async() => {
			const hedged = await drone.hedgeMany("development", exposureLedger, [], true, {});
			assert.isFalse(hedged);
		});

		it("should return hedge result", async() => {
			const prices = { ETH: '100.00', BTC: '200.00', BCH: '320.00', LTC: '400.00', XRP: '500.00', INDEX1: '1500.00' };
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: web3.toWei("0.01","ether")}).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: web3.toWei("0.01","ether")}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: env.DRONE_ADDRESS}).should.be.fulfilled;
			await exposureLedger.autoFund(1, {from: env.DRONE_ADDRESS}).should.be.fulfilled;

			const hedgeResult = await drone.hedgeMany("development", exposureLedger, [0, 1], true, prices);

			assert.equal(hedgeResult[0].type, "buy");
			assert.deepEqual(hedgeResult[0].tickers, ["XETHZUSD"]);
			assert.deepEqual(hedgeResult[0].volumes, [0.01]);
			assert.equal(hedgeResult[1].type, "buy");
			assert.deepEqual(hedgeResult[1].tickers, ["XETHZUSD"]);
			assert.deepEqual(hedgeResult[1].volumes, [0.01]);
		});
	});

	describe("marginCall()", () => {
		it("should close exposure by margin call", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 1, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: env.DRONE_ADDRESS}).should.be.fulfilled;

			await drone.marginCall(exposureLedger, 0, drone.states.STATE_MARGIN_CALL_TIME_ELAPSED, defaultPrices).should.be.fulfilled;

			const exposureSystemData = await exposureLedger.getExposureSystemData(0);
			assert.equal(exposureSystemData[1], drone.states.STATE_MARGIN_CALL_TIME_ELAPSED);
		});
	});

	describe("marginCallMany()", () => {
		it("should return false if indexes array is empty", async() => {
			const marginCallRes = await drone.marginCallMany(exposureLedger, [], 0, {});
			assert.isFalse(marginCallRes);
		});

		it("should close many exposure by margin call", async() => {
			await exposureLedger.createAndFundNewExposure(false, drone.currencies.USD, 5000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: web3.toWei("0.01","ether")}).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(false, drone.currencies.USD, 5000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.BTC, {from: userAddr, value: web3.toWei("0.01","ether")}).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(false, drone.currencies.USD, 5000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.INDEX1, {from: userAddr, value: web3.toWei("0.01","ether")}).should.be.fulfilled;
			await drone.autoFundMany(exposureLedger, [0, 1, 2]).should.be.fulfilled;

			await drone.marginCallMany(exposureLedger, [0, 1, 2], drone.states.STATE_MARGIN_CALL_PRICE_MOVEMENT, defaultPrices);

			const exposureSystemData1 = await exposureLedger.getExposureSystemData(0);
			const exposureResultData1 = await exposureLedger.getExposureResultData(0);
			const exposureSystemData2 = await exposureLedger.getExposureSystemData(1);
			const exposureResultData2 = await exposureLedger.getExposureResultData(1);
			const exposureSystemData3 = await exposureLedger.getExposureSystemData(2);
			const exposureResultData3 = await exposureLedger.getExposureResultData(2);
			assert.equal(exposureSystemData1[1], drone.states.STATE_MARGIN_CALL_PRICE_MOVEMENT);
			assert.equal(exposureResultData1[0], defaultPrices.ETH * 100);
			assert.equal(exposureSystemData2[1], drone.states.STATE_MARGIN_CALL_PRICE_MOVEMENT);
			assert.equal(exposureResultData2[0], defaultPrices.BTC * 100);
			assert.equal(exposureSystemData3[1], drone.states.STATE_MARGIN_CALL_PRICE_MOVEMENT);
			assert.equal(exposureResultData3[0], defaultPrices.INDEX1 * 100);
		});
	});

	describe("maxUserGain()", () => {
		it("should return false if user didn't win everything", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const prices = { ETH: '198.00' };
			const maxUserGain = await drone.maxUserGain(exposureLedger, 0, prices);
			assert.isFalse(maxUserGain);
		});

		it("should return true on price max down and short position", async() => {
			await exposureLedger.createAndFundNewExposure(false, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const prices = { ETH: '1.00' };
			const maxUserGain = await drone.maxUserGain(exposureLedger, 0, prices);
			assert.isTrue(maxUserGain);
		});

		it("should return true on price max up and long position", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const prices = { ETH: '199.00' };
			const maxUserGain = await drone.maxUserGain(exposureLedger, 0, prices);
			assert.isTrue(maxUserGain);
		});
	});

	describe("maxUserLoss()", () => {
		it("should return false if user didn't lose everything with 1:1 leverage", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const prices = { ETH: '50.01' };
			const maxUserLoss = await drone.maxUserLoss(exposureLedger, 0, prices);
			assert.isFalse(maxUserLoss);
		});

		it("should return false if user didn't lose everything with 2:1 leverage", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.TwoToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const prices = { ETH: '25.01' };
			const maxUserLoss = await drone.maxUserLoss(exposureLedger, 0, prices);
			assert.isFalse(maxUserLoss);
		});

		it("should return false if user didn't lose everything with 4:1 leverage", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.FourToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const prices = { ETH: '12.51' };
			const maxUserLoss = await drone.maxUserLoss(exposureLedger, 0, prices);
			assert.isFalse(maxUserLoss);
		});

		it("should return true on price max down and long position with 1:1 leverage", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const prices = { ETH: '50.00' };
			const maxUserLoss = await drone.maxUserLoss(exposureLedger, 0, prices);
			assert.isTrue(maxUserLoss);
		});

		it("should return true on price max down and long position with 2:1 leverage", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.TwoToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const prices = { ETH: '25.00' };
			const maxUserLoss = await drone.maxUserLoss(exposureLedger, 0, prices);
			assert.isTrue(maxUserLoss);
		});

		it("should return true on price max down and long position with 4:1 leverage", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.FourToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const prices = { ETH: '12.50' };
			const maxUserLoss = await drone.maxUserLoss(exposureLedger, 0, prices);
			assert.isTrue(maxUserLoss);
		});

		it("should return true on price max up and short position with 1:1 leverage", async() => {
			await exposureLedger.createAndFundNewExposure(false, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const prices = { ETH: '150.00' };
			const maxUserLoss = await drone.maxUserLoss(exposureLedger, 0, prices);
			assert.isTrue(maxUserLoss);
		});

		it("should return true on price max up and short position with 2:1 leverage", async() => {
			await exposureLedger.createAndFundNewExposure(false, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.TwoToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const prices = { ETH: '175.00' };
			const maxUserLoss = await drone.maxUserLoss(exposureLedger, 0, prices);
			assert.isTrue(maxUserLoss);
		});

		it("should return true on price max up and short position with 4:1 leverage", async() => {
			await exposureLedger.createAndFundNewExposure(false, drone.currencies.USD, 10000, 30, returnUserAddr, drone.leverages.FourToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const prices = { ETH: '187.50' };
			const maxUserLoss = await drone.maxUserLoss(exposureLedger, 0, prices);
			assert.isTrue(maxUserLoss);
		});
	});

	describe("neatLog()", () => {
		it("should return formatted message", () => {
			const msg = drone.neatLog("ANY_THEME", "word1", "word2");
			const formattedDate = new Date().toLocaleString("en-US", {timeZone: env.TIMEZONE});
			const formattedMsg = `${formattedDate} ANY_THEME word1,word2`;
			assert.equal(formattedMsg, msg);
		});
	});

	describe("timeElapsed()", () => {
		it("should return false if exposure is not finished", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 1, 1, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const lastBlock = await web3.eth.getBlock("latest");
			const timeElapsed = await drone.timeElapsed(exposureLedger, 0, lastBlock.timestamp);
			assert.isFalse(timeElapsed);
		});

		it("should return true if exposure is finished", async() => {
			await exposureLedger.createAndFundNewExposure(true, drone.currencies.USD, 1, 1, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await increaseTime(60 * 60 * 24 * 2);
			const lastBlock = await web3.eth.getBlock("latest");
			const timeElapsed = await drone.timeElapsed(exposureLedger, 0, lastBlock.timestamp);
			assert.isTrue(timeElapsed);
		});
	});

	describe("validateExposure()", () => {
		it("should return false if real price is beyond price borders", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const realPrices = {ETH: "94.99"};
			const valid = await drone.validateExposure(exposureLedger, 0, realPrices);
			assert.isFalse(valid);
		});

		it("should return true if real price is within price borders", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 10000, 30, returnUserAddr, drone.leverages.OneToOne, drone.assets.ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const realPrices = {ETH: "95.00"};
			const valid = await drone.validateExposure(exposureLedger, 0, realPrices);
			assert.isTrue(valid);
		});
	});
});
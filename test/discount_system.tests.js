require('chai').use(require('chai-as-promised')).should();
const { increaseTime } = require('./utils/helpers.js');

const MintableToken = artifacts.require("MintableToken");
const DiscountSystem = artifacts.require("DiscountSystemTestable");

contract("DiscountSystem", (accounts) => {

	const TOKEN_OPERATION_TYPE_STAKE = 0;
	const TOKEN_OPERATION_TYPE_WITHDRAW = 1;
	const TOKEN_OPERATION_TYPE_USE_FOR_FEE_DISCOUNT = 2;
	const TOKEN_OPERATION_TYPE_APPLY_FOR_FEE_DISCOUNT = 3;

    const ownerAddress = accounts[0];
	const adminAddress = accounts[1];
	const otherAddress = accounts[2];
	const userAddress = accounts[3];
	
	let discountToken;
	let discountSystem;

	beforeEach(async() => {
		discountToken = await MintableToken.new();
		await discountToken.mint(userAddress, 1);
		discountSystem = await DiscountSystem.new(discountToken.address, adminAddress, 10, 10000, 30);
	});

	describe("applyForFeeDiscount()", () => {
		it("should revert if user address is 0x00", async() => {
			await discountSystem.applyForFeeDiscount(0x00).should.be.rejectedWith("revert"); 
		});

		it("should create apply for fee discount token operation", async() => {
			await discountSystem.applyForFeeDiscount(userAddress, {from: ownerAddress}).should.be.fulfilled;

			const tokenOperation = await discountSystem.getTokenOperation(userAddress, 0, {from: userAddress});
			assert.equal(tokenOperation[0], TOKEN_OPERATION_TYPE_APPLY_FOR_FEE_DISCOUNT);
			assert.notEqual(tokenOperation[1], 0);
			assert.equal(tokenOperation[2], 0);
		});
	});

	describe("changeAdminAddress()", () => {
		it("should revert if new admin address is 0x00", async() => {
			await discountSystem.changeAdminAddress(0x00).should.be.rejectedWith("revert"); 
		});

		it("should change admin address", async() => {
			await discountSystem.changeAdminAddress(otherAddress).should.be.fulfilled;
			assert.equal(await discountSystem.adminAddress(), otherAddress); 
		});
	});

	describe("changeDaysOfIdleToResetPoints()", () => {
		it("should change number of days of idle to reset points", async() => {
			await discountSystem.changeDaysOfIdleToResetPoints(1, {from: adminAddress}).should.be.fulfilled;
			assert.equal(await discountSystem.daysOfIdleToResetPoints(), 1); 
		});
	});

	describe("changePointsPerTokensCount()", () => {
		it("should revert if points per tokens count is 0", async() => {
			await discountSystem.changePointsPerTokensCount(0, 1, {from: adminAddress}).should.be.rejectedWith("revert"); 
		});

		it("should revert if tokens per points count is 0", async() => {
			await discountSystem.changePointsPerTokensCount(1, 0, {from: adminAddress}).should.be.rejectedWith("revert"); 
		});

		it("should change amount of points per amount of tokens", async() => {
			await discountSystem.changePointsPerTokensCount(1, 2, {from: adminAddress}).should.be.fulfilled;
			assert.equal(await discountSystem.pointsPerTokensCount(), 1);
			assert.equal(await discountSystem.tokensPerPointsCount(), 2); 
		});
	});

    describe("constructor()", () => {
		it("should revert if token address is 0x00", async() => {
			const invalidTokenAddr = 0x00;
			await DiscountSystem.new(invalidTokenAddr, adminAddress, 10, 10000, 7).should.be.rejectedWith("revert");
		});

		it("should revert if admin address is 0x00", async() => {
			const invalidAdminAddr = 0x00;
			await DiscountSystem.new(discountToken.address, invalidAdminAddr, 10, 10000, 7).should.be.rejectedWith("revert");
		});

		it("should revert if points per tokens count is 0", async() => {
			await DiscountSystem.new(discountToken.address, adminAddress, 0, 10000, 7).should.be.rejectedWith("revert");
		});

		it("should revert if tolens per points count is 0", async() => {
			await DiscountSystem.new(discountToken.address, adminAddress, 10, 0, 7).should.be.rejectedWith("revert");
		});

		it("should set contract properties", async() => {
			assert.equal(await discountSystem.discountToken(), discountToken.address);
			assert.equal(await discountSystem.adminAddress(), adminAddress);
			assert.equal(await discountSystem.pointsPerTokensCount(), 10);
			assert.equal(await discountSystem.tokensPerPointsCount(), 10000);
			assert.equal(await discountSystem.daysOfIdleToResetPoints(), 30);
		});
	});

	describe("createDiscountStage()", () => {
		it("should create a new discount stage", async() => {
			await discountSystem.createDiscountStage(10, 50000, {from: adminAddress}).should.be.fulfilled;
			const discountStage = await discountSystem.discountStages(0);
			assert.equal(discountStage[0], 10);
			assert.equal(discountStage[1], 50000);
		});
	});

	describe("createTokenOperation()", () => {
		it("should revert if user address is 0x00", async() => {
			await discountSystem.createTokenOperation(0x00, 1, TOKEN_OPERATION_TYPE_STAKE, {from: userAddress}).should.be.rejectedWith("revert");
		});

		it("should add token operation", async() => {
			await discountToken.approve(discountSystem.address, 1, {from: userAddress}).should.be.fulfilled;
			await discountSystem.createTokenOperation(userAddress, 1, TOKEN_OPERATION_TYPE_STAKE, {from: userAddress}).should.be.fulfilled;
			const tokenOperation = await discountSystem.getTokenOperation(userAddress, 0);
			assert.equal(tokenOperation[0], TOKEN_OPERATION_TYPE_STAKE);
			assert.notEqual(tokenOperation[1], 0);
			assert.equal(tokenOperation[2], 1);
		});

		it("should update discount properties", async() => {
			await discountToken.approve(discountSystem.address, 1, {from: userAddress}).should.be.fulfilled;

			const discountBefore = await discountSystem.userDiscount(userAddress);
			assert.equal(discountBefore[0], 0);

			await increaseTime(1);
			await discountSystem.createTokenOperation(userAddress, 1, TOKEN_OPERATION_TYPE_STAKE, {from: userAddress}).should.be.fulfilled;

			const discountAfter = await discountSystem.userDiscount(userAddress);
			assert.equal(discountAfter[0], 1);
			assert.notEqual(discountAfter[2], discountBefore[2]);
		});

		it("should stake tokens", async() => {
			await discountToken.approve(discountSystem.address, 1, {from: userAddress}).should.be.fulfilled;

			const systemBalanceBefore = await discountToken.balanceOf(discountSystem.address);
			const userBalanceBefore = await discountToken.balanceOf(userAddress);
			const discountBefore = await discountSystem.userDiscount(userAddress);
			assert.equal(systemBalanceBefore, 0);
			assert.equal(userBalanceBefore, 1);
			assert.equal(discountBefore[1], 0);

			await discountSystem.createTokenOperation(userAddress, 1, TOKEN_OPERATION_TYPE_STAKE, {from: userAddress}).should.be.fulfilled;

			const systemBalanceAfter = await discountToken.balanceOf(discountSystem.address);
			const userBalanceAfter = await discountToken.balanceOf(userAddress);
			const discountAfter = await discountSystem.userDiscount(userAddress);
			assert.equal(systemBalanceAfter, 1);
			assert.equal(userBalanceAfter, 0);
			assert.equal(discountAfter[1], 1);
		});

		it("should withdraw tokens", async() => {
			await discountToken.approve(discountSystem.address, 1, {from: userAddress}).should.be.fulfilled;
			await discountSystem.createTokenOperation(userAddress, 1, TOKEN_OPERATION_TYPE_STAKE, {from: userAddress}).should.be.fulfilled;

			const systemBalanceBefore = await discountToken.balanceOf(discountSystem.address);
			const userBalanceBefore = await discountToken.balanceOf(userAddress);
			const discountBefore = await discountSystem.userDiscount(userAddress);
			assert.equal(systemBalanceBefore, 1);
			assert.equal(userBalanceBefore, 0);
			assert.equal(discountBefore[1], 1);

			await discountSystem.createTokenOperation(userAddress, 1, TOKEN_OPERATION_TYPE_WITHDRAW, {from: userAddress}).should.be.fulfilled;

			const systemBalanceAfter = await discountToken.balanceOf(discountSystem.address);
			const userBalanceAfter = await discountToken.balanceOf(userAddress);
			const discountAfter = await discountSystem.userDiscount(userAddress);
			assert.equal(systemBalanceAfter, 0);
			assert.equal(userBalanceAfter, 1);
			assert.equal(discountAfter[1], 0);
		});

		it("should use tokens for fee discount", async() => {
			await discountToken.approve(discountSystem.address, 1, {from: userAddress}).should.be.fulfilled;
			await discountSystem.createTokenOperation(userAddress, 1, TOKEN_OPERATION_TYPE_STAKE, {from: userAddress}).should.be.fulfilled;

			const systemBalanceBefore = await discountToken.balanceOf(discountSystem.address);
			const userBalanceBefore = await discountToken.balanceOf(userAddress);
			assert.equal(systemBalanceBefore, 1);
			assert.equal(userBalanceBefore, 0);

			await discountSystem.createTokenOperation(userAddress, 1, TOKEN_OPERATION_TYPE_USE_FOR_FEE_DISCOUNT, {from: userAddress}).should.be.fulfilled;

			const systemBalanceAfter = await discountToken.balanceOf(discountSystem.address);
			const userBalanceAfter = await discountToken.balanceOf(userAddress);
			assert.equal(systemBalanceAfter, 1);
			assert.equal(userBalanceAfter, 0);
		});
	});

	describe("deleteDiscountStage()", () => {
		it("should revert if discount stage does not exist", async() => {
			await discountSystem.createDiscountStage(10, 50000, {from: adminAddress}).should.be.fulfilled;
			await discountSystem.deleteDiscountStage(1, {from: adminAddress}).should.be.rejectedWith("revert");
		});

		it("should delete discount stage", async() => {
			await discountSystem.createDiscountStage(10, 50000, {from: adminAddress}).should.be.fulfilled;
			await discountSystem.deleteDiscountStage(0, {from: adminAddress}).should.be.fulfilled;
			const discountStage = await discountSystem.discountStages(0).should.be.rejectedWith("invalid opcode");
		});
	});

	describe("getDaysPassed()", () => {
		it("should revert if user address is 0x00", async() => {
			await discountSystem.getDaysPassed(0x00, 0, {from: userAddress}).should.be.rejectedWith("revert");
		});

		it("should revert if token operation does not exist", async() => {
			await discountSystem.getDaysPassed(userAddress, 0, {from: userAddress}).should.be.rejectedWith("revert");
		});

		it("should return days between current and the next token operation", async() => {
			await discountToken.approve(discountSystem.address, 1, {from: userAddress}).should.be.fulfilled;
			await discountSystem.stake(1, {from: userAddress}).should.be.fulfilled;
			await increaseTime(60 * 60 * 24 * 1);
			await discountSystem.withdraw(1, {from: userAddress}).should.be.fulfilled;
			const daysPassed = await discountSystem.getDaysPassed(userAddress, 0, {from: userAddress});
			assert.equal(daysPassed, 1);
		});

		it("should return days passed between current token operation and current timestamp", async() => {
			await discountToken.approve(discountSystem.address, 1, {from: userAddress}).should.be.fulfilled;
			await discountSystem.stake(1, {from: userAddress}).should.be.fulfilled;
			await increaseTime(60 * 60 * 24 * 1);
			const daysPassed = await discountSystem.getDaysPassed(userAddress, 0, {from: userAddress});
			assert.equal(daysPassed, 1);
		});
	});

	describe("getDiscountStageByUserAddress()", () => {
		it("should revert if there are no discount stages", async() => {
			await discountSystem.getDiscountStageByUserAddress(userAddress, {from: userAddress}).should.be.rejectedWith("revert");
		});

		it("should return 0 discounts points and rate if user has not enough points", async() => {
			// create 1 stage
			await discountSystem.createDiscountStage(10, 50000, {from: adminAddress}).should.be.fulfilled;
			// mint and stake 10k tokens per 4 days
			await discountToken.mint(userAddress, 10000, {from: ownerAddress}).should.be.fulfilled;
			await discountToken.approve(discountSystem.address, 10000, {from: userAddress}).should.be.fulfilled;
			await discountSystem.stake(10000, {from: userAddress}).should.be.fulfilled;
			const discountStage = await discountSystem.getDiscountStageByUserAddress(userAddress, {from: userAddress});
			assert.equal(discountStage[0], 0);
			assert.equal(discountStage[1], 0);
		});

		it("should return discount stage by user address", async() => {
			// create 3 stages
			await discountSystem.createDiscountStage(10, 50000, {from: adminAddress}).should.be.fulfilled;
			await discountSystem.createDiscountStage(20, 100000, {from: adminAddress}).should.be.fulfilled;
			await discountSystem.createDiscountStage(40, 200000, {from: adminAddress}).should.be.fulfilled;
			// mint and stake 10k tokens per 4 days
			await discountToken.mint(userAddress, 10000, {from: ownerAddress}).should.be.fulfilled;
			await discountToken.approve(discountSystem.address, 10000, {from: userAddress}).should.be.fulfilled;
			await discountSystem.stake(10000, {from: userAddress}).should.be.fulfilled;
			await increaseTime(60 * 60 * 24 * 4);
			const discountStage = await discountSystem.getDiscountStageByUserAddress(userAddress, {from: userAddress});
			assert.equal(discountStage[0], 40);
			assert.equal(discountStage[1], 200000);
		});
	});

	describe("getMinDiscountStage()", () => {
		it("should revert if there are no discount stages", async() => {
			await discountSystem.getMinDiscountStage({from: userAddress}).should.be.rejectedWith("revert");
		});

		it("should return min discount stage", async() => {
			await discountSystem.createDiscountStage(40, 200000, {from: adminAddress}).should.be.fulfilled;
			await discountSystem.createDiscountStage(20, 100000, {from: adminAddress}).should.be.fulfilled;
			await discountSystem.createDiscountStage(10, 50000, {from: adminAddress}).should.be.fulfilled;
			const discountStage = await discountSystem.getMinDiscountStage({from: userAddress});
			assert.equal(discountStage[0], 10);
			assert.equal(discountStage[1], 50000);
		});
	});

	describe("getPointsCountByUserAddress()", () => {
		it("should revert if user address is 0x00", async() => {
			await discountSystem.getPointsCountByUserAddress(0x00, {from: userAddress}).should.be.rejectedWith("revert");
		});

		it("should return points count with single stake operation", async() => {
			await discountSystem.createDiscountStage(10, 50000, {from: adminAddress}).should.be.fulfilled;
			await discountToken.mint(userAddress, 10000, {from: ownerAddress}).should.be.fulfilled;
			await discountToken.approve(discountSystem.address, 10000, {from: userAddress}).should.be.fulfilled;
			await discountSystem.stake(10000, {from: userAddress}).should.be.fulfilled;
			await increaseTime(60 * 60 * 24 * 1);
			const pointsCount = await discountSystem.getPointsCountByUserAddress(userAddress, {from: userAddress});
			assert.equal(pointsCount, 10);
		});

		it("should return points count with stake and withdraw operations", async() => {
			await discountSystem.createDiscountStage(10, 50000, {from: adminAddress}).should.be.fulfilled;
			await discountToken.mint(userAddress, 20000, {from: ownerAddress}).should.be.fulfilled;
			await discountToken.approve(discountSystem.address, 20000, {from: userAddress}).should.be.fulfilled;
			await discountSystem.stake(20000, {from: userAddress}).should.be.fulfilled;
			await increaseTime(60 * 60 * 24 * 1);
			await discountSystem.withdraw(10000, {from: userAddress}).should.be.fulfilled;
			await increaseTime(60 * 60 * 24 * 1);
			const pointsCount = await discountSystem.getPointsCountByUserAddress(userAddress, {from: userAddress});
			assert.equal(pointsCount, 30);
		});

		it("should return points count when points to subtract amount is greater than curent points amount", async() => {
			await discountSystem.createDiscountStage(10, 50000, {from: adminAddress}).should.be.fulfilled;
			await discountToken.mint(userAddress, 10000, {from: ownerAddress}).should.be.fulfilled;
			await discountToken.approve(discountSystem.address, 10000, {from: userAddress}).should.be.fulfilled;
			await discountSystem.stake(10000, {from: userAddress}).should.be.fulfilled;
			await increaseTime(60 * 60 * 24 * 3);
			await discountSystem.useForFeeDiscount(userAddress, 10000, {from: ownerAddress}).should.be.fulfilled;
			await increaseTime(60 * 60 * 24 * 3);
			const pointsCount = await discountSystem.getPointsCountByUserAddress(userAddress, {from: userAddress});
			assert.equal(pointsCount, 20);
		});

		it("should return 0 if min discount stage is not reached and too much time passed", async() => {
			await discountSystem.createDiscountStage(350, 50000, {from: adminAddress}).should.be.fulfilled;
			await discountToken.mint(userAddress, 10000, {from: ownerAddress}).should.be.fulfilled;
			await discountToken.approve(discountSystem.address, 10000, {from: userAddress}).should.be.fulfilled;
			await discountSystem.stake(10000, {from: userAddress}).should.be.fulfilled;
			
			// 30 days passed
			await increaseTime(60 * 60 * 24 * 30);
			let pointsCount = await discountSystem.getPointsCountByUserAddress(userAddress, {from: userAddress});
			assert.equal(pointsCount, 300);

			// 1 day passed to reset points
			await increaseTime(60 * 60 * 24 * 1);
			pointsCount = await discountSystem.getPointsCountByUserAddress(userAddress, {from: userAddress});
			assert.equal(pointsCount, 0);
		});
	});

	describe("getTokenOperation()", () => {
		it("should revert if user address is 0x00", async() => {
			await discountSystem.getTokenOperation(0x00, 0, {from: userAddress}).should.be.rejectedWith("revert");
		});

		it("should revert if token operation does not exist", async() => {
			await discountSystem.getTokenOperation(userAddress, 0, {from: userAddress}).should.be.rejectedWith("revert");
		});

		it("should return token operation details", async() => {
			await discountToken.approve(discountSystem.address, 1, {from: userAddress}).should.be.fulfilled;
			await discountSystem.stake(1, {from: userAddress}).should.be.fulfilled;
			const tokenOperation = await discountSystem.getTokenOperation(userAddress, 0, {from: userAddress});
			assert.equal(tokenOperation[0], TOKEN_OPERATION_TYPE_STAKE);
			assert.notEqual(tokenOperation[1], 0);
			assert.equal(tokenOperation[2], 1);
		});
	});

	describe("stake()", () => {
		it("should revert if amount is 0", async() => {
			await discountSystem.stake(0, {from: userAddress}).should.be.rejectedWith("revert");
		});

		it("should revert if user does not have enough LEND tokens", async() => {
			await discountSystem.stake(2, {from: userAddress}).should.be.rejectedWith("revert");
		});

		it("should stake tokens", async() => {
			await discountToken.approve(discountSystem.address, 1, {from: userAddress}).should.be.fulfilled;

			const userBalanceBefore = await discountToken.balanceOf(userAddress);
			const systemBalanceBefore = await discountToken.balanceOf(discountSystem.address);
			assert.equal(userBalanceBefore, 1);
			assert.equal(systemBalanceBefore, 0);

			await discountSystem.stake(1, {from: userAddress}).should.be.fulfilled;

			const userBalanceAfter = await discountToken.balanceOf(userAddress);
			const systemBalanceAfter = await discountToken.balanceOf(discountSystem.address);
			assert.equal(userBalanceAfter, 0);
			assert.equal(systemBalanceAfter, 1);
		});
	});

	describe("updateDiscountStage()", () => {
		it("should revert if discount stage does not exist", async() => {
			await discountSystem.createDiscountStage(10, 50000, {from: adminAddress}).should.be.fulfilled;
			await discountSystem.updateDiscountStage(1, 11, 50001, {from: adminAddress}).should.be.rejectedWith("revert");
		});

		it("should update discount stage", async() => {
			await discountSystem.createDiscountStage(10, 50000, {from: adminAddress}).should.be.fulfilled;
			await discountSystem.updateDiscountStage(0, 11, 50001, {from: adminAddress}).should.be.fulfilled;
			const discountStage = await discountSystem.discountStages(0);
			assert.equal(discountStage[0], 11);
			assert.equal(discountStage[1], 50001);
		});
	});

	describe("useForFeeDiscount()", () => {
		it("should revert if user address is 0x00", async() => {
			await discountSystem.useForFeeDiscount(0x00, 0).should.be.rejectedWith("revert"); 
		});

		it("should revert if user does not have enough tokens", async() => {
			await discountSystem.useForFeeDiscount(userAddress, 1).should.be.rejectedWith("revert"); 
		});

		it("should use tokens for fee discount", async() => {
			await discountToken.approve(discountSystem.address, 1, {from: userAddress}).should.be.fulfilled;
			await discountSystem.stake(1, {from: userAddress}).should.be.fulfilled;

			await discountSystem.useForFeeDiscount(userAddress, 1, {from: ownerAddress}).should.be.fulfilled;

			const tokenOperation = await discountSystem.getTokenOperation(userAddress, 1, {from: userAddress});
			assert.equal(tokenOperation[0], TOKEN_OPERATION_TYPE_USE_FOR_FEE_DISCOUNT);
			assert.notEqual(tokenOperation[1], 0);
			assert.equal(tokenOperation[2], 1);
		});
	});

	describe("withdraw()", () => {
		it("should revert if amount is 0", async() => {
			await discountSystem.withdraw(0, {from: userAddress}).should.be.rejectedWith("revert");
		});

		it("should revert if user does not have enough LEND tokens on discount balance", async() => {
			await discountSystem.withdraw(1, {from: userAddress}).should.be.rejectedWith("revert");
		});

		it("should withdraw tokens", async() => {
			await discountToken.approve(discountSystem.address, 1, {from: userAddress}).should.be.fulfilled;
			await discountSystem.stake(1, {from: userAddress}).should.be.fulfilled;

			const userBalanceBefore = await discountToken.balanceOf(userAddress);
			assert.equal(userBalanceBefore, 0);

			await discountSystem.withdraw(1, {from: userAddress}).should.be.fulfilled;

			const userBalanceAfter = await discountToken.balanceOf(userAddress);
			assert.equal(userBalanceAfter, 1);
		});
	});

});
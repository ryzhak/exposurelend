require('chai').use(require('chai-as-promised')).should();
const { increaseTime } = require('./utils/helpers.js');

const ExposureLedger = artifacts.require("ExposureLedgerTestable");
const DiscountSystem = artifacts.require("DiscountSystemTestable");
const MintableToken = artifacts.require("MintableToken");

contract("DiscountSystem: Functional", (accounts) => {

    const ownerAddress = accounts[0];
    const adminAddress = accounts[1];
    const otherAddress = accounts[2];
	const userAddress = accounts[3];
	const droneAddress = accounts[4];
	const platformFeeAddress = accounts[5];
    
    let discountToken;
	let discountSystem;
	let exposureLedger;
    
    beforeEach(async() => {
		discountToken = await MintableToken.new();
		exposureLedger = await ExposureLedger.new(adminAddress, droneAddress, platformFeeAddress, discountToken.address, 50000, 5, 10000, 30, web3.toWei("1", "ether"), 4, 5000);
		discountSystem = DiscountSystem.at(await exposureLedger.discountSystem());
		await discountSystem.createDiscountStage(10, 100000, {from: adminAddress}).should.be.fulfilled;
		await exposureLedger.send(web3.toWei("0.1", "ether"), {from: adminAddress}).should.be.fulfilled;
    });
    
    describe("onlyAdmin()", () => {
		it("should revert if method is called not from admin", async() => {
			await discountSystem.changeDaysOfIdleToResetPoints(1, {from: otherAddress}).should.be.rejectedWith("revert");
		});

		it("should call method that can be called only by admin", async() => {
			await discountSystem.changeDaysOfIdleToResetPoints(1, {from: adminAddress}).should.be.fulfilled;
		});
    });
    
    describe("getPointsCountByUserAddress()", () => {
		it("should return points count on scenario: stake 10000", async() => {
			await discountToken.mint(userAddress, 10000, {from: ownerAddress}).should.be.fulfilled;
			await discountToken.approve(discountSystem.address, 10000, {from: userAddress}).should.be.fulfilled;
			await discountSystem.stake(10000, {from: userAddress}).should.be.fulfilled;
			const pointsCount = await discountSystem.getPointsCountByUserAddress(userAddress, {from: userAddress});
			assert.equal(pointsCount, 0);
		});

		it("should return points count on scenario: stake 10000 => wait 3 days", async() => {
			await discountToken.mint(userAddress, 10000, {from: ownerAddress}).should.be.fulfilled;
			await discountToken.approve(discountSystem.address, 10000, {from: userAddress}).should.be.fulfilled;
			await discountSystem.stake(10000, {from: userAddress}).should.be.fulfilled;
			await increaseTime(60 * 60 * 24 * 3);
			const pointsCount = await discountSystem.getPointsCountByUserAddress(userAddress, {from: userAddress});
			assert.equal(pointsCount, 30);
        });
        
        it("should return points count on scenario: stake 10000 => wait 3 days => stake 10000", async() => {
			await discountToken.mint(userAddress, 20000, {from: ownerAddress}).should.be.fulfilled;
			await discountToken.approve(discountSystem.address, 20000, {from: userAddress}).should.be.fulfilled;
			await discountSystem.stake(10000, {from: userAddress}).should.be.fulfilled;
            await increaseTime(60 * 60 * 24 * 3);
            await discountSystem.stake(10000, {from: userAddress}).should.be.fulfilled;
			const pointsCount = await discountSystem.getPointsCountByUserAddress(userAddress, {from: userAddress});
			assert.equal(pointsCount, 30);
        });
        
        it("should return points count on scenario: stake 10000 => wait 3 days => stake 10000 => wait 3 days", async() => {
			await discountToken.mint(userAddress, 20000, {from: ownerAddress}).should.be.fulfilled;
			await discountToken.approve(discountSystem.address, 20000, {from: userAddress}).should.be.fulfilled;
			await discountSystem.stake(10000, {from: userAddress}).should.be.fulfilled;
            await increaseTime(60 * 60 * 24 * 3);
            await discountSystem.stake(10000, {from: userAddress}).should.be.fulfilled;
            await increaseTime(60 * 60 * 24 * 3);
			const pointsCount = await discountSystem.getPointsCountByUserAddress(userAddress, {from: userAddress});
			assert.equal(pointsCount, 90);
        });
        
        it("should return points count on scenario: stake 20000 => wait 3 days => withdraw 10000 => wait 3 days => stake 10000 => wait 3 days", async() => {
			await discountToken.mint(userAddress, 30000, {from: ownerAddress}).should.be.fulfilled;
			await discountToken.approve(discountSystem.address, 30000, {from: userAddress}).should.be.fulfilled;
			await discountSystem.stake(20000, {from: userAddress}).should.be.fulfilled;
            await increaseTime(60 * 60 * 24 * 3);
            await discountSystem.withdraw(10000, {from: userAddress}).should.be.fulfilled;
            await increaseTime(60 * 60 * 24 * 3);
            await discountSystem.stake(10000, {from: userAddress}).should.be.fulfilled;
            await increaseTime(60 * 60 * 24 * 3);
			const pointsCount = await discountSystem.getPointsCountByUserAddress(userAddress, {from: userAddress});
			assert.equal(pointsCount, 150);
		});

		it("should return points count on scenario: stake 10000 => wait 3 days => create and close exposure => wait 3 days", async() => {
			// user stake 10000 tokens
			await discountToken.mint(userAddress, 10000, {from: ownerAddress}).should.be.fulfilled;
			await discountToken.approve(discountSystem.address, 10000, {from: userAddress}).should.be.fulfilled;
			await discountSystem.stake(10000, {from: userAddress}).should.be.fulfilled;
			// wait 3 days
			await increaseTime(60 * 60 * 24 * 3);
			// create and close exposure
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddress, 0, 0, {from: userAddress, value: web3.toWei("0.01", "ether")}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddress}).should.be.fulfilled;
			await exposureLedger.close(0, {from: userAddress}).should.be.fulfilled;
			await exposureLedger.autoClose(0, 1, {from: droneAddress}).should.be.fulfilled;
			// wait 3 days
			await increaseTime(60 * 60 * 24 * 3);

			const pointsCount = await discountSystem.getPointsCountByUserAddress(userAddress, {from: userAddress});
			assert.equal(pointsCount, 20);
		});
	});

});
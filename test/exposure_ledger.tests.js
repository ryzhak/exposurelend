require('chai').use(require('chai-as-promised')).should();
const BigNumber = require('bignumber.js');
const { increaseTime } = require('./utils/helpers.js');

const DiscountSystem = artifacts.require("DiscountSystem");
const ExposureLedger = artifacts.require("ExposureLedgerTestable");
const MintableToken = artifacts.require("MintableToken");

contract("ExposureLedger", (accounts) => {

	const adminAddr = accounts[0];
	const userAddr = accounts[1];
	const droneAddr = accounts[2];
	const platformFeeAddr = accounts[3];
	const returnUserAddr = accounts[4];
	const otherAddr = accounts[9];

	const STATE_FUNDED_BY_USER = 0;
	const STATE_BAD_ETH_RATE = 1;
	const STATE_FUNDED_BY_PLATFORM = 2;
	const STATE_CLOSED_BY_USER = 3;
	const STATE_MARGIN_CALL_TIME_ELAPSED = 4;
	const STATE_MARGIN_CALL_PRICE_MOVEMENT = 5;
	const STATE_MARGIN_CALL_MAX_GAIN_REACHED = 6;
	const STATE_CLOSED_BY_ADMIN = 7;

	const ASSET_ETH = 0;
	const ASSET_BTC = 1;

	const LEVERAGE_1TO1 = 0;
	const LEVERAGE_2TO1 = 1;
	const LEVERAGE_4TO1 = 2;

	const initialFund = +web3.toWei("0.1", "ether");

	let exposureLedger;
	let discountToken;
	let discountSystem;

	beforeEach(async() => {
		discountToken = await MintableToken.new();
		exposureLedger = await ExposureLedger.new(adminAddr, droneAddr, platformFeeAddr, discountToken.address, 50000, 5, 10000, 30, web3.toWei("1", "ether"), 4, 5000);
		const discountSystemAddr = await exposureLedger.discountSystem();
		discountSystem = DiscountSystem.at(discountSystemAddr);
		await discountSystem.createDiscountStage(10, 100000, {from: adminAddr}).should.be.fulfilled;
		await exposureLedger.send(initialFund, {from: adminAddr}).should.be.fulfilled;
	});

	describe("autoClose()", () => {
		it("should revert if exposure is not going to be closed", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled; 
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;
			await exposureLedger.autoClose(0, 1, {from: droneAddr}).should.be.rejectedWith("revert");
		});

		it("should revert if real exchange rate is 0", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;
			await exposureLedger.close(0, {from: userAddr}).should.be.fulfilled;
			await exposureLedger.autoClose(0, 0, {from: droneAddr}).should.be.rejectedWith("revert");
		});

		it("should set state to closed by user", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;
			await exposureLedger.close(0, {from: userAddr}).should.be.fulfilled;
			await exposureLedger.autoClose(0, 1, {from: droneAddr}).should.be.fulfilled;

			const exposureSystemData = await exposureLedger.getExposureSystemData(0);
			assert.equal(exposureSystemData[1], STATE_CLOSED_BY_USER);
		});
	});

	describe("autoCloseMany()", () => {
		it("should revert if indexes array is empty", async() => {
			await exposureLedger.autoCloseMany([], 1, {from: droneAddr}).should.be.rejectedWith("revert");
		});

		it("should revert if real exchange rate is 0", async() => {
			await exposureLedger.autoCloseMany([0], 0, {from: droneAddr}).should.be.rejectedWith("revert");
		});

		it("should autoclose many exposures", async() => {
			// user creates and closes exposure #1
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: web3.toWei("0.01", "ether")}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;
			await exposureLedger.close(0, {from: userAddr}).should.be.fulfilled;
			// user creates and closes exposure #2
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: web3.toWei("0.01", "ether")}).should.be.fulfilled;
			await exposureLedger.autoFund(1, {from: droneAddr}).should.be.fulfilled;
			await exposureLedger.close(1, {from: userAddr}).should.be.fulfilled;

			await exposureLedger.autoCloseMany([0,1], 1, {from: droneAddr}).should.be.fulfilled;

			const exposureSystemData1 = await exposureLedger.getExposureSystemData(0);
			const exposureSystemData2 = await exposureLedger.getExposureSystemData(1);
			assert.equal(exposureSystemData1[1], STATE_CLOSED_BY_USER);
			assert.equal(exposureSystemData2[1], STATE_CLOSED_BY_USER);
		});
	});

	describe("autoFund()", () => {
		it("should set current state to funded by platform", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			const exposureSystemData = await exposureLedger.getExposureSystemData(0);
			assert.equal(exposureSystemData[1], STATE_FUNDED_BY_PLATFORM);
		});

		it("should create apply for fee token operation", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			const TOKEN_OPERATION_TYPE_APPLY_FOR_FEE_DISCOUNT = 3;
			const tokenOperation = await discountSystem.getTokenOperation(userAddr, 0, {from: userAddr});
			assert.equal(tokenOperation[0], TOKEN_OPERATION_TYPE_APPLY_FOR_FEE_DISCOUNT);
			assert.notEqual(tokenOperation[1], 0);
			assert.equal(tokenOperation[2], 0);
		});
	});

	describe("autoFundMany()", () => {
		it("should revert if indexes array is empty", async() => {
			await exposureLedger.autoFundMany([], {from: droneAddr}).should.be.rejectedWith("revert");
		});

		it("should autofund many exposures", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: web3.toWei("0.01", "ether")}).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: web3.toWei("0.01", "ether")}).should.be.fulfilled;
			
			await exposureLedger.autoFundMany([0,1], {from: droneAddr}).should.be.fulfilled;

			const exposureSystemData1 = await exposureLedger.getExposureSystemData(0);
			const exposureSystemData2 = await exposureLedger.getExposureSystemData(1);
			assert.equal(exposureSystemData1[1], STATE_FUNDED_BY_PLATFORM);
			assert.equal(exposureSystemData2[1], STATE_FUNDED_BY_PLATFORM);
		});
	});

	describe("badEthRate()", () => {
		it("should revert if real exchange rate is 0", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr,value: initialFund}).should.be.fulfilled;
			await exposureLedger.badEthRate(0, 0, {from: droneAddr}).should.be.rejectedWith("revert");
		});

		it("should decrease locked balance", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr,value: initialFund}).should.be.fulfilled;
			
			const lockedBalanceBefore = await exposureLedger.lockedBalance();
			assert.equal(lockedBalanceBefore.toNumber(), web3.toWei("0.2", "ether"));
			
			await exposureLedger.badEthRate(0, 1, {from: droneAddr}).should.be.fulfilled;

			const lockedBalanceAfter = await exposureLedger.lockedBalance();
			assert.equal(lockedBalanceAfter.toNumber(), 0);
		});

		it("should decrease user's open exposure count", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr,value: initialFund}).should.be.fulfilled;
			
			const openExposureCountBefore = await exposureLedger.getOpenExposuresCountByAddress(userAddr);
			assert.equal(openExposureCountBefore.toNumber(), 1);
			
			await exposureLedger.badEthRate(0, 1, {from: droneAddr}).should.be.fulfilled;

			const openExposureCountAfter = await exposureLedger.getOpenExposuresCountByAddress(userAddr);
			assert.equal(openExposureCountAfter.toNumber(), 0);
		});

		it("should set close exchange rate", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr,value: initialFund}).should.be.fulfilled;
			await exposureLedger.badEthRate(0, 1, {from: droneAddr}).should.be.fulfilled;

			const exposureResultData = await exposureLedger.getExposureResultData(0);
			assert.equal(exposureResultData[0], 1);
		});

		it("should set current state to bad eth rate", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.badEthRate(0, 1, {from: droneAddr}).should.be.fulfilled;

			const exposureSystemData = await exposureLedger.getExposureSystemData(0);
			assert.equal(exposureSystemData[1], STATE_BAD_ETH_RATE);
		});

		it("should set closed at", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.badEthRate(0, 1, {from: droneAddr}).should.be.fulfilled;

			const exposureSystemData = await exposureLedger.getExposureSystemData(0);
			assert.notEqual(exposureSystemData[2], 0);
		});

		it("should refund user on ivalid exposure", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;

			const ledgerBalanceBefore = web3.eth.getBalance(exposureLedger.address).toNumber();
			const userBalanceBefore = web3.eth.getBalance(userAddr).toNumber();

			await exposureLedger.badEthRate(0, 1, {from: droneAddr}).should.be.fulfilled;

			const ledgerBalanceAfter = web3.eth.getBalance(exposureLedger.address).toNumber();
			const userBalanceAfter = web3.eth.getBalance(userAddr).toNumber();

			assert.equal(userBalanceAfter - userBalanceBefore, initialFund);
			assert.equal(ledgerBalanceBefore - ledgerBalanceAfter, initialFund);
		});
	});

	describe("badEthRateMany()", () => {
		it("should revert if indexes array is empty", async() => {
			await exposureLedger.badEthRateMany([], 1, {from: droneAddr}).should.be.rejectedWith("revert");
		});

		it("should revert if real exchange rate is 0", async() => {
			await exposureLedger.badEthRateMany([0], 0, {from: droneAddr}).should.be.rejectedWith("revert");
		});

		it("should bad eth rate many exposures", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: web3.toWei("0.01", "ether")}).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: web3.toWei("0.01", "ether")}).should.be.fulfilled;
			
			await exposureLedger.badEthRateMany([0,1], 1, {from: droneAddr}).should.be.fulfilled;

			const exposureSystemData1 = await exposureLedger.getExposureSystemData(0);
			const exposureSystemData2 = await exposureLedger.getExposureSystemData(1);
			assert.equal(exposureSystemData1[1], STATE_BAD_ETH_RATE);
			assert.equal(exposureSystemData2[1], STATE_BAD_ETH_RATE);
		});
	});

	describe("changeAdminAddress()", () => {
		it("should revert if new admin address is 0x00", async() => {
			await exposureLedger.changeAdminAddress(0x00).should.be.rejectedWith("revert"); 
		});

		it("should change admin address", async() => {
			await exposureLedger.changeAdminAddress(otherAddr).should.be.fulfilled;
			assert.equal(await exposureLedger.adminAddress(), otherAddr); 
			assert.equal(await discountSystem.adminAddress(), otherAddr);
		});
	});

	describe("changeClosingFee()", () => {
		it("should revert if new closing fee is 0", async() => {
			await exposureLedger.changeClosingFee(0).should.be.rejectedWith("revert"); 
		});

		it("should change closing fee", async() => {
			await exposureLedger.changeClosingFee(1).should.be.fulfilled;
			assert.equal(await exposureLedger.closingFee(), 1); 
		});
	});

	describe("changeDefaultRate()", () => {
		it("should revert if new default rate is 0", async() => {
			await exposureLedger.changeDefaultRate(0).should.be.rejectedWith("revert"); 
		});

		it("should change default rate", async() => {
			await exposureLedger.changeDefaultRate(1).should.be.fulfilled;
			assert.equal(await exposureLedger.defaultRate(), 1); 
		});
	});

	describe("changeDroneAddress()", () => {
		it("should revert if new drone address is 0x00", async() => {
			await exposureLedger.changeDroneAddress(0x00).should.be.rejectedWith("revert"); 
		});

		it("should change drone address", async() => {
			await exposureLedger.changeDroneAddress(otherAddr).should.be.fulfilled;
			assert.equal(await exposureLedger.droneAddress(), otherAddr); 
		});
	});

	describe("changeExchangeDelta()", () => {
		it("should change exchange delta", async() => {
			await exposureLedger.changeExchangeDelta(1).should.be.fulfilled;
			assert.equal(await exposureLedger.exchangeDeltaPercent(), 1); 
		});
	});

	describe("changeMaxExposureDurationInDays()", () => {
		it("should revert if new duration is 0", async() => {
			await exposureLedger.changeMaxExposureDurationInDays(0).should.be.rejectedWith("revert"); 
		});

		it("should change max exposure duration in days", async() => {
			await exposureLedger.changeMaxExposureDurationInDays(1).should.be.fulfilled;
			assert.equal(await exposureLedger.maxExposureDurationInDays(), 1); 
		});
	});

	describe("changeMaxFunding()", () => {
		it("should revert if new max funding is 0", async() => {
			await exposureLedger.changeMaxFunding(0).should.be.rejectedWith("revert"); 
		});

		it("should change max funding sum", async() => {
			await exposureLedger.changeMaxFunding(1).should.be.fulfilled;
			assert.equal(await exposureLedger.maxFunding(), 1); 
		});
	});

	describe("changeMaxOpenExposuresPerAddress()", () => {
		it("should change max open exposures per address", async() => {
			await exposureLedger.changeMaxOpenExposuresPerAddress(1).should.be.fulfilled;
			assert.equal(await exposureLedger.maxOpenExposuresPerAddress(), 1); 
		});
	});

	describe("changeMaxUserLoss()", () => {
		it("should change max user loss", async() => {
			await exposureLedger.changeMaxUserLoss(1).should.be.fulfilled;
			assert.equal(await exposureLedger.maxUserLoss(), 1); 
		});
	});

	describe("changePlatformFeeAddress()", () => {
		it("should revert if new platform fee address is 0x00", async() => {
			await exposureLedger.changePlatformFeeAddress(0x00).should.be.rejectedWith("revert"); 
		});

		it("should change platform fee address", async() => {
			await exposureLedger.changePlatformFeeAddress(otherAddr).should.be.fulfilled;
			assert.equal(await exposureLedger.platformFeeAddress(), otherAddr); 
		});
	});

	describe("close()", () => {
		it("should revert if exposure is marked for closing", async() => {
			// user creates exposure
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled; 
			// set state to funded by platform
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;
			// mark exposure to be closed
			await exposureLedger.close(0, {from: userAddr}).should.be.fulfilled;
			// try to mark 2nd time
			await exposureLedger.close(0, {from: userAddr}).should.be.rejectedWith("revert");
		});

		it("should mark exposure to be closed", async() => {
			// user creates exposure
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled; 
			// set state to funded by platform
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;
			// mark exposure to be closed
			await exposureLedger.close(0, {from: userAddr}).should.be.fulfilled;
			
			const exposureUserData = await exposureLedger.getExposureUserData(0);
			assert.equal(exposureUserData[5], true);
		});
	});

	describe("constructor()", () => {
		it("should revert if admin address is 0x00", async() => {
			const invalidAdminAddr = 0x00;
			const exposureLedgerNew = await ExposureLedger.new(invalidAdminAddr, droneAddr, platformFeeAddr, discountToken.address, 1, 0, 10000, 30, 1, 1, 1).should.be.rejectedWith("revert"); 
		});

		it("should revert if drone address is 0x00", async() => {
			const invalidDroneAddr = 0x00;
			const exposureLedgerNew = await ExposureLedger.new(adminAddr, invalidDroneAddr, platformFeeAddr, discountToken.address, 1, 0, 10000, 30, 1, 1, 1).should.be.rejectedWith("revert"); 
		});

		it("should revert if platform fee address is 0x00", async() => {
			const invalidPlatformFeeAddr = 0x00;
			const exposureLedgerNew = await ExposureLedger.new(adminAddr, droneAddr, invalidPlatformFeeAddr, discountToken.address, 1, 0, 10000, 30, 1, 1, 1).should.be.rejectedWith("revert"); 
		});

		it("should revert if discount token address is 0x00", async() => {
			const invalidDiscountTokenAddr = 0x00;
			const exposureLedgerNew = await ExposureLedger.new(adminAddr, droneAddr, platformFeeAddr, invalidDiscountTokenAddr, 1, 0, 10000, 30, 1, 1, 1).should.be.rejectedWith("revert"); 
		});

		it("should revert if closing fee is 0", async() => {
			const invalidClosingFee = 0;
			const exposureLedgerNew = await ExposureLedger.new(adminAddr, droneAddr, platformFeeAddr, discountToken.address, invalidClosingFee, 0, 10000, 30, 1, 1, 1).should.be.rejectedWith("revert"); 
		});

		it("should revert if max exposure duration in days is 0", async() => {
			const invalidMaxExposureDurationInDays = 0;
			const exposureLedgerNew = await ExposureLedger.new(adminAddr, droneAddr, platformFeeAddr, discountToken.address, 1, 0, 10000, invalidMaxExposureDurationInDays, 1, 1, 1).should.be.rejectedWith("revert"); 
		});

		it("should revert if max funding is 0", async() => {
			const invalidMaxFunding = 0;
			const exposureLedgerNew = await ExposureLedger.new(adminAddr, droneAddr, platformFeeAddr, discountToken.address, 1, 0, 10000, 30, invalidMaxFunding, 1, 1).should.be.rejectedWith("revert"); 
		});

		it("should set ledger params", async() => {
			const closingFee = 1;
			const exchangeDelta = 2;
			const defaultRate = 3;
			const maxExposureDurationInDays = 4;
			const maxFunding = 5;
			const maxOpenExposuresPerAddress = 6;
			const maxUserLoss = 7;

			const exposureLedgerNew = await ExposureLedger.new(adminAddr, droneAddr, platformFeeAddr, discountToken.address, closingFee, exchangeDelta, defaultRate, maxExposureDurationInDays, maxFunding, maxOpenExposuresPerAddress, maxUserLoss).should.be.fulfilled; 
			const discountSystemNew = DiscountSystem.at(await exposureLedger.discountSystem());

			assert.equal(await exposureLedgerNew.adminAddress(), adminAddr);
			assert.equal(await exposureLedgerNew.droneAddress(), droneAddr);
			assert.equal(await exposureLedgerNew.platformFeeAddress(), platformFeeAddr);
			assert.equal(await discountSystemNew.discountToken(), discountToken.address);
			assert.equal(await exposureLedgerNew.closingFee(), closingFee);
			assert.equal(await exposureLedgerNew.exchangeDeltaPercent(), exchangeDelta);
			assert.equal(await exposureLedgerNew.defaultRate(), defaultRate);
			assert.equal(await exposureLedgerNew.maxExposureDurationInDays(), maxExposureDurationInDays);
			assert.equal(await exposureLedgerNew.maxFunding(), maxFunding);
			assert.equal(await exposureLedgerNew.maxOpenExposuresPerAddress(), maxOpenExposuresPerAddress);
			assert.equal(await exposureLedgerNew.maxUserLoss(), maxUserLoss);
		});
	});

	describe("createAndFundNewExposure()", () => {
		it("should revert if exchange rate is 0", async() => {
			const invalidExchangeRate = 0;
			await exposureLedger.createAndFundNewExposure(true, 0, invalidExchangeRate, 1, otherAddr, LEVERAGE_1TO1, ASSET_ETH).should.be.rejectedWith("revert"); 
		});

		it("should revert if duration in days is 0", async() => {
			const invalidDurationInDays = 0;
			await exposureLedger.createAndFundNewExposure(true, 0, 1, invalidDurationInDays, otherAddr, LEVERAGE_1TO1, ASSET_ETH).should.be.rejectedWith("revert"); 
		});

		it("should revert if duration in days is more than max", async() => {
			const maxExposureDurationInDays = await exposureLedger.maxExposureDurationInDays();
			await exposureLedger.createAndFundNewExposure(true, 0, 1, maxExposureDurationInDays + 1, otherAddr, LEVERAGE_1TO1, ASSET_ETH).should.be.rejectedWith("revert"); 
		});

		it("should revert if return user address is 0x00", async() => {
			const invalidReturnUserAddr = 0x00;
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, invalidReturnUserAddr, LEVERAGE_1TO1, ASSET_ETH).should.be.rejectedWith("revert"); 
		});

		it("should revert if msg value is 0", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH).should.be.rejectedWith("revert"); 
		});

		it("should revert if msg value is more than max funding sum", async() => {
			await exposureLedger.changeMaxFunding(web3.toWei("0.01")).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: web3.toWei("0.02")}).should.be.rejectedWith("revert"); 
		});

		it("should revert if msg value is more than unlocked balance", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: web3.toWei("0.11", "ether")}).should.be.rejectedWith("revert"); 
		});

		it("should revert if user exceeded the limit on max open exposures count", async() => {
			// user creates 4 exposures
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: web3.toWei("0.01", "ether")}).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: web3.toWei("0.01", "ether")}).should.be.fulfilled; 
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: web3.toWei("0.01", "ether")}).should.be.fulfilled; 
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: web3.toWei("0.01", "ether")}).should.be.fulfilled; 
			// reverts on exposure #5
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: web3.toWei("0.01", "ether")}).should.be.rejectedWith("revert"); 
		});

		it("should add exposure to ledger", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled; 
			assert.equal(await exposureLedger.exposureCount(), 1);
		});

		it("should set exposure properties", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled; 
			
			const exposureUserData = await exposureLedger.getExposureUserData(0);
			const exposureSystemData = await exposureLedger.getExposureSystemData(0);
			const exposureResultData = await exposureLedger.getExposureResultData(0);

			assert.equal(exposureUserData[0], userAddr);
			assert.equal(exposureUserData[1], otherAddr);
			assert.equal(exposureUserData[2].toNumber(), 1);
			assert.equal(exposureUserData[3].toNumber(), 30);
			assert.equal(exposureUserData[4], true);
			assert.equal(exposureUserData[6].toNumber(), 0);
			assert.notEqual(exposureSystemData[0].toNumber(), 0);
			assert.equal(exposureSystemData[1].toNumber(), 0);
			assert.equal(exposureSystemData[2].toNumber(), 0);
			assert.equal(exposureSystemData[3].toNumber(), 0);
			assert.equal(exposureSystemData[4].toNumber(), 0);
			assert.equal(exposureResultData[1].toNumber(), initialFund);
		});

		it("should assign exposure to mappings", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled; 
			
			const exposuresCountForUser = await exposureLedger.getExposuresCountForUser(userAddr);
			const exposureCount = await exposureLedger.exposureCount();
			const exposureId = await exposureLedger.getExposureIdForUserByIndex(userAddr, 0);

			assert.equal(exposuresCountForUser.toNumber(), 1);
			assert.equal(exposureCount.toNumber(), 1);
			assert.equal(exposureId.toNumber(), 0);
		});

		it("should increase locked balance", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const lockedBalance = await exposureLedger.lockedBalance();
			assert.equal(lockedBalance, web3.toWei("0.2", "ether"));
		});

		it("should increase user's open exposure count", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const openExposureCount = await exposureLedger.getOpenExposuresCountByAddress(userAddr);
			assert.equal(openExposureCount, 1);
		});
	});

	describe("fallback()", () => {
		it("should transfer ether to the ledger", async() => {
			await exposureLedger.send(initialFund, {from: userAddr}).should.be.fulfilled;
		});
	});

	describe("finish()", () => {
		it("should revert if real exchange rate is 0", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled; 
			await exposureLedger.finish(0, 0).should.be.rejectedWith("revert");
		});

		it("should set close exchange rate", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled; 
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			await exposureLedger.finish(0, 1);

			const exposureResultData = await exposureLedger.getExposureResultData(0);
			assert.equal(exposureResultData[0], 1);
		});

		it("should set closed at", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled; 
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			await exposureLedger.finish(0, 1);

			const exposureSystemData = await exposureLedger.getExposureSystemData(0);
			assert.notEqual(exposureSystemData[2], 0);
		});

		it("should decrease locked balance", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled; 
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			const lockedBalanceBefore = await exposureLedger.lockedBalance();
			assert.equal(lockedBalanceBefore.toNumber(), web3.toWei("0.2", "ether"));

			await exposureLedger.finish(0, 1);

			const lockedBalanceAfter = await exposureLedger.lockedBalance();
			assert.equal(lockedBalanceAfter.toNumber(), 0);
		});

		it("should decrease user's open exposures count", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled; 
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			const openExposureCountBefore = await exposureLedger.getOpenExposuresCountByAddress(userAddr);
			assert.equal(openExposureCountBefore.toNumber(), 1);

			await exposureLedger.finish(0, 1);

			const openExposureCountAfter = await exposureLedger.getOpenExposuresCountByAddress(userAddr);
			assert.equal(openExposureCountAfter.toNumber(), 0);
		});

		it("should set result sums", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 50000, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled; 
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			await exposureLedger.finish(0, 50000);

			const exposureResultData = await exposureLedger.getExposureResultData(0);
			assert.equal(web3.fromWei(exposureResultData[2], "ether"), "0.005");
			assert.equal(web3.fromWei(exposureResultData[3], "ether"), "0.1");
			assert.equal(web3.fromWei(exposureResultData[4], "ether"), "0.095");
		});

		it("should create use tokens operation", async() => {
			// mint 10000 tokens to user, stake 10000 tokens
			await discountToken.mint(userAddr, 10000).should.be.fulfilled;
			await discountToken.approve(discountSystem.address, 10000, {from: userAddr}).should.be.fulfilled;
			await discountSystem.stake(10000, {from: userAddr}).should.be.fulfilled;
			// wait 1 day to get 10 points
			await increaseTime(60 * 60 * 24 * 1);
			// user creates exposure and drone funds 
			await exposureLedger.createAndFundNewExposure(true, 0, 50000, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled; 
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			await exposureLedger.finish(0, 50000);

			const TOKEN_OPERATION_TYPE_USE_FOR_FEE_DISCOUNT = 2;
			const tokenOperation = await discountSystem.getTokenOperation(userAddr, 2);
			assert.equal(tokenOperation[0], TOKEN_OPERATION_TYPE_USE_FOR_FEE_DISCOUNT);
			assert.equal(tokenOperation[2], 10000);
		});

		it("should transfer fee and user funds", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 50000, 30, returnUserAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled; 
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			const feeBalanceBefore = web3.eth.getBalance(await exposureLedger.platformFeeAddress()).toNumber();
			const userBalanceBefore = web3.eth.getBalance(returnUserAddr).toNumber();

			await exposureLedger.finish(0, 75000);

			const feeBalanceAfter = web3.eth.getBalance(await exposureLedger.platformFeeAddress()).toNumber();
			const userBalanceAfter = web3.eth.getBalance(returnUserAddr).toNumber();
			const ledgerBalanceAfter = web3.eth.getBalance(exposureLedger.address).toNumber();

			assert.equal(new BigNumber(feeBalanceAfter).minus(feeBalanceBefore), web3.toWei("0.005", "ether"));
			assert.equal(new BigNumber(userBalanceAfter).minus(userBalanceBefore), web3.toWei("0.145", "ether"));
			assert.equal(ledgerBalanceAfter, web3.toWei("0.05", "ether"));
		});
	});

	describe("forceClose()", () => {
		it("should revert if exposure does not exist", async() => {
			await exposureLedger.forceClose(0, 1, {from: adminAddr}).should.be.rejectedWith("revert");
		});

		it("should revert if real exchange rate is 0", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.forceClose(0, 0, {from: adminAddr}).should.be.rejectedWith("revert");
		});


		it("should revert if exposure state is not funded by user or funded by platform", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.badEthRate(0, 1, {from: droneAddr}).should.be.fulfilled;
			await exposureLedger.forceClose(0, 1, {from: adminAddr}).should.be.rejectedWith("revert");
		});

		it("should set state to closed by admin", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;
			await exposureLedger.forceClose(0, 1, {from: adminAddr}).should.be.fulfilled;

			const exposureSystemData = await exposureLedger.getExposureSystemData(0);
			assert.equal(exposureSystemData[1], STATE_CLOSED_BY_ADMIN);
		});
	});

	describe("forceCloseMany()", () => {
		it("should revert if indexes array is empty", async() => {
			await exposureLedger.forceCloseMany([], 1, {from: adminAddr}).should.be.rejectedWith("revert");
		});

		it("should revert if real exchange rate is 0", async() => {
			await exposureLedger.forceCloseMany([0], 0, {from: adminAddr}).should.be.rejectedWith("revert");
		});

		it("should force close many exposures", async() => {
			// user creates exposure #1
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: web3.toWei("0.01", "ether")}).should.be.fulfilled;
			// user creates exposure #2
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: web3.toWei("0.01", "ether")}).should.be.fulfilled;
			await exposureLedger.autoFund(1, {from: droneAddr}).should.be.fulfilled;

			await exposureLedger.forceCloseMany([0,1], 1, {from: adminAddr}).should.be.fulfilled;

			const exposureSystemData1 = await exposureLedger.getExposureSystemData(0);
			const exposureSystemData2 = await exposureLedger.getExposureSystemData(1);
			assert.equal(exposureSystemData1[1], STATE_CLOSED_BY_ADMIN);
			assert.equal(exposureSystemData2[1], STATE_CLOSED_BY_ADMIN);
		});
	});

	describe("getExposureResultData()", () => {
		it("should return exposure result data by index", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;

			const exposureResultData = await exposureLedger.getExposureResultData(0);

			assert.equal(exposureResultData[0].toNumber(), 0);
			assert.equal(exposureResultData[1].toNumber(), initialFund);
			assert.equal(exposureResultData[2].toNumber(), 0);
			assert.equal(exposureResultData[3].toNumber(), 0);
			assert.equal(exposureResultData[4].toNumber(), 0);
		});
	});

	describe("getExposureSystemData()", () => {
		it("should return exposure system data by index", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_2TO1, ASSET_BTC, {from: userAddr, value: initialFund}).should.be.fulfilled;

			const exposureSystemData = await exposureLedger.getExposureSystemData(0);

			assert.notEqual(exposureSystemData[0].toNumber(), 0);
			assert.equal(exposureSystemData[1].toNumber(), 0);
			assert.equal(exposureSystemData[2].toNumber(), 0);
			assert.equal(exposureSystemData[3].toNumber(), LEVERAGE_2TO1);
			assert.equal(exposureSystemData[4].toNumber(), ASSET_BTC);
		});
	});

	describe("getExposureUserData()", () => {
		it("should revert if exposure index does not exist", async() => {
			const exposureUserData = await exposureLedger.getExposureUserData(0).should.be.rejectedWith("revert");
		});

		it("should return exposure user data by index", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;

			const exposureUserData = await exposureLedger.getExposureUserData(0);

			assert.equal(exposureUserData[0], userAddr);
			assert.equal(exposureUserData[1], otherAddr);
			assert.equal(exposureUserData[2].toNumber(), 1);
			assert.equal(exposureUserData[3].toNumber(), 30);
			assert.equal(exposureUserData[4], true);
			assert.equal(exposureUserData[5], false);
			assert.equal(exposureUserData[6].toNumber(), 0);
		});
	});

	describe("getExposuresCountForUser()", () => {
		it("should return exposures count for user", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;

			const exposureCount = await exposureLedger.getExposuresCountForUser(userAddr);
			assert.equal(exposureCount, 1); 
		});
	});

	describe("getExposureIdForUserByIndex()", () => {
		it("should revert if index is less than user's exposure count", async() => {
			await exposureLedger.getExposureIdForUserByIndex(userAddr, 0).should.be.rejectedWith("revert"); 
		});

		it("should return exposure id for user by index", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;

			const exposureId = await exposureLedger.getExposureIdForUserByIndex(userAddr, 0);
			assert.equal(exposureId, 0); 
		});
	});

	describe("getOpenExposuresCountByAddress()", () => {
		it("should revert user address is 0x00", async() => {
			await exposureLedger.getOpenExposuresCountByAddress(0x00).should.be.rejectedWith("revert"); 
		});

		it("should return open exposures count by user address", async() => {
			// create exposure in state funded by user
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: web3.toWei("0.01","ether")}).should.be.fulfilled;
			// create exposure in state funded by platform
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: web3.toWei("0.01","ether")}).should.be.fulfilled;
			await exposureLedger.autoFund(1, {from: droneAddr}).should.be.fulfilled;
			// create and close exposure
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: web3.toWei("0.01","ether")}).should.be.fulfilled;
			await exposureLedger.autoFund(2, {from: droneAddr}).should.be.fulfilled;
			await exposureLedger.close(2, {from: userAddr}).should.be.fulfilled;
			await exposureLedger.autoClose(2, 1, {from: droneAddr}).should.be.fulfilled;

			const openExposuresCount = await exposureLedger.getOpenExposuresCountByAddress(userAddr);
			assert.equal(openExposuresCount, 2); 
		});
	});

	describe("getPlatformFee()", () => {
		it("should return platform fee on 0, 1, 2 days passed", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			// 0 days passed
			let fee = await exposureLedger.getPlatformFee(0);
			assert.equal(fee.toNumber(), web3.toWei("0.005", "ether"));

			// 1st day passed
			await increaseTime(60 * 60 * 24 * 1);
			fee = await exposureLedger.getPlatformFee(0);
			assert.equal(fee.toNumber(), web3.toWei("0.005", "ether"));

			// 2nd day passed
			await increaseTime(60 * 60 * 24 * 1);
			fee = await exposureLedger.getPlatformFee(0);
			assert.equal(fee.toNumber(), web3.toWei("0.01", "ether"));
		});

		it("should return 0 if discount sum is greater than platform fee", async() => {
			// create 100% discount stage
			await discountSystem.createDiscountStage(15, 1000000, {from: adminAddr}).should.be.fulfilled;
			// mint 10000 tokens to user, stake 10000 tokens
			await discountToken.mint(userAddr, 10000).should.be.fulfilled;
			await discountToken.approve(discountSystem.address, 10000, {from: userAddr}).should.be.fulfilled;
			await discountSystem.stake(10000, {from: userAddr}).should.be.fulfilled;
			// wait 2 days to get 20 points
			await increaseTime(60 * 60 * 24 * 2);
			// user creates exposure and drone funds it
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			let fee = await exposureLedger.getPlatformFee(0);
			assert.equal(fee.toNumber(), 0);
		});

		it("should return platform fee minus discount", async() => {
			// mint 10000 tokens to user, stake 10000 tokens
			await discountToken.mint(userAddr, 10000).should.be.fulfilled;
			await discountToken.approve(discountSystem.address, 10000, {from: userAddr}).should.be.fulfilled;
			await discountSystem.stake(10000, {from: userAddr}).should.be.fulfilled;
			// wait 1 day to get 10 points
			await increaseTime(60 * 60 * 24 * 5);
			// user creates exposure and drone funds it
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			let fee = await exposureLedger.getPlatformFee(0);
			assert.equal(fee.toNumber(), web3.toWei("0.0045", "ether"));
		});

		it("should return platform fee when it is greater than initial user fund", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			// 21 days passed
			await increaseTime(60 * 60 * 24 * 21);
			let fee = await exposureLedger.getPlatformFee(0);
			assert.equal(fee.toNumber(), web3.toWei("0.1", "ether"));
		});
	});

	describe("getResultUserSum()", () => {
		it("should revert if real exchange rate is 0", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			await exposureLedger.getResultUserSum(0, 0).should.be.rejectedWith("revert");
		});

		it("should return result user sum on price up and long position", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 50000, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			const resultUserSum = await exposureLedger.getResultUserSum(0, 75000);
			assert.equal(resultUserSum.toNumber(), web3.toWei("0.145", "ether"));
		});

		it("should return result user sum on price down and long position", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 50000, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			const resultUserSum = await exposureLedger.getResultUserSum(0, 25000);
			assert.equal(resultUserSum.toNumber(), web3.toWei("0.045", "ether"));
		});

		it("should return result user sum on price up and short position", async() => {
			await exposureLedger.createAndFundNewExposure(false, 0, 50000, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			const resultUserSum = await exposureLedger.getResultUserSum(0, 75000);
			assert.equal(resultUserSum.toNumber(), web3.toWei("0.045", "ether"));
		});

		it("should return result user sum on price down and short position", async() => {
			await exposureLedger.createAndFundNewExposure(false, 0, 50000, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			const resultUserSum = await exposureLedger.getResultUserSum(0, 25000);
			assert.equal(resultUserSum.toNumber(), web3.toWei("0.145", "ether"));
		});

		it("should restrict user loss on price down and long position", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 50000, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			const resultUserSum = await exposureLedger.getResultUserSum(0, 1);
			assert.equal(resultUserSum.toNumber(), web3.toWei("0.045", "ether"));
		});

		it("should restrict user loss on price up and short position", async() => {
			await exposureLedger.createAndFundNewExposure(false, 0, 50000, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			const resultUserSum = await exposureLedger.getResultUserSum(0, 100000);
			assert.equal(resultUserSum.toNumber(), web3.toWei("0.045", "ether"));
		});

		it("should return result user sum on price 200% up and long position when user wins everything", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 50000, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			const resultUserSum = await exposureLedger.getResultUserSum(0, 150000);
			assert.equal(resultUserSum.toNumber(), web3.toWei("0.195", "ether"));
		});

		it("should return result user sum on price down and short position when user wins everything", async() => {
			await exposureLedger.createAndFundNewExposure(false, 0, 50000, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			const resultUserSum = await exposureLedger.getResultUserSum(0, 1);
			assert.equal(resultUserSum.toNumber(), web3.toWei("0.195", "ether"));
		});

		it("should return 0 on price down and long position if closing fee equals to initial user fund", async() => {
			// set closing fee to 100%
			await exposureLedger.changeClosingFee(1000000).should.be.fulfilled;
			// exposure is created and funded
			await exposureLedger.createAndFundNewExposure(true, 0, 50000, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			const resultUserSum = await exposureLedger.getResultUserSum(0, 1);
			assert.equal(resultUserSum.toNumber(), 0);
		});

		it("should return 0 on price up and short position if closing fee equals to initial user fund", async() => {
			// set closing fee to 100%
			await exposureLedger.changeClosingFee(1000000).should.be.fulfilled;
			// exposure is created and funded
			await exposureLedger.createAndFundNewExposure(false, 0, 50000, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			const resultUserSum = await exposureLedger.getResultUserSum(0, 100000);
			assert.equal(resultUserSum.toNumber(), 0);
		});
	});

	describe("getUnlockedBalance()", () => {
		it("should return 0 if ledger does not has enough ether for funding", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			const unlockedBalance = await exposureLedger.getUnlockedBalance();
			assert.equal(unlockedBalance.toNumber(), 0);
		});

		it("should return unlocked balance", async() => {
			// create funded by user exposure
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: web3.toWei("0.01", "ether")}).should.be.fulfilled;
			// create funded by platform exposure
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: web3.toWei("0.02", "ether")}).should.be.fulfilled;
			await exposureLedger.autoFund(1, {from: droneAddr}).should.be.fulfilled;

			const unlockedBalance = await exposureLedger.getUnlockedBalance();
			assert.equal(unlockedBalance.toNumber(), web3.toWei("0.07", "ether"));
		});
	});

	describe("marginCall()", () => {
		it("should revert if real exchange rate is 0", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			await exposureLedger.marginCall(0, 0, STATE_MARGIN_CALL_TIME_ELAPSED, {from: droneAddr}).should.be.rejectedWith("revert");
		});

		it("should revert if new state is not margin call", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			await exposureLedger.marginCall(0, 1, STATE_BAD_ETH_RATE, {from: droneAddr}).should.be.rejectedWith("revert");
		});

		it("should set current state to margin call", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;

			await exposureLedger.marginCall(0, 1, STATE_MARGIN_CALL_TIME_ELAPSED, {from: droneAddr}).should.be.fulfilled;

			const exposureSystemData = await exposureLedger.getExposureSystemData(0);
			assert.equal(exposureSystemData[1], STATE_MARGIN_CALL_TIME_ELAPSED);
		});
	});

	describe("marginCallMany()", () => {
		it("should revert if indexes array is empty", async() => {
			await exposureLedger.marginCallMany([], 1, STATE_MARGIN_CALL_PRICE_MOVEMENT, {from: droneAddr}).should.be.rejectedWith("revert");
		});

		it("should revert if real exchange rate is 0", async() => {
			await exposureLedger.marginCallMany([0], 0, STATE_MARGIN_CALL_PRICE_MOVEMENT, {from: droneAddr}).should.be.rejectedWith("revert");
		});

		it("should revert if new state is not margin call", async() => {
			await exposureLedger.marginCallMany([0], 1, STATE_BAD_ETH_RATE, {from: droneAddr}).should.be.rejectedWith("revert");
		});

		it("should margin call many exposures", async() => {
			// user creates exposure #1
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: web3.toWei("0.01", "ether")}).should.be.fulfilled;
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;
			// user creates exposure #2
			await exposureLedger.createAndFundNewExposure(true, 0, 1, 30, otherAddr, LEVERAGE_1TO1, ASSET_ETH, {from: userAddr, value: web3.toWei("0.01", "ether")}).should.be.fulfilled;
			await exposureLedger.autoFund(1, {from: droneAddr}).should.be.fulfilled;

			await exposureLedger.marginCallMany([0,1], 1, STATE_MARGIN_CALL_PRICE_MOVEMENT, {from: droneAddr}).should.be.fulfilled;

			const exposureSystemData1 = await exposureLedger.getExposureSystemData(0);
			const exposureSystemData2 = await exposureLedger.getExposureSystemData(1);
			assert.equal(exposureSystemData1[1], STATE_MARGIN_CALL_PRICE_MOVEMENT);
			assert.equal(exposureSystemData2[1], STATE_MARGIN_CALL_PRICE_MOVEMENT);
		});
	});

	describe("withdrawUnlockedBalance()", () => {
		it("should revert if withdraw amount is 0", async() => {
			await exposureLedger.withdrawUnlockedBalance(0, otherAddr).should.be.rejectedWith("revert");
		});

		it("should revert if withdraw amount is greater than unlocked balance", async() => {
			const unlockedBalance = await exposureLedger.getUnlockedBalance();
			await exposureLedger.withdrawUnlockedBalance(unlockedBalance + 1, otherAddr).should.be.rejectedWith("revert");
		});

		it("should revert if output address is 0x00", async() => {
			await exposureLedger.withdrawUnlockedBalance(1, 0x00).should.be.rejectedWith("revert");
		});

		it("should withdraw unlocked balance", async() => {
			const ledgerBalanceBefore = web3.eth.getBalance(exposureLedger.address).toNumber();
			const outputBalanceBefore = web3.eth.getBalance(otherAddr).toNumber();

			await exposureLedger.withdrawUnlockedBalance(initialFund, otherAddr).should.be.fulfilled;

			const ledgerBalanceAfter = web3.eth.getBalance(exposureLedger.address).toNumber();
			const outputBalanceAfter = web3.eth.getBalance(otherAddr).toNumber();

			assert.equal(ledgerBalanceBefore - ledgerBalanceAfter, initialFund);
			assert.equal(outputBalanceAfter - outputBalanceBefore, initialFund);
		});
	});

});
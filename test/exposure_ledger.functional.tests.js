require('chai').use(require('chai-as-promised')).should();

const DiscountSystem = artifacts.require("DiscountSystem");
const ExposureLedger = artifacts.require("ExposureLedgerTestable");
const MintableToken = artifacts.require("MintableToken");

contract("ExposureLedger", (accounts) => {

	const adminAddr = accounts[0];
	const userAddr = accounts[1];
	const droneAddr = accounts[2];
	const platformFeeAddr = accounts[3];
	const returnUserAddr = accounts[4];

	const initialFund = web3.toWei("0.1", "ether");

	let exposureLedger;
	let discountToken;
	let discountSystem;

	beforeEach(async() => {
		discountToken = await MintableToken.new();
		exposureLedger = await ExposureLedger.new(adminAddr, droneAddr, platformFeeAddr, discountToken.address, 50000, 5, 10000, 30, web3.toWei("1","ether"), 4, 5000);
		discountSystem = DiscountSystem.at(await exposureLedger.discountSystem());
		await discountSystem.createDiscountStage(10, 100000, {from: adminAddr}).should.be.fulfilled;
		await exposureLedger.send(web3.toWei("0.5", "ether"), {from: adminAddr}).should.be.fulfilled;
		await exposureLedger.createAndFundNewExposure(true, 0, 50000, 30, returnUserAddr, 0, 0, {from: userAddr, value: initialFund}).should.be.fulfilled;
	});

	describe("getResultUserSum()", () => {
		it("should return result user sum on long position 300.00 USD per ether with 298.69 USD real price", async() => {
			const realPrice = 29869;
			await exposureLedger.createAndFundNewExposure(true, 0, 30000, 30, returnUserAddr, 0, 0, {from: userAddr, value: initialFund}).should.be.fulfilled;
			await exposureLedger.autoFund(1, {from: droneAddr}).should.be.fulfilled;

			const resultUserSum = await exposureLedger.getResultUserSum(1, realPrice);
			assert.equal(resultUserSum.toNumber(), web3.toWei("0.09456", "ether"));
		});
	});

	describe("onlyAdmin()", () => {
		it("should revert if method is called not from admin", async() => {
			await exposureLedger.withdrawUnlockedBalance(1, returnUserAddr, {from: userAddr}).should.be.rejectedWith("revert");
		});

		it("should call method that can be called only by admin", async() => {
			await exposureLedger.withdrawUnlockedBalance(1, returnUserAddr, {from: adminAddr}).should.be.fulfilled;
		});
	});

	describe("onlyDrone()", () => {
		it("should revert if method is called not from drone", async() => {
			await exposureLedger.autoFund(0, {from: userAddr}).should.be.rejectedWith("revert");
		});

		it("should call method that can be called only by drone", async() => {
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;
		});
	});

	describe("onlyInState()", () => {
		it("should revert if method is called when exposure is not in the correct state", async() => {
			// exposure state should be FundedByPlatform, but it is FundedByUser
			await exposureLedger.close(0, {from: userAddr}).should.be.rejectedWith("revert");
		});

		it("should revert if exposure index does not exist", async() => {
			await exposureLedger.autoFund(1, {from: droneAddr}).should.be.rejectedWith("revert");
		});

		it("should call method when exposure is in the correct state", async() => {
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;
			// exposure state is FundedByPlatform so method will be called
			await exposureLedger.close(0, {from: userAddr}).should.be.fulfilled;
		});
	});

	describe("onlyUser()", () => {
		it("should revert if exposure index does not exist", async() => {
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;
			await exposureLedger.close(1, {from: userAddr}).should.be.rejectedWith("revert");
		});

		it("should revert if method is called not from user", async() => {
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;
			await exposureLedger.close(0, {from: droneAddr}).should.be.rejectedWith("revert");
		});

		it("should call method that can be called only by user", async() => {
			await exposureLedger.autoFund(0, {from: droneAddr}).should.be.fulfilled;
			await exposureLedger.close(0, {from: userAddr}).should.be.fulfilled;
		});
	});

	describe("validExposureIndex()", () => {
		it("should revert if exposure index does not exist", async() => {
			await exposureLedger.getExposureResultData(1, {from: userAddr}).should.be.rejectedWith("revert");
		});

		it("should call method if exposure index exists", async() => {
			await exposureLedger.getExposureResultData(0, {from: userAddr}).should.be.fulfilled;
		});
	});

	describe("whenNotPaused()", () => {
		it("should create an exposure when contract is not paused", async() => {
			await exposureLedger.createAndFundNewExposure(true, 0, 30000, 30, returnUserAddr, 0, 0, {from: userAddr, value: initialFund}).should.be.fulfilled;
		});

		it("should revert on exposure creation when contract is paused", async() => {
			await exposureLedger.pause({from: adminAddr}).should.be.fulfilled;
			await exposureLedger.createAndFundNewExposure(true, 0, 30000, 30, returnUserAddr, 0, 0, {from: userAddr, value: initialFund}).should.be.rejectedWith("revert");
		});
	});

});